## v1.40.22 (2025-02-23)

### Fix

- add is_test key to bypass FCM in test mode

## v1.40.21 (2025-02-21)

### Fix

- add missing notifications pub sub fields

## v1.40.20 (2025-02-19)

### Fix

- add notifications related values for pubsub

## v1.40.19 (2025-02-04)

### Fix

- add mongo dates functions to query documents with dates

## v1.40.18 (2025-01-30)

### Fix

- typo

## v1.40.17 (2025-01-30)

### Fix

- fix process method

## v1.40.16 (2025-01-30)

### Fix

- fix date processing method setting wrong valyes for day month year and hour

## v1.40.15 (2025-01-30)

### Fix

- fix SLog method not trimming long strings

## v1.40.14 (2025-01-30)

### Fix

- fix trimJSON not accepting slices of json

## v1.40.13 (2025-01-30)

### Fix

- implement trimJSOn inside JSONLog

## v1.40.12 (2025-01-30)

### Fix

- add json cleaning for logging safely

## v1.40.11 (2025-01-30)

### Fix

- remove unused method

## v1.40.10 (2025-01-30)

### Fix

- fix time rounding issues

## v1.40.9 (2025-01-30)

### Fix

- add rounding method

## v1.40.8 (2025-01-29)

### Fix

- add missing validators for day and month

## v1.40.7 (2025-01-29)

### Fix

- remove hardcoded UTC value

## v1.40.6 (2025-01-28)

### Fix

- prevent slices returned to be nil

## v1.40.5 (2025-01-28)

### Fix

- add method to remove duplicates of non comparable primitive.ObjectID elements

## v1.40.4 (2025-01-27)

### Fix

- fix wrong key nomenclature in date.go

## v1.40.3 (2025-01-27)

### Fix

- add comments

## v1.40.2 (2025-01-27)

### Fix

- fix address constructors to add one to compute timezone directly
- fix date constructor to populate implicit fields directly

## v1.40.1 (2025-01-27)

### Fix

- add date.go in embedcup and enhance swipetimezone method

## v1.40.0 (2025-01-26)

### Feat

- add slice comparaison methods for comparable types and a distinct function for object ids slice

## v1.39.8 (2025-01-26)

### Fix

- add necessary construcors to all embedcup files

## v1.39.7 (2025-01-26)

### Fix

- change float64 for float32 for lat and lon valyes

## v1.39.6 (2025-01-26)

### Fix

- fix key alias

## v1.39.5 (2025-01-26)

### Fix

- use type alias for embedcup keys to be an alias for embedcup.Key to be able to call .String() method on keys

## v1.39.4 (2025-01-26)

### Fix

- use distinct types for keys and add a String() method

## v1.39.3 (2025-01-26)

### Fix

- add a new alias for json and bson key constants

## v1.39.2 (2025-01-26)

### Fix

- add proper keys to embedcup files and add a new embed called owner to place in all services

## v1.39.1 (2025-01-21)

### Fix

- add keys constants for address embed

## v1.39.0 (2025-01-21)

### Feat

- add an address struct to avoid hardcoding across services

## v1.38.7 (2025-01-19)

### Fix

- un capitalize error

## v1.38.6 (2025-01-18)

### Fix

- handle errors from grok api

## v1.38.5 (2025-01-18)

### Fix

- add debug logs to grok

## v1.38.4 (2025-01-18)

### Fix

- fix wrong return structure for grok api

## v1.38.3 (2025-01-18)

### Fix

- redefine response structure of grok api

## v1.38.2 (2025-01-18)

### Fix

- fix wrong response structure when interacting with Grok api

## v1.38.1 (2025-01-18)

### Fix

- fix panic when no choice returns

## v1.38.0 (2025-01-18)

### Feat

- finish grok prompter using select case for the http request

## v1.37.0 (2025-01-16)

### Feat

- add grok prompt support

## v1.36.20 (2025-01-15)

### Fix

- remove fmt.stringer interface causing issues

## v1.36.19 (2025-01-15)

### Fix

- add string normalization method

## v1.36.18 (2025-01-12)

### Fix

- rename template name to avoid camelCase

## v1.36.17 (2025-01-12)

### Fix

- properly user slog.String
- add suspicious connection constants to emailjuice

## v1.36.16 (2025-01-11)

### Fix

- add missing method to retrieve city name using lat and lon

## v1.36.15 (2025-01-05)

### Fix

- fix wrong key in mongojuice regarding search index type

## v1.36.14 (2025-01-05)

### Fix

- fix factory registry causing panic

## v1.36.13 (2025-01-05)

### Fix

- fix factory registry returning pointers to the same instance, provoking serious collateral issues in production

## v1.36.12 (2025-01-05)

### Fix

- fix package name issue in reflectjuice

## v1.36.11 (2025-01-05)

### Fix

- fix recursion issue

## v1.36.10 (2025-01-05)

### Fix

- fix recursive call of method causing error

## v1.36.9 (2025-01-05)

### Fix

- make method safer and more simple

## v1.36.8 (2025-01-05)

### Fix

- add zero fields case handling
- simplify method

## v1.36.7 (2025-01-05)

### Fix

- fix method that was not using deep copying

## v1.36.6 (2025-01-04)

### Fix

- fix method to recursively check for nil pointer fields in embedded structs and embedded pointers to structs

## v1.36.5 (2024-12-29)

### Refactor

- remove unused method

## v1.36.4 (2024-12-15)

### Fix

- prevent pipeline from triggering when tagging
- fix pipeline failing when pushing tags

## v1.36.3 (2024-12-14)

## v1.36.2 (2024-12-13)

### Refactor

- fix typo in authorizationjuice in swagger comments

## v1.36.1 (2024-12-13)

### Fix

- add missing struct in authorizationjuice package

## v1.36.0 (2024-12-12)

### Feat

- add authorizationjuice package to uniformize creation of authorization payloads accross services

### Fix

- remove unused file

## v1.35.21 (2024-12-07)

### Fix

- add different charges in bill mocks

## v1.35.20 (2024-12-06)

### Fix

- add new embedded struct to archive data without deleting it and keep backend healty

## v1.35.19 (2024-12-05)

### Fix

- fix wrong value in json number for mocking deduction aggregations

## v1.35.18 (2024-12-05)

### Refactor

- encapsulate func params in various packages

## v1.35.17 (2024-12-04)

### Fix

- add missing total_per_group key for deduction aggregations

## v1.35.16 (2024-12-02)

### Fix

- edit fields for tessting purposes

## v1.35.15 (2024-12-02)

### Fix

- fix wrong mock type

## v1.35.14 (2024-12-02)

### Fix

- fix wrong type for maps

## v1.35.13 (2024-12-01)

### Fix

- add missing key in bill mock to mock default supplier invoice rate to be printed on PDF

## v1.35.12 (2024-11-29)

### Fix

- structure sub contributions properly

## v1.35.11 (2024-11-29)

### Fix

- change bool value for testing purposes

## v1.35.10 (2024-11-29)

### Fix

- testing problematic payroll

## v1.35.9 (2024-11-29)

### Fix

- add parameter to allow passing json.Number to deduction entry mock

## v1.35.8 (2024-11-29)

### Fix

- add a variable to hold shared primitive ID value

## v1.35.7 (2024-11-29)

### Fix

- fix bad mimicking of Go json decoder by adding a method to generate slices of interface

## v1.35.6 (2024-11-28)

### Fix

- add missing field in payroll mock

## v1.35.5 (2024-11-25)

### Fix

- fix var names in mocks

## v1.35.4 (2024-11-25)

### Fix

- add mocks for payrolls service

## v1.35.3 (2024-11-14)

### Fix

- add missing constants to generate payrolls and deduction aggregations pdfs

## v1.35.2 (2024-11-14)

### Fix

- remove deprecated constants

## v1.35.1 (2024-11-14)

### Fix

- add a few more constants for pub sub and change the type pdftype for value for a more generic approach

## v1.35.0 (2024-11-14)

### Feat

- add pub sub keys to modify flags related to the payment status of payrolls and bills entities

## v1.34.5 (2024-10-23)

### Fix

- escape html in logtest method

## v1.34.4 (2024-10-22)

### Fix

- remove const

## v1.34.3 (2024-10-21)

### Fix

- rename key

## v1.34.2 (2024-10-21)

### Fix

- add missing const in gcloudjuice

## v1.34.1 (2024-10-21)

### Fix

- add a slice of headers to be forwarded to storage service

## v1.34.0 (2024-10-21)

### Feat

- add constcup package to hold shared constants

## v1.33.2 (2024-10-21)

### Fix

- add missing const in testjuice

## v1.33.1 (2024-10-21)

### Fix

- fix unused const in testjuice

## v1.33.0 (2024-10-21)

### Feat

- add logtest method in testjuice

## v1.32.0 (2024-10-21)

### Feat

- add WaitForServer method in testjuice

## v1.31.1 (2024-10-18)

### Fix

- enhance testjuice methods for id and string retrievals from response bodies

## v1.31.0 (2024-10-18)

### Feat

- add values retrieval recursive methods in testjuice

## v1.30.2 (2024-10-07)

### Fix

- add missing idempotency header key forwarding in multipartjuice

## v1.30.1 (2024-10-06)

### Refactor

- **billable.go**: change file name

## v1.30.0 (2024-10-06)

### Feat

- **common.go**: add common struct to be embedded in fabriktors entities

## v1.29.28 (2024-08-25)

### Fix

- dynamic bill

## v1.29.27 (2024-08-25)

### Fix

- fixed bill

## v1.29.26 (2024-08-25)

### Fix

- fixed estimate

## v1.29.25 (2024-08-25)

### Fix

- dynamic estimate

## v1.29.24 (2024-08-19)

### Fix

- add iscanceled field in bill mock

## v1.29.23 (2024-08-19)

### Fix

- edit mock field

## v1.29.22 (2024-08-19)

### Fix

- **mockcup.go**: add missing fields to bill mock

## v1.29.21 (2024-08-18)

### Fix

- **mockcup.go**: add missing fields in bill mock

## v1.29.20 (2024-08-16)

### Fix

- **mockcup.go**: add missing value in details in supinvoice mock

## v1.29.19 (2024-08-15)

### Fix

- estimate true

## v1.29.18 (2024-08-15)

### Fix

- estimate false and dynamic

## v1.29.17 (2024-08-15)

### Fix

- estimate true fixed

## v1.29.16 (2024-08-15)

### Fix

- estimate true and dynamic

## v1.29.15 (2024-08-14)

### Fix

- edit mock

## v1.29.14 (2024-08-14)

### Fix

- edit mock

## v1.29.13 (2024-08-14)

### Fix

- edit mock

## v1.29.12 (2024-08-13)

### Fix

- edit mock for testing

## v1.29.11 (2024-08-13)

### Fix

- edit bill mock

## v1.29.10 (2024-08-13)

### Fix

- test bill mock

## v1.29.9 (2024-08-13)

### Fix

- edit mock for testing purposes

## v1.29.8 (2024-08-12)

### Fix

- switch field valuye for tessting purposes

## v1.29.7 (2024-08-11)

### Fix

- fix credit mock

## v1.29.6 (2024-08-11)

### Fix

- **mockcup.go**: add a credit mock

## v1.29.5 (2024-08-09)

### Fix

- **mockcup.go**: enhance mock

## v1.29.4 (2024-08-09)

### Fix

- **mockcup.go**: move period from employee name to details field

## v1.29.3 (2024-08-08)

### Refactor

- add longer note

## v1.29.2 (2024-08-08)

### Fix

- **multipartjuice.go**: remove unused param

## v1.29.1 (2024-08-08)

### Fix

- change key

## v1.29.0 (2024-08-08)

### Feat

- **multipartjuice.go**: add multipartjuice package

## v1.28.6 (2024-08-07)

### Refactor

- simplify keys name

## v1.28.5 (2024-08-07)

### Refactor

- change key names

## v1.28.4 (2024-08-06)

### Fix

- **pubsub.go**: add constants

## v1.28.3 (2024-08-06)

### Refactor

- **mockcup.go**: add key suffix

## v1.28.2 (2024-08-06)

### Refactor

- **mockcup.go**: add keys

## v1.28.1 (2024-08-06)

### Fix

- **mockcup.go**: fix wrong mock

## v1.28.0 (2024-07-30)

### Feat

- **mockcup.go**: add mockcup

## v1.27.3 (2024-07-27)

### Fix

- **stringjuice.go**: fix int being converted to bool in case of 0 and 1

## v1.27.2 (2024-07-24)

### Refactor

- add constant for pdf type

## v1.27.1 (2024-07-24)

### Refactor

- **pubsub.go**: add constants

## v1.27.0 (2024-07-24)

### Feat

- **pubsub.go**: add constants for pubsub keys and topic

## v1.26.1 (2024-07-23)

### Fix

- remove payloadcup

## v1.26.0 (2024-07-23)

### Feat

- add payloadcup

## v1.25.0 (2024-07-19)

### Feat

- add moneyjuice package

## v1.24.1 (2024-07-19)

### Fix

- add conversion factors

## v1.24.0 (2024-07-14)

### Feat

- add BuildQueryString

## v1.23.3 (2024-07-11)

### Fix

- exported unexported const

## v1.23.2 (2024-07-11)

### Refactor

- change variable names

## v1.23.1 (2024-07-10)

### Refactor

- refactor unitjuice

## v1.23.0 (2024-07-09)

### Feat

- add a ISO currency checker

## v1.22.1 (2024-07-09)

### Fix

- big in int32 validator

## v1.22.0 (2024-07-09)

### Feat

- add validators

## v1.21.0 (2024-07-09)

### Feat

- unitjuice

## v1.20.0 (2024-06-19)

### Feat

- **validationjuice.go**: added a validator to validate phone numbers in e164 format under a pointer field

## v1.19.6 (2024-06-07)

## v1.19.5 (2024-06-06)

## v1.19.4 (2024-06-06)

## v1.19.3 (2024-06-05)

## v1.19.2 (2024-05-27)

## v1.19.1 (2024-05-27)

## v1.19.0 (2024-05-27)

## v1.18.9 (2024-05-26)

## v1.18.7 (2024-05-26)

## v1.18.6 (2024-05-14)

## v1.18.5 (2024-05-14)

## v1.18.4 (2024-05-13)

## v1.18.3 (2024-05-13)

## v1.18.2 (2024-05-11)

## v1.18.1 (2024-05-11)

## v1.18.0 (2024-05-11)

## v1.17.20 (2024-05-10)

## v1.17.17 (2024-05-06)

## v1.17.16 (2024-05-06)

## v1.17.15 (2024-05-06)

## v1.17.14 (2024-05-05)

## v1.17.13 (2024-05-04)

## v1.17.12 (2024-05-04)

## v1.17.11 (2024-05-04)

## v1.17.10 (2024-05-03)

## v1.17.9 (2024-05-01)

## v1.17.8 (2024-05-01)

## v1.17.7 (2024-04-30)

## v1.17.6 (2024-04-30)

## v1.17.5 (2024-04-30)

## v1.17.4 (2024-04-26)

## v1.17.3 (2024-04-24)

## v1.17.2 (2024-04-23)

## v1.17.1 (2024-04-18)

## v1.17.0 (2024-04-18)

## v1.16.9 (2024-04-17)

## v1.16.8 (2024-04-17)

## v1.16.7 (2024-04-17)

## v1.16.6 (2024-04-17)

## v1.16.5 (2024-04-17)

## v1.16.4 (2024-04-17)

## v1.16.3 (2024-04-17)

## v1.16.2 (2024-04-17)

## v1.16.1 (2024-04-17)

## v1.16.0 (2024-04-17)

## v1.15.12 (2024-04-17)

## v1.15.11 (2024-04-16)

## v1.15.10 (2024-04-15)

## v1.15.9 (2024-04-15)

## v1.15.8 (2024-04-15)

## v1.15.7 (2024-04-12)

## v1.15.6 (2024-04-12)

## v1.15.5 (2024-04-12)

## v1.15.4 (2024-04-10)

## v1.15.3 (2024-04-10)

## v1.15.2 (2024-04-09)

## v1.15.1 (2024-04-09)

## v1.15.0 (2024-04-09)

## v1.14.11 (2024-04-09)

## v1.14.10 (2024-04-09)

## v1.14.9 (2024-04-08)

## v1.14.8 (2024-04-06)

## v1.14.7 (2024-04-05)

## v1.14.6 (2024-04-02)

## v1.14.5 (2024-03-28)

## v1.14.4 (2024-03-28)

## v1.14.3 (2024-03-28)

## v1.14.2 (2024-03-28)

## v1.14.1 (2024-03-27)

## v1.14.0 (2024-03-24)

## v1.13.10 (2024-03-21)

## v1.13.9 (2024-03-20)

## v1.13.8 (2024-03-20)

## v1.13.7 (2024-03-20)

## v1.13.6 (2024-03-20)

## v1.13.5 (2024-03-15)

## v1.13.4 (2024-03-15)

## v1.13.3 (2024-03-15)

## v1.13.2 (2024-03-15)

## v1.13.1 (2024-03-14)

## v1.13.0 (2024-03-13)

## v1.12.17 (2024-03-05)

## v1.12.16 (2024-02-29)

## v1.12.15 (2024-02-28)

## v1.12.14 (2024-02-28)

## v1.12.13 (2024-02-27)

## v1.12.12 (2024-02-27)

## v1.12.10 (2024-02-26)

## v1.12.9 (2024-02-25)

## v1.12.8 (2024-02-23)

## v1.12.7 (2024-02-23)

## v1.12.6 (2024-02-23)

## v1.12.5 (2024-02-23)

## v1.12.4 (2024-02-23)

## v1.12.3 (2024-02-23)

## v1.12.23 (2024-02-22)

## v1.12.2 (2024-02-22)

## v1.12.1 (2024-02-22)

## v1.12.0 (2024-02-22)

## v1.11.25 (2024-02-22)

## v1.11.24 (2024-02-22)

## v1.11.23 (2024-02-22)

## v1.11.22 (2024-02-22)

## v1.11.21 (2024-02-21)

## v1.11.20 (2024-02-21)

## v1.11.19 (2024-02-20)

## v1.11.18 (2024-02-20)

## v1.11.16 (2024-02-20)

## v1.11.14 (2024-02-20)

## v1.11.13 (2024-02-19)

## v1.11.12 (2024-02-19)

## v1.11.11 (2024-02-19)

## v1.11.10 (2024-02-19)

## v1.11.9 (2024-02-19)

## v1.11.8 (2024-02-18)

## v1.11.7 (2024-02-17)

## v1.11.6 (2024-02-16)

## v1.11.5 (2024-02-16)

## v1.11.3 (2024-02-16)

## v1.11.2 (2024-02-14)

## v1.11.1 (2024-02-14)

## v1.11.0 (2024-02-13)

## v1.10.3 (2024-02-13)

## v1.10.2 (2024-02-13)

## v1.10.0 (2024-02-13)

## v1.9.21 (2024-02-13)

## v1.9.20 (2024-02-13)

## v1.9.19 (2024-02-13)

## v1.9.18 (2024-02-13)

## v1.9.16 (2024-02-13)

## v1.9.15 (2024-02-12)

## v1.9.14 (2024-02-12)

## v1.9.13 (2024-02-12)

## v1.9.12 (2024-02-12)

## v1.9.11 (2024-02-12)

## v1.9.10 (2024-02-12)

## v1.9.9 (2024-02-12)

## v1.9.8 (2024-02-12)

## v1.9.7 (2024-02-12)

## v1.9.6 (2024-02-12)

## v1.9.5 (2024-02-12)

## v1.9.4 (2024-02-12)

## v1.9.3 (2024-02-12)

## v1.9.2 (2024-02-11)

## v1.9.1 (2024-02-10)

## v1.9.0 (2024-02-03)

## v1.8.8 (2024-02-01)

## v1.8.7 (2024-01-28)

## v1.8.5 (2024-01-09)

## v1.8.4 (2024-01-07)

## v1.8.3 (2024-01-07)

## v1.8.2 (2024-01-07)

## v1.8.1 (2024-01-07)

## v1.8.0 (2024-01-04)

## v1.7.9 (2023-12-22)

## v1.7.8 (2023-12-18)

## v1.7.7 (2023-12-18)

## v1.7.6 (2023-12-18)

## v1.7.5 (2023-12-14)

## v1.7.4 (2023-12-14)

## v1.7.3 (2023-12-12)

## v1.7.2 (2023-12-12)

## v1.7.1 (2023-12-12)

## v1.7.0 (2023-12-12)

## v1.6.9 (2023-12-12)

## v1.6.8 (2023-12-04)

## v1.6.7 (2023-12-04)

## v1.6.6 (2023-12-04)

## v1.6.5 (2023-12-04)

## v1.6.4 (2023-12-03)

## v1.6.3 (2023-12-03)

## v1.6.2 (2023-12-03)

## v1.6.1 (2023-12-03)

## v1.6.0 (2023-12-03)

## v1.5.9 (2023-12-03)

## v1.5.8 (2023-12-03)

## v1.5.7 (2023-12-03)

## v1.5.6 (2023-12-03)

## v1.5.5 (2023-12-02)

## v1.5.4 (2023-12-01)

## v1.5.3 (2023-12-01)

## v1.5.2 (2023-11-30)

## v1.5.1 (2023-11-18)

## v1.5.0 (2023-11-15)

## v1.4.8 (2023-11-14)

## v1.4.7 (2023-11-13)

## v1.4.5 (2023-11-13)

## v1.4.4 (2023-11-13)

## v1.4.3 (2023-11-13)

## v1.4.2 (2023-11-07)

## v1.4.1 (2023-11-07)

## v1.4.0 (2023-11-07)

## v1.3.8 (2023-11-07)

## v1.3.7 (2023-11-06)

## v1.3.6 (2023-11-06)

## v1.3.5 (2023-11-04)

## v1.3.4 (2023-11-02)

## v1.3.3 (2023-11-02)

## v1.3.2 (2023-11-02)

## v1.3.1 (2023-11-02)
