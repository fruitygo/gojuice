.PHONY: air localrununix testall testallverbose testrace testparallel testcoverage showreport updateallmodules help

air:
	@echo "Press Enter to launch the application with hot reload using air, or any other key to cancel."
	@read -n 1 answer && \
	if [ "$$answer" = "" ]; then \
		echo "Launching the application with hot reload using air..."; \
		air; \
	else \
		echo && \
		echo "Aborting launch."; \
	fi

localrununix:
	@echo "Press Enter to launch the application, or any other key to cancel."
	@read -n 1 answer && \
	if [ "$$answer" = "" ]; then \
		echo "Launching the application..."; \
		go run ./cmd/main/; \
	else \
		echo && \
		echo "Aborting launch."; \
	fi

localrunwindows:
	@echo "Press Enter to launch the application on Windows, or any other key to cancel."
	@set /p answer=">" && if not defined answer ( \
	    echo "Launching the application on Windows..."; \
	    go run .\cmd\main\; \
	) else ( \
    	echo Aborting launch.; \
	)

testall:
	@echo "Running all tests..."
	@go test -timeout 30m ./... && echo "Successfully ran all tests."

testallverbose:
	@echo "Running all tests with verbose output..."
	@go test -timeout 30m -v ./... && echo "Successfully ran all tests with verbose output."

showreport:
	@echo "Showing test coverage report..."
	@go test -timeout 30m -coverprofile=coverage.out ./... && echo "Successfully generated test coverage report."
	@go tool cover -html ./coverage.out
	@rm -rf ./coverage.out && echo "Successfully showed test coverage report."

testrace:
	@echo "Running race detection tests..."
	@go test -timeout 30m -race ./... && echo "Successfully ran race detection tests."

testparallel:
	@echo "Running tests in parallel with verbose output..."
	@go test -timeout 30m -v -race -parallel 200 ./... && echo "Successfully ran tests in parallel with verbose output."

testcoverage:
	@echo "Generating test coverage report..."
	@go test -timeout 30m -coverprofile=coverage.out ./... && echo "Successfully generated test coverage report."

updatemodules:
	@echo "Press Enter to update all Go packages, or any other key to cancel."
	@read -n 1 answer && \
	if [ "$$answer" = "" ]; then \
		echo "Updating all Go packages..."; \
		go get -u ./... && \
		echo "Successfully updated all Go packages."; \
	else \
		echo && \
		echo "Aborting module update."; \
	fi

showlatesttag:
	@echo "Showing the latest tag..."
	@latest_tag=$$(git describe --tags $$(git rev-list --tags --max-count=1)) && \
	echo "Latest tag is $$latest_tag."

help:
	@echo "Available targets:"
	@echo "  air              - Run the application with air for hot reloading."
	@echo "  localrununix     - Run the application locally on a Unix system."
	@echo "  localrunwindows  - Run the application locally on Windows."
	@echo "  testall          - Run all tests."
	@echo "  testallverbose   - Run all tests with verbose output."
	@echo "  testrace         - Run all tests with race detection."
	@echo "  testparallel     - Run all tests in parallel with race detection."
	@echo "  testcoverage     - Generate test coverage report."
	@echo "  showreport       - Show the test coverage report in a web browser."
	@echo "  updatemodules    - Update all Go modules."
	@echo "  showlatesttag    - Show the latest tag."
	@echo "  help             - Show this help message."
