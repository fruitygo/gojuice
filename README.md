# Gojuice 🧃

![Gojuice Logo](./img/gojuice-fabriktor.png)

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/fruitygo/gojuice.svg)](https://pkg.go.dev/gitlab.com/fruitygo/gojuice)
[![License Badge](https://img.shields.io/gitlab/license/fruitygo/gojuice?color=orange)](https://gitlab.com/fruitygo/gojuice/-/raw/main/LICENSE)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/fruitygo/gojuice)](https://goreportcard.com/report/gitlab.com/fruitygo/gojuice)
[![Tag Badge](https://img.shields.io/gitlab/v/tag/fruitygo/gojuice?color=gree&sort=date)](https://gitlab.com/fruitygo/gojuice/-/tags)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/fruitygo/gojuice)](https://gitlab.com/fruitygo/gojuice/-/commits/main?ref_type=heads)
[![Pipeline Badge](https://gitlab.com/fruitygo/gojuice/badges/main/pipeline.svg)](https://gitlab.com/fruitygo/gojuice/-/pipelines)
[![Maintenance](https://img.shields.io/maintenance/yes/9999)](https://gitlab.com/fruitygo/gojuice/-/project_members)
[![coverage](https://gitlab.com/fruitygo/gojuice/badges/main/coverage.svg?job=test)](https://gitlab.com/fruitygo/gojuice/-/graphs/main/charts)

[![GitLab stars](https://img.shields.io/gitlab/stars/fruitygo/gojuice?style=social)](https://gitlab.com/fruitygo/gojuice/-/starrers)

## Table of Contents

- [Features](#features)
- [Description](#description)
- [Installation](#installation)
- [Reference](#reference)
- [License](#license)
- [End](#end)

## Features

#### Gojuice 🧃 is the public Fabriktor's code library.

1. **Cup Packages**: Contain various data. They include:

   - `🥤 structcup`
   - `🥤 embedcup`
   - `🥤 mockcup`
   - `And more...`

2. **Juice Packages**: Provide utility functions and methods related to their respective names. They include:
   - `🧃 apijuice`
   - `🧃 mapjuice`
   - `🧃 modeljuice`
   - `🧃 mongojuice`
   - `🧃 logjuice`
   - `🧃 httpjuice`
   - `🧃 firebasejuice`
   - `🧃 gcloudjuice`
   - `🧃 reflectjuice`
   - `🧃 slicejuice`
   - `🧃 testjuice`
   - `🧃 timejuice`
   - `🧃 validationjuice`
   - `🧃 workerjuice`
   - `And more...`

## Description

**Gojuice** 🧃 is the public library of Fabriktor.

- 🔗 [Fabriktor Inc.](https://fabriktor.com)

Gojuice is organized into two main sections:

1. **Data Packages**: These packages, ending with **cup**, define various data. They include `embedcup`, `mockcup`, `structcup`, & more.

2. **Utility Packages**: These packages, ending with **juice**, provide a set of utility functions and methods related to their respective names. They include `apijuice`, `modeljuice`, `logjuice`, & more.

## Installation

To install Gojuice, use the `go get` command:

```bash
go get gitlab.com/fruitygo/gojuice
```

Then, import it:

```go
import "gitlab.com/fruitygo/gojuice"
```

## Reference

Refer to our [Go Reference](https://pkg.go.dev/gitlab.com/fruitygo/gojuice) for detailed usage instructions and examples.

## License

Gojuice is licensed under the [MIT License](https://gitlab.com/fruitygo/gojuice/-/raw/main/LICENSE).

## End

![Fabriktor Logo](https://backend.fabriktor.com/filehub/img/gitlab/chubby.gif)
![Fabriktor Logo](https://backend.fabriktor.com/filehub/img/gitlab/screwdriver.gif)
![Fabriktor Logo](https://backend.fabriktor.com/filehub/img/gitlab/key.gif)
![Fabriktor Logo](https://backend.fabriktor.com/filehub/img/gitlab/boss.gif)
