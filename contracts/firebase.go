package contracts

import (
	"context"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
)

var _ FirebaseAppInterface = (*firebase.App)(nil)

// FirebaseAppInterface abstracts methods from firebase.App.
type FirebaseAppInterface interface {
	Auth(ctx context.Context) (*auth.Client, error)
	// Add other methods as needed.
}
