package contracts

import (
	"context"

	"gitlab.com/fruitygo/gojuice/juice/llmjuice"
)

type Prompter interface {
	Prompt(ctx context.Context, opts llmjuice.PrompterOpts) (string, error)
}
