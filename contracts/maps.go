package contracts

import (
	"context"

	"googlemaps.github.io/maps"
)

var _ MapsClientInterface = (*maps.Client)(nil)

// MapsClientInterface abstracts methods from maps.Client.
type MapsClientInterface interface {
	Timezone(ctx context.Context, r *maps.TimezoneRequest) (*maps.TimezoneResult, error)
	Geocode(ctx context.Context, r *maps.GeocodingRequest) ([]maps.GeocodingResult, error)
	// Add other methods as needed.
}
