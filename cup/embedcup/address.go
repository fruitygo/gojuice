// Package embedcup contains the common models and embedded structs used across
// different entities in the Fabriktor. These structs represent shared fields
// and provide common functionality like Getters and Setters.
package embedcup

import (
	"context"

	"gitlab.com/fruitygo/gojuice/contracts"
	"gitlab.com/fruitygo/gojuice/juice/gcloudjuice"
)

// ------------------------------
// CONSTS
// ------------------------------

const (
	AddressStreetKey           AddressKey = "street"
	AddressCityKey             AddressKey = "city"
	AddressStateKey            AddressKey = "state"
	AddressPostalCodeKey       AddressKey = "postal_code"
	AddressCountryKey          AddressKey = "country"
	AddressLatitudeKey         AddressKey = "latitude"
	AddressLongitudeKey        AddressKey = "longitude"
	AddressPlaceIDKey          AddressKey = "place_id"
	AddressTimezoneKey         AddressKey = "timezone"
	AddressFormattedAddressKey AddressKey = "formatted_address"
	AddressPlaceNameKey        AddressKey = "place_name"
)

// ------------------------------
// TYPES
// ------------------------------

// AddressKey represents a key for address-related fields.
type AddressKey = Key

// Address represents an address picked from a Flutter Google location picker.
// swagger:model
type Address struct {
	// ------------------------------
	// EXPLICIT FIELDS
	// CAN BE ZERO VALUES
	// ------------------------------

	// The complete formatted address, as returned by Google.
	// example: 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA
	// required: false
	FormattedAddress *string `json:"formatted_address" bson:"formatted_address" validate:"nonnilpointer"`

	// The name of the place, typically for landmarks or specific locations.
	// example: Central Park
	// required: false
	PlaceName *string `json:"place_name" bson:"place_name" validate:"nonnilpointer"`

	// The street number and name of the address.
	// example: 1600 Amphitheatre Parkway
	// required: false
	Street *string `json:"street" bson:"street" validate:"nonnilpointer"`

	// The city or locality of the address.
	// example: Mountain View
	// required: false
	City *string `json:"city" bson:"city" validate:"nonnilpointer"`

	// The state, province, or region of the address.
	// example: CA
	// required: false
	State *string `json:"state" bson:"state" validate:"nonnilpointer"`

	// The postal code of the address.
	// example: 94043
	// required: false
	PostalCode *string `json:"postal_code" bson:"postal_code" validate:"nonnilpointer"`

	// The country of the address.
	// example: USA
	// required: false
	Country *string `json:"country" bson:"country" validate:"nonnilpointer"`

	// The latitude of the location, if available.
	// example: 37.4224764
	// required: false
	Latitude *float64 `json:"latitude" bson:"latitude" validate:"nonnilpointer"`

	// The longitude of the location, if available.
	// example: -122.0842499
	// required: false
	Longitude *float64 `json:"longitude" bson:"longitude" validate:"nonnilpointer"`

	// The Google Place ID associated with the location.
	// example: ChIJ2eUgeAK6j4ARbn5u_wAGqWA
	// required: false
	PlaceID *string `json:"place_id" bson:"place_id" validate:"nonnilpointer"`

	// ------------------------------
	// IMPLICIT FIELDS
	// CAN BE ZERO VALUES
	// ------------------------------

	// The timezone of the location implicitely computed from the coordinates.
	// example: America/New_York
	// readOnly: true
	Timezone *string `bson:"timezone" json:"timezone" validate:"nonnilpointer"`
}

// ------------------------------
// CONSTRUCTOR
// ------------------------------

func NewAddress(
	formattedAddress, placeName, street, city, state, postalCode, country, placeID string,
	latitude, longitude float64,
) *Address {
	x := new(Address)
	x.SetFormattedAddress(formattedAddress)
	x.SetPlaceName(placeName)
	x.SetStreet(street)
	x.SetCity(city)
	x.SetState(state)
	x.SetPostalCode(postalCode)
	x.SetCountry(country)
	x.SetLatitude(latitude)
	x.SetLongitude(longitude)
	x.SetPlaceID(placeID)
	return x
}

func NewAddressWithTimezone(
	ctx context.Context, c contracts.MapsClientInterface,
	formattedAddress, placeName, street, city, state, postalCode, country, placeID string,
	latitude, longitude float64,
) (*Address, error) {
	x := NewAddress(
		formattedAddress,
		placeName,
		street,
		city,
		state,
		postalCode,
		country,
		placeID,
		latitude,
		longitude,
	)

	err := x.SetTimezoneFromCoordinates(ctx, c, latitude, longitude)

	return x, err
}

// ------------------------------
// METHODS
// ------------------------------

// SetTimezoneFromCoordinates sets the timezone from the given coordinates.
func (a *Address) SetTimezoneFromCoordinates(ctx context.Context, c contracts.MapsClientInterface, latitude float64, longitude float64) error {
	timezone, err := gcloudjuice.GetTimezoneFromCoordinates(
		ctx,
		c,
		latitude,
		longitude,
	)
	if err != nil {
		return err
	}

	a.SetTimezone(timezone)

	return nil
}

// ------------------------------
// GETTERS
// ------------------------------
// Copies of the values are returned to prevent the caller from modifying the original values.
// The caller can only modify the original values by calling the setters.

func (a *Address) GetFormattedAddress() string {
	if a.FormattedAddress == nil {
		return ""
	}
	return *a.FormattedAddress
}

func (a *Address) GetPlaceName() string {
	if a.PlaceName == nil {
		return ""
	}
	return *a.PlaceName
}

func (a *Address) GetStreet() string {
	if a.Street == nil {
		return ""
	}
	return *a.Street
}

func (a *Address) GetCity() string {
	if a.City == nil {
		return ""
	}
	return *a.City
}

func (a *Address) GetState() string {
	if a.State == nil {
		return ""
	}
	return *a.State
}

func (a *Address) GetPostalCode() string {
	if a.PostalCode == nil {
		return ""
	}
	return *a.PostalCode
}

func (a *Address) GetCountry() string {
	if a.Country == nil {
		return ""
	}
	return *a.Country
}

func (a *Address) GetLatitude() float64 {
	if a.Latitude == nil {
		return 0
	}
	return *a.Latitude
}

func (a *Address) GetLongitude() float64 {
	if a.Longitude == nil {
		return 0
	}
	return *a.Longitude
}

func (a *Address) GetPlaceID() string {
	if a.PlaceID == nil {
		return ""
	}
	return *a.PlaceID
}

func (a *Address) GetTimezone() string {
	if a.Timezone == nil {
		return ""
	}
	return *a.Timezone
}

// ------------------------------
// SETTERS
// ------------------------------
// Setters are utilized to set the values of the struct fields.

func (a *Address) SetFormattedAddress(address string) {
	if a.FormattedAddress == nil {
		a.FormattedAddress = new(string)
	}
	*a.FormattedAddress = address
}

func (a *Address) SetPlaceName(placeName string) {
	if a.PlaceName == nil {
		a.PlaceName = new(string)
	}
	*a.PlaceName = placeName
}

func (a *Address) SetStreet(street string) {
	if a.Street == nil {
		a.Street = new(string)
	}
	*a.Street = street
}

func (a *Address) SetCity(city string) {
	if a.City == nil {
		a.City = new(string)
	}
	*a.City = city
}

func (a *Address) SetState(state string) {
	if a.State == nil {
		a.State = new(string)
	}
	*a.State = state
}

func (a *Address) SetPostalCode(postalCode string) {
	if a.PostalCode == nil {
		a.PostalCode = new(string)
	}
	*a.PostalCode = postalCode
}

func (a *Address) SetCountry(country string) {
	if a.Country == nil {
		a.Country = new(string)
	}
	*a.Country = country
}

func (a *Address) SetLatitude(latitude float64) {
	if a.Latitude == nil {
		a.Latitude = new(float64)
	}
	*a.Latitude = latitude
}

func (a *Address) SetLongitude(longitude float64) {
	if a.Longitude == nil {
		a.Longitude = new(float64)
	}
	*a.Longitude = longitude
}

func (a *Address) SetPlaceID(placeID string) {
	if a.PlaceID == nil {
		a.PlaceID = new(string)
	}
	*a.PlaceID = placeID
}

func (a *Address) SetTimezone(timezone string) {
	if a.Timezone == nil {
		a.Timezone = new(string)
	}
	*a.Timezone = timezone
}
