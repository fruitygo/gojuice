// Package embedcup contains the common models and embedded structs used across
// different entities in the Fabriktor. These structs represent shared fields
// and provide common functionality like Getters and Setters.
package embedcup

// ------------------------------
// CONSTS
// ------------------------------

const (
	ArchiveIsArchivedKey ArchiveKey = "is_archived"
)

// ------------------------------
// TYPES
// ------------------------------

// ArchiveKey represents a key for archive-related fields.
type ArchiveKey = Key

// Archive provides a mechanism to flag documents as archived.
//
// This flag enables filtering without preventing access to archived documents.
//
// It serves as an alternative to deletion, helping maintain data integrity
// and supporting queries for deprecated or legacy documents.
// swagger:model
type Archive struct {
	// Indicates whether the document is archived.
	// example: false
	// required: false
	IsArchived *bool `bson:"is_archived" json:"is_archived" validate:"nonnilpointer"`
}

// ------------------------------
// CONSTRUCTOR
// ------------------------------

func NewArchive(isArchived bool) *Archive {
	x := new(Archive)
	x.SetIsArchived(isArchived)
	return x
}

// ------------------------------
// GETTERS
// ------------------------------
// Copies of the values are returned to avoid the caller to modify the original values.
// The caller can only modify the original values by calling the setters.

func (c *Archive) GetIsArchived() bool {
	if c.IsArchived == nil {
		return false
	}
	return *c.IsArchived
}

// ------------------------------
// SETTERS
// ------------------------------
// The setters are utilized to set the values of the struct fields.

func (c *Archive) SetIsArchived(archive bool) {
	if c.IsArchived == nil {
		c.IsArchived = new(bool)
	}
	*c.IsArchived = archive
}
