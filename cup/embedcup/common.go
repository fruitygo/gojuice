// Package embedcup contains the common models and embedded structs used across
// different entities in the Fabriktor. These structs represent shared fields
// and provide common functionality like Getters and Setters.
package embedcup

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// ------------------------------
// CONSTS
// ------------------------------

const (
	CommonIDKey            CommonKey = "_id"
	CommonSchemaVersionKey CommonKey = "schema_version"
	CommonUpdatedAtKey     CommonKey = "updated_at"
	CommonCreatedAtKey     CommonKey = "created_at"
)

// ------------------------------
// TYPES
// ------------------------------

// CommonKey represents a key for common fields.
type CommonKey = Key

// Common represents the common fields for all entities, embedded in all entities.
// swagger:model
type Common struct {
	// The MongoDB ID of the document.
	// example: 5f6d4b9b9c6f9f0001b9c6f9
	// readOnly: true
	ID *primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`

	// The schema version of the document.
	// example: v1
	// readOnly: true
	SchemaVersion *string `bson:"schema_version" json:"schema_version" conform:"lower" validate:"nonnilpointer,nonzeropointerelem"`

	// The last time the document was updated.
	// example: 2020-10-05T10:00:00Z
	// readOnly: true
	UpdatedAt *time.Time `bson:"updated_at" json:"updated_at" validate:"nonnilpointer,nonzeropointerelem"`

	// The time the document was created.
	// example: 2020-10-05T10:00:00Z
	// readOnly: true
	CreatedAt *time.Time `bson:"created_at" json:"created_at" validate:"nonnilpointer,nonzeropointerelem"`
}

// ------------------------------
// CONSTRUCTOR
// ------------------------------

func NewCommon(id primitive.ObjectID, schemaVersion string, updatedAt, createdAt time.Time) *Common {
	x := new(Common)
	x.SetID(id)
	x.SetSchemaVersion(schemaVersion)
	x.SetUpdatedAt(updatedAt)
	x.SetCreatedAt(createdAt)
	return x
}

// ------------------------------
// GETTERS
// ------------------------------
// Copies of the values are returned to prevent the caller from modifying the original values.
// The caller can only modify the original values by calling the setters.

func (c *Common) GetID() primitive.ObjectID {
	if c.ID == nil {
		return primitive.NilObjectID
	}
	return *c.ID
}

func (c *Common) GetSchemaVersion() string {
	if c.SchemaVersion == nil {
		return ""
	}
	return *c.SchemaVersion
}

func (c *Common) GetUpdatedAt() time.Time {
	if c.UpdatedAt == nil {
		return time.Time{}
	}
	return *c.UpdatedAt
}

func (c *Common) GetCreatedAt() time.Time {
	if c.CreatedAt == nil {
		return time.Time{}
	}
	return *c.CreatedAt
}

// ------------------------------
// SETTERS
// ------------------------------
// Setters are utilized to set the values of the struct fields.

func (c *Common) SetID(id primitive.ObjectID) {
	if c.ID == nil {
		c.ID = new(primitive.ObjectID)
	}
	*c.ID = id
}

func (c *Common) SetSchemaVersion(version string) {
	if c.SchemaVersion == nil {
		c.SchemaVersion = new(string)
	}
	*c.SchemaVersion = version
}

func (c *Common) SetUpdatedAt(updatedAt time.Time) {
	if c.UpdatedAt == nil {
		c.UpdatedAt = new(time.Time)
	}
	*c.UpdatedAt = updatedAt
}

func (c *Common) SetCreatedAt(createdAt time.Time) {
	if c.CreatedAt == nil {
		c.CreatedAt = new(time.Time)
	}
	*c.CreatedAt = createdAt
}
