// Package embedcup contains the common models and embedded structs used across
// different entities in the Fabriktor. These structs represent shared fields
// and provide common functionality like Getters and Setters.
package embedcup

import (
	"time"

	"gitlab.com/fruitygo/gojuice/juice/timejuice"
)

// ------------------------------
// VARS
// ------------------------------

// SwapTimezoneFunc is the function used to swap timezones.
var swapTimezoneFunc = timejuice.SwapTimezone

// ------------------------------
// CONSTS
// ------------------------------

const (
	DateDateKey          DateKey = "date"
	DateProcessedDateKey DateKey = "processed_date"
	DateDayKey           DateKey = "day"
	DateMonthKey         DateKey = "month"
	DateYearKey          DateKey = "year"
	DateHourKey          DateKey = "hour"
)

// ------------------------------
// TYPES
// ------------------------------

// DateKey represents a key for date-related fields.
type DateKey = Key

// Date represents a date optimized for handling multiple time zones, MongoDB pipelines, and query parameter filtering.
// swagger:model
type Date struct {
	// ------------------------------
	// EXPLICIT FIELDS
	// CANNOT BE ZERO VALUES
	// ------------------------------

	// The input date and time in RFC 3339 format, initially set to America/New_York, with the option to override the timezone using the Process method, normalize to midnight, and convert to UTC as per Fabriktor's group README.
	// example: 2021-01-01T00:00:00Z
	// required: true
	Date *time.Time `json:"date" bson:"date" validate:"nonnilpointer,nonzeropointerelem"`

	// ------------------------------
	// IMPLICIT FIELDS
	// CANNOT BE ZERO VALUES
	// ------------------------------

	// ProcessedDate is the date after timezone override, normalization to midnight, and conversion to UTC. If no processing occurs, this field will be equal to date field.
	// example: 2021-01-01T00:00:00Z
	// readOnly: true
	ProcessedDate *time.Time `json:"processed_date" bson:"processed_date" validate:"nonnilpointer,nonzeropointerelem,isutc"`

	// Day extracted from the normalized date.
	// example: 1
	// readOnly: true
	Day *uint `json:"day" bson:"day" validate:"nonnilpointer,nonzeropointerelem,dayrange"`

	// Month extracted from the normalized date.
	// example: 1
	// readOnly: true
	Month *uint `json:"month" bson:"month" validate:"nonnilpointer,nonzeropointerelem,monthrange"`

	// Year extracted from the normalized date.
	// example: 2021
	// readOnly: true
	Year *uint `json:"year" bson:"year" validate:"nonnilpointer,nonzeropointerelem"`

	// ------------------------------
	// IMPLICIT FIELDS
	// CAN BE ZERO VALUES
	// ------------------------------

	// Hour extracted from the normalized date.
	// example: 0
	// readOnly: true
	Hour *uint `json:"hour" bson:"hour" validate:"nonnilpointer"`
}

// ------------------------------
// CONSTRUCTOR
// ------------------------------

func NewDate(date time.Time) *Date {
	d := new(Date)
	d.SetDate(date)
	return d
}

func NewProcessedDate(date time.Time, timezone string, toUTC bool, toMidnight bool) (*Date, error) {
	d := NewDate(date)
	err := d.Process(timezone, toUTC, toMidnight)
	return d, err
}

// ------------------------------
// METHODS
// ------------------------------

// Process converts the date to the specified timezone, normalizes to midnight, and converts to UTC.
func (d *Date) Process(timezone string, toUTC bool, toMidnight bool) error {
	// Convert `GetDate()` (which might be in UTC from MongoDB) to the correct timezone.
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		return err
	}

	// Ensure the original date is interpreted in the correct timezone.
	originalDate := d.GetDate().In(loc)

	// Now pass the corrected local date to SwapTimezone
	processedDate, err := swapTimezoneFunc(originalDate, timezone, toUTC, toMidnight)
	if err != nil {
		return err
	}

	// Set implicit fields.
	d.SetProcessedDate(processedDate)

	// Extract `day, month, year` from the corrected local date.
	d.SetDay(uint(originalDate.Day()))
	d.SetMonth(uint(originalDate.Month()))
	d.SetYear(uint(originalDate.Year()))
	d.SetHour(uint(originalDate.Hour()))

	return nil
}

// // Process converts the date to the specified timezone, normalizes to midnight, and converts to UTC.
// func (d *Date) Process(timezone string, toUTC bool, toMidnight bool) error {
// 	processedDate, err := swapTimezoneFunc(d.GetDate(), timezone, toUTC, toMidnight)
// 	if err != nil {
// 		return err
// 	}
//
// 	// Set implicit fields.
// 	d.SetProcessedDate(processedDate)
//
// 	// Use the original date to fill those fields.
// 	d.SetDay(uint(d.GetDate().Day()))
// 	d.SetMonth(uint(d.GetDate().Month()))
// 	d.SetYear(uint(d.GetDate().Year()))
// 	d.SetHour(uint(d.GetDate().Hour()))
//
// 	return nil
// }

// ------------------------------
// GETTERS
// ------------------------------

func (d *Date) GetDate() time.Time {
	if d.Date == nil {
		return time.Time{}
	}
	return *d.Date
}

func (d *Date) GetProcessedDate() time.Time {
	if d.ProcessedDate == nil {
		return time.Time{}
	}
	return *d.ProcessedDate
}

func (d *Date) GetDay() uint {
	if d.Day == nil {
		return 1
	}
	return *d.Day
}

func (d *Date) GetMonth() uint {
	if d.Month == nil {
		return 1
	}
	return *d.Month
}

func (d *Date) GetYear() uint {
	if d.Year == nil {
		return 1
	}
	return *d.Year
}

func (d *Date) GetHour() uint {
	if d.Hour == nil {
		return 0
	}
	return *d.Hour
}

// ------------------------------
// SETTERS
// ------------------------------

func (d *Date) SetDate(date time.Time) {
	if d.Date == nil {
		d.Date = new(time.Time)
	}
	*d.Date = date
}

func (d *Date) SetProcessedDate(processedDate time.Time) {
	if d.ProcessedDate == nil {
		d.ProcessedDate = new(time.Time)
	}
	*d.ProcessedDate = processedDate
}

func (d *Date) SetDay(day uint) {
	if d.Day == nil {
		d.Day = new(uint)
	}
	*d.Day = day
}

func (d *Date) SetMonth(month uint) {
	if d.Month == nil {
		d.Month = new(uint)
	}
	*d.Month = month
}

func (d *Date) SetYear(year uint) {
	if d.Year == nil {
		d.Year = new(uint)
	}
	*d.Year = year
}

func (d *Date) SetHour(hour uint) {
	if d.Hour == nil {
		d.Hour = new(uint)
	}
	*d.Hour = hour
}
