package embedcup

// Key is an alias for a string.
//
// It is utilized in all embedcup files to represent JSON and BSON keys constants.
type Key string

// String returns the string representation of the key.
func (key Key) String() string {
	return string(key)
}
