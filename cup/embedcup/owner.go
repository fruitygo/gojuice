// Package embedcup contains the common models and embedded structs used across
// different entities in the Fabriktor. These structs represent shared fields
// and provide common functionality like Getters and Setters.
package embedcup

import "go.mongodb.org/mongo-driver/bson/primitive"

// ------------------------------
// CONSTS
// ------------------------------

const (
	OwnerIDKey OwnerKey = "owner_id"
)

// ------------------------------
// TYPES
// ------------------------------

// OwnerKey is an alias for string, representing a key for owner-related fields.
type OwnerKey = Key

// Owner represents the owner of a document.
// swagger:model
type Owner struct {
	// The MongoDB ID of the owner of the document.
	// example: 5f7b1b9b9b9b9b9b9b9b9b9b
	// required: true
	OwnerID *primitive.ObjectID `bson:"owner_id" json:"owner_id" validate:"nonnilpointer,nonzeropointerelem"`
}

// ------------------------------
// CONSTRUCTOR
// ------------------------------

func NewOwner(ownerID primitive.ObjectID) *Owner {
	x := new(Owner)
	x.SetOwnerID(ownerID)
	return x
}

// ------------------------------
// GETTERS
// ------------------------------
// Copies of the values are returned to avoid the caller to modify the original values.
// The caller can only modify the original values by calling the setters.

func (o *Owner) GetOwnerID() primitive.ObjectID {
	if o.OwnerID == nil {
		return primitive.NilObjectID
	}
	return *o.OwnerID
}

// ------------------------------
// SETTERS
// ------------------------------
// The setters are utilized to set the values of the struct fields.

func (o *Owner) SetOwnerID(id primitive.ObjectID) {
	if o.OwnerID == nil {
		o.OwnerID = new(primitive.ObjectID)
	}
	*o.OwnerID = id
}
