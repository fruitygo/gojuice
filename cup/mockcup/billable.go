package mockcup

import "encoding/json"

const (
	ProductNameKey    = "product_name"
	AppliedTaxesKey   = "applied_taxes"
	QuantityKey       = "quantity"
	QuantifierNameKey = "quantifier_name"
	UnitPriceKey      = "unit_price"
	SubTotalKey       = "sub_total"
	TaxTotalKey       = "tax_total"
	TotalKey          = "total"
	DetailsKey        = "details"

	TotalImplicitSubTotalKey   = "sub_total"
	TotalImplicitTaxDetailsKey = "tax_details"
	TotalImplicitTaxTotalKey   = "tax_total"
	TotalImplicitTotalKey      = "total"

	TaxDetailTaxIDKey    = "tax_id"
	TaxDetailTaxTotalKey = "tax_total"
	TaxDetailTaxNameKey  = "tax_name"

	TaxNameKey          = "name"
	TaxAuthorIDKey      = "author_id"
	TaxDescriptionKey   = "description"
	TaxRateKey          = "rate"
	TaxIDKey            = "tax_id"
	TaxSchemaVersionKey = "schema_version"
	TaxUpdatedAtKey     = "updated_at"
	TaxCreatedAtKey     = "created_at"

	BillNumberKey                     = "number"
	BillStartPeriodKey                = "start_period"
	BillEndPeriodKey                  = "end_period"
	BillYearKey                       = "year"
	BillSalaryEntriesKey              = "salary_entries"
	BillTransportationEntriesKey      = "transportation_entries"
	BillChargeEntriesKey              = "charge_entries"
	BillSupInvoiceEntriesKey          = "sup_invoice_entries"
	BillSalaryTotalKey                = "salary_total"
	BillTransportationTotalKey        = "transportation_total"
	BillChargeTotalKey                = "charge_total"
	BillSupInvoiceTotalKey            = "sup_invoice_total"
	BillTotalKey                      = "total"
	BillUpfrontKey                    = "upfront"
	BillBalanceKey                    = "balance"
	BillTitleKey                      = "title"
	BillTypeKey                       = "bill_type"
	BillStartDateKey                  = "start_date"
	BillEndDateKey                    = "end_date"
	BillAuthorIDKey                   = "author_id"
	BillClientIDKey                   = "client_id"
	BillWorksiteIDKey                 = "worksite_id"
	BillTimezoneKey                   = "timezone"
	BillSalaryTaxesKey                = "salary_taxes"
	BillSupInvoiceTaxesKey            = "sup_invoice_taxes"
	BillChargeTaxesKey                = "charge_taxes"
	BillTopNoteKey                    = "top_note"
	BillExcludeChargedKey             = "exclude_charged"
	BillIsEstimateKey                 = "is_estimate"
	BillIsInternalKey                 = "is_internal"
	BillIncludeTransportationKey      = "include_transportation"
	BillIncludeSalaryKey              = "include_salary"
	BillIncludeSupInvoiceKey          = "include_sup_invoice"
	BillIncludeChargeKey              = "include_charge"
	BillIsCanceledKey                 = "is_canceled"
	BillIsAchievedKey                 = "is_achieved"
	BillUpfrontRateKey                = "upfront_rate"
	BillDueDateKey                    = "due_date"
	BillDueDateStrKey                 = "due_date_str"
	BillDefaultSupplierInvoiceRateKey = "default_supplier_invoice_rate"

	BillTypeFixedValue   = "fixed"
	BillTypeDynamicValue = "dynamic"

	BillChargeQuantifierNameForEquipmentHour = "hour"
	BillChargeQuantifierNameForEquipmentDay  = "day"
	BillChargeQuantifierNameForProduct       = "Boite 50 Lbs"
)

// Mock data for charge emb.Entry.
var ChargeEntryMock = func(itemName, quantifierName string) map[string]interface{} {
	return map[string]interface{}{
		ProductNameKey:    itemName,
		AppliedTaxesKey:   getSlice("TPS", "TVQ"),
		QuantityKey:       json.Number("1.25"),
		QuantifierNameKey: quantifierName,
		UnitPriceKey:      json.Number("4550"),
		SubTotalKey:       json.Number("10000"),
		TaxTotalKey:       json.Number("1400"),
		TotalKey:          json.Number("11400"),
		DetailsKey:        "",
	}
}

// Mock data for credit emb.Entry.
var CreditEntryMock = map[string]interface{}{
	ProductNameKey:    "Clous 3 Pouces",
	AppliedTaxesKey:   getSlice("TPS", "TVQ"),
	QuantityKey:       json.Number("-3.50"),
	QuantifierNameKey: "Boite 50 Lbs",
	UnitPriceKey:      json.Number("4550"),
	SubTotalKey:       json.Number("-100000"),
	TaxTotalKey:       json.Number("-14000"),
	TotalKey:          json.Number("-114000"),
	DetailsKey:        "",
}

// Mock data for supinvoice emb.Entry.
var SupInvoiceEntryMock = map[string]interface{}{
	ProductNameKey:    "Les Entreprises Bob Lafortune Inc.",
	AppliedTaxesKey:   getSlice("TPS", "TVQ"),
	QuantityKey:       json.Number("1"),
	QuantifierNameKey: "",
	UnitPriceKey:      json.Number("4550"),
	SubTotalKey:       json.Number("10000"),
	TaxTotalKey:       json.Number("1400"),
	TotalKey:          json.Number("11400"),
	DetailsKey:        "F183423",
}

// Mock data for the salary emb.Entry.
var SalaryEntryMock = map[string]interface{}{
	ProductNameKey:    "Olivier Bernard",
	AppliedTaxesKey:   getSlice("TPS", "TVQ"),
	QuantityKey:       json.Number("30.50"),
	QuantifierNameKey: "h",
	UnitPriceKey:      json.Number("6000"),
	SubTotalKey:       json.Number("10000"),
	TaxTotalKey:       json.Number("1400"),
	TotalKey:          json.Number("11400"),
	DetailsKey:        "2021-11-01 / 2021-11-07 (32)",
}

// Mock data for emb.TotalImplicit.
var TotalImplicitMock = map[string]interface{}{
	TotalImplicitSubTotalKey: json.Number("10000"),
	TotalImplicitTaxDetailsKey: getSlice(
		TaxDetailMockTPS,
		TaxDetailMockTVQ,
	),
	TotalImplicitTaxTotalKey: json.Number("1400"),
	TotalImplicitTotalKey:    json.Number("11400"),
}

// Mock data for emb.TaxDetail.
var TaxDetailMockTPS = map[string]interface{}{
	TaxDetailTaxIDKey:    "5f7b1b9b9b9b9b9b9b9b9b9b",
	TaxDetailTaxTotalKey: json.Number("2450"),
	TaxDetailTaxNameKey:  "TPS",
}

// Mock data for emb.TaxDetail.
var TaxDetailMockTVQ = map[string]interface{}{
	TaxDetailTaxIDKey:    "5f7b1b9b9b9b9b9b9b9b9b9b",
	TaxDetailTaxTotalKey: json.Number("2450"),
	TaxDetailTaxNameKey:  "TVQ",
}

// Mock data for emb.Tax.
var TaxMock = map[string]interface{}{
	TaxNameKey:          "TPS",
	TaxAuthorIDKey:      "5f7b1b9b9b9b9b9b9b9b9b9b",
	TaxDescriptionKey:   "Taxes sur les produits et services",
	TaxRateKey:          json.Number("5000"),
	TaxIDKey:            "5f7b1b9b9b9b9b9b9b9b9b9b",
	TaxSchemaVersionKey: SchemaVersionValue,
	TaxUpdatedAtKey:     "2020-10-05T10:00:00Z",
	TaxCreatedAtKey:     "2020-10-05T10:00:00Z",
}

// Mock data for model.Bill.
var BillMock = map[string]interface{}{
	IDKey:                        IDValue,
	SchemaVersionKey:             SchemaVersionValue,
	UpdatedAtKey:                 "2020-10-05T10:00:00Z",
	CreatedAtKey:                 "2020-10-05T10:00:00Z",
	BillNumberKey:                json.Number("18342394"),
	BillStartPeriodKey:           json.Number("30"),
	BillEndPeriodKey:             json.Number("35"),
	BillYearKey:                  YearValue,
	BillSalaryEntriesKey:         getSlice(SalaryEntryMock, SalaryEntryMock, SalaryEntryMock, SalaryEntryMock, SalaryEntryMock, SalaryEntryMock, SalaryEntryMock, SalaryEntryMock, SalaryEntryMock, SalaryEntryMock, SalaryEntryMock),
	BillTransportationEntriesKey: getSlice(SalaryEntryMock, SalaryEntryMock, SalaryEntryMock),
	BillChargeEntriesKey: getSlice(
		CreditEntryMock,
		ChargeEntryMock("Clous 2 pouces", BillChargeQuantifierNameForProduct),
		ChargeEntryMock("Vis à tôle bleue", BillChargeQuantifierNameForProduct),
		ChargeEntryMock("Clous 3 pouces", BillChargeQuantifierNameForProduct),
		ChargeEntryMock("Clous 4 pouces", BillChargeQuantifierNameForProduct),
		ChargeEntryMock("Clous 5 pouces", BillChargeQuantifierNameForProduct),
		ChargeEntryMock("Clous 6 pouces", BillChargeQuantifierNameForProduct),
		ChargeEntryMock("Clous à finir", BillChargeQuantifierNameForProduct),
		ChargeEntryMock("RedLift 25", BillChargeQuantifierNameForEquipmentDay),
		ChargeEntryMock("Plafolift 4", BillChargeQuantifierNameForEquipmentHour),
		ChargeEntryMock("Roulette de tape", BillChargeQuantifierNameForProduct),
		ChargeEntryMock("RedLift 55", BillChargeQuantifierNameForEquipmentDay),
		ChargeEntryMock("Plafolift Électrique", BillChargeQuantifierNameForEquipmentHour),
	),
	BillSupInvoiceEntriesKey:          getSlice(SupInvoiceEntryMock, SupInvoiceEntryMock, SupInvoiceEntryMock, SupInvoiceEntryMock, SupInvoiceEntryMock),
	BillSalaryTotalKey:                TotalImplicitMock,
	BillTransportationTotalKey:        TotalImplicitMock,
	BillChargeTotalKey:                TotalImplicitMock,
	BillSupInvoiceTotalKey:            TotalImplicitMock,
	BillTotalKey:                      TotalImplicitMock,
	BillUpfrontKey:                    json.Number("2000"),
	BillBalanceKey:                    json.Number("6000"),
	BillTitleKey:                      "Construction d'un Poulailler de 200 Pieds & 2 Étages",
	BillTypeKey:                       BillTypeFixedValue, // Switch this field for testing purposes.
	BillStartDateKey:                  "2020-10-05T14:00:00Z",
	BillEndDateKey:                    "2020-10-05T14:00:00Z",
	BillAuthorIDKey:                   "5f7b1b9b9b9b9b9b9b9b9b9b",
	BillClientIDKey:                   "5f7b1b9b9b9b9b9b9b9b9b9b",
	BillWorksiteIDKey:                 "5f7b1b9b9b9b9b9b9b9b9b9b",
	BillTimezoneKey:                   "America/New_York",
	BillSalaryTaxesKey:                getSlice(TaxMock),
	BillSupInvoiceTaxesKey:            getSlice(TaxMock),
	BillChargeTaxesKey:                getSlice(TaxMock),
	BillTopNoteKey:                    "Ceci est une note.",
	BillExcludeChargedKey:             true,
	BillIsEstimateKey:                 true, // Switch this field for testing purposes.
	BillIsInternalKey:                 true,
	BillIncludeTransportationKey:      true,
	BillIncludeSalaryKey:              true,
	BillIncludeSupInvoiceKey:          true,
	BillIncludeChargeKey:              true,
	BillIsCanceledKey:                 false,
	BillUpfrontRateKey:                json.Number("2000"),
	BillDueDateKey:                    "2020-10-05T14:00:00Z",
	BillDueDateStrKey:                 "30 days post-completion",
	BillIsAchievedKey:                 true,                // Switch this field for testing purposes.
	BillDefaultSupplierInvoiceRateKey: json.Number("4000"), // 4%
}
