// Description: This package provides mock data for testing purposes.
// It provides functions to mock the behavior of Go's JSON decoder, specifically when `json.Number` is enabled.
// This allows for precise testing of code that relies on the exact data structures and types produced by the standard JSON decoder.
//
// This package is primarily used in development and staging environments to mimic real HTTP requests and their decoded responses,
// simplifying testing without requiring actual network calls. In production, real http requests are used.
//
// When decoding JSON with `json.Number` enabled:
// - Numbers are decoded as `json.Number`
// - Slices are decoded as `[]interface{}`
// - Objects, and maps, are decoded as `map[string]interface{}`
//
// Example:
//
//	{
//	    "name": "Alice",
//	    "age": 30,
//	    "score": "1234567890123456789",
//	    "scores": [95.5, 88, 92.1]
//	}
//
// Decodes to:
// - `name`: `string`
// - `age`: `json.Number`
// - `score`: `json.Number`
// - `scores`: `[]interface{}` (each element is `json.Number`)
package mockcup

import "encoding/json"

// Define an alias for int and uint utilized as keys.
type (
	intKey = string
)

const (
	// General keys.
	IDKey            = "_id"
	SchemaVersionKey = "schema_version"
	UpdatedAtKey     = "updated_at"
	CreatedAtKey     = "created_at"

	// General values.
	CurrencyValue      = "USD"
	SchemaVersionValue = "v1"
	YearValue          = json.Number("2024")
	IDValue            = "5f6d4b9b9c6f9f0001b9c6f9"
)

// getSlice creates a slice of interfaces from the given elements.
//
// All elements in the slice must have the same underlying type.
func getSlice(elements ...interface{}) []interface{} {
	slice := make([]interface{}, len(elements))
	copy(slice, elements)
	return slice
}
