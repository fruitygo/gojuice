package mockcup

import (
	"encoding/json"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Define constants for all keys.
const (
	// General keys.
	OwnerIDKey = "owner_id"

	// Payroll keys.
	PayrollYearKey                           = "year"
	PayrollPeriodsKey                        = "periods"
	PayrollCurrencyCodeKey                   = "currency_code"
	PayrollHourbankIDKey                     = "hourbank_id"
	PayrollEmployeeIDKey                     = "employee_id"
	PayrollSummaryIDsKey                     = "summary_ids"
	PayrollIsBankedKey                       = "is_banked"
	PayrollWorkAmountKey                     = "work_amount"
	PayrollTransportAmountKey                = "transport_amount"
	PayrollGrossPayrollAmountKey             = "gross_payroll_amount"
	PayrollDeductionAmountKey                = "deduction_amount"
	PayrollContributionAmountKey             = "contribution_amount"
	PayrollNetPayrollAmountKey               = "net_payroll_amount"
	PayrollIsProblematicKey                  = "is_problematic"
	PayrollEmployeeConfigIDKey               = "employee_config_id"
	PayrollDeductionAndContributionAmountKey = "deduction_and_contribution_amount"

	// DeductionEntry keys.
	DeductionEntryCurrencyCodeKey            = "currency_code"
	DeductionEntryEmployeeIDKey              = "employee_id"
	DeductionEntryYearKey                    = "year"
	DeductionEntryPeriodKey                  = "period"
	DeductionEntryPayrollIDKey               = "payroll_id"
	DeductionEntryDeductionAmountsKey        = "deduction_amounts"
	DeductionEntryContributionAmountsKey     = "contribution_amounts"
	DeductionEntryDeductionTotalAmountKey    = "deduction_total_amount"
	DeductionEntryContributionTotalAmountKey = "contribution_total_amount"
	DeductionEntryTotalAmountKey             = "total_amount"

	// DeductionAmount keys.
	DeductionAmountNameKey              = "name"
	DeductionAmountDeductionIDKey       = "deduction_id"
	DeductionAmountAmountKey            = "amount"
	DeductionAmountIsSubContributionKey = "is_sub_contribution"

	// DeductionAggregation keys.
	DeductionAggregationOwnerIDKey                      = "owner_id"
	DeductionAggregationCurrencyCodeKey                 = "currency_code"
	DeductionAggregationYearKey                         = "year"
	DeductionAggregationPeriodsKey                      = "periods"
	DeductionAggregationDeductionEntryIDsKey            = "deduction_entry_ids"
	DeductionAggregationDeductionTotalPerPeriodKey      = "deduction_total_per_period"
	DeductionAggregationContributionTotalPerPeriodKey   = "contribution_total_per_period"
	DeductionAggregationDeductionAmountsPerPeriodKey    = "deduction_amounts_per_period"
	DeductionAggregationContributionAmountsPerPeriodKey = "contribution_amounts_per_period"
	DeductionAggregationTotalPerDeductionKey            = "total_per_deduction"
	DeductionAggregationTotalPerContributionKey         = "total_per_contribution"
	DeductionAggregationDeductionAmountsKey             = "deduction_amounts"
	DeductionAggregationContributionAmountsKey          = "contribution_amounts"
	DeductionAggregationDeductionTotalAmountKey         = "deduction_total_amount"
	DeductionAggregationContributionTotalAmountKey      = "contribution_total_amount"
	DeductionAggregationTotalAmountKey                  = "total_amount"
	DeductionAggregationEmployeeIDsKey                  = "employee_ids"
	DeductionAggregationTotalPerGroupKey                = "total_per_group"
)

// Mock data for the model.Payroll.
var (
	PayrollMock = map[string]interface{}{
		IDKey:                                    IDValue,
		SchemaVersionKey:                         SchemaVersionValue,
		UpdatedAtKey:                             time.Now().UTC().Format(time.RFC3339),
		CreatedAtKey:                             time.Now().UTC().Format(time.RFC3339),
		PayrollHourbankIDKey:                     primitive.NewObjectID().Hex(),
		PayrollEmployeeIDKey:                     primitive.NewObjectID().Hex(),
		PayrollSummaryIDsKey:                     getSlice(primitive.NewObjectID().Hex(), primitive.NewObjectID().Hex()),
		PayrollIsBankedKey:                       true,                  // Switch this field for testing purposes.
		PayrollIsProblematicKey:                  false,                 // Switch this field for testing purposes.
		PayrollWorkAmountKey:                     json.Number("150000"), // 1500.00
		PayrollTransportAmountKey:                json.Number("50000"),  // 500.00
		PayrollGrossPayrollAmountKey:             json.Number("200000"), // 2000.00
		PayrollDeductionAmountKey:                json.Number("30000"),  // 300.00
		PayrollContributionAmountKey:             json.Number("20000"),  // 200.00
		PayrollDeductionAndContributionAmountKey: json.Number("50000"),  // 500.00
		PayrollNetPayrollAmountKey:               json.Number("170000"), // 1700.00
		PayrollEmployeeConfigIDKey:               primitive.NewObjectID().Hex(),
		OwnerIDKey:                               primitive.NewObjectID().Hex(),
		PayrollYearKey:                           YearValue,
		PayrollPeriodsKey:                        getSlice(json.Number("1"), json.Number("2"), json.Number("3"), json.Number("4")),
		PayrollCurrencyCodeKey:                   CurrencyValue,
	}
)

// Mock data for the model.DeductionEntry.
var DeductionEntryMock = func(period json.Number) map[string]interface{} {
	return map[string]interface{}{
		IDKey:                                    primitive.NewObjectID().Hex(),
		SchemaVersionKey:                         SchemaVersionValue,
		UpdatedAtKey:                             time.Now().UTC().Format(time.RFC3339),
		CreatedAtKey:                             time.Now().UTC().Format(time.RFC3339),
		OwnerIDKey:                               primitive.NewObjectID().Hex(),
		DeductionEntryCurrencyCodeKey:            CurrencyValue,
		DeductionEntryEmployeeIDKey:              primitive.NewObjectID().Hex(),
		DeductionEntryYearKey:                    YearValue,
		DeductionEntryPeriodKey:                  period,
		DeductionEntryPayrollIDKey:               primitive.NewObjectID().Hex(),
		DeductionEntryDeductionAmountsKey:        DeductionAmountsMock,
		DeductionEntryContributionAmountsKey:     ContributionAmountsMock,
		DeductionEntryDeductionTotalAmountKey:    json.Number("8900"),
		DeductionEntryContributionTotalAmountKey: json.Number("2000"),
		DeductionEntryTotalAmountKey:             json.Number("10900"),
	}
}

var DeductionAmountsMock = getSlice(
	map[string]interface{}{
		DeductionAmountNameKey:              "Deduction 1",
		DeductionAmountDeductionIDKey:       primitive.NewObjectID().Hex(),
		DeductionAmountAmountKey:            json.Number("4000"),
		DeductionAmountIsSubContributionKey: false,
	},
	map[string]interface{}{
		DeductionAmountNameKey:              "Deduction 2",
		DeductionAmountDeductionIDKey:       primitive.NewObjectID().Hex(),
		DeductionAmountAmountKey:            json.Number("4000"),
		DeductionAmountIsSubContributionKey: false,
	},
)

var ContributionAmountsMock = getSlice(
	map[string]interface{}{
		DeductionAmountNameKey:              "Contribution 1",
		DeductionAmountDeductionIDKey:       primitive.NewObjectID().Hex(),
		DeductionAmountAmountKey:            json.Number("1000"),
		DeductionAmountIsSubContributionKey: false,
	},
	map[string]interface{}{
		DeductionAmountNameKey:              "Contribution 2",
		DeductionAmountDeductionIDKey:       primitive.NewObjectID().Hex(),
		DeductionAmountAmountKey:            json.Number("1000"),
		DeductionAmountIsSubContributionKey: false,
	},
	map[string]interface{}{
		DeductionAmountNameKey:              "Sub-Contribution 1",
		DeductionAmountDeductionIDKey:       primitive.NewObjectID().Hex(),
		DeductionAmountAmountKey:            json.Number("400"),
		DeductionAmountIsSubContributionKey: true,
	},
	map[string]interface{}{
		DeductionAmountNameKey:              "Sub-Contribution 2",
		DeductionAmountDeductionIDKey:       primitive.NewObjectID().Hex(),
		DeductionAmountAmountKey:            json.Number("500"),
		DeductionAmountIsSubContributionKey: true,
	},
)

// Mocked data for the model.DeductionAggregation.
var (
	DeductionAggregationMock = map[string]interface{}{
		IDKey:                                             IDValue,
		SchemaVersionKey:                                  SchemaVersionValue,
		UpdatedAtKey:                                      time.Now().UTC().Format(time.RFC3339),
		CreatedAtKey:                                      time.Now().UTC().Format(time.RFC3339),
		DeductionAggregationOwnerIDKey:                    primitive.NewObjectID().Hex(),
		DeductionAggregationCurrencyCodeKey:               CurrencyValue,
		DeductionAggregationYearKey:                       YearValue,
		DeductionAggregationPeriodsKey:                    getSlice(json.Number("1"), json.Number("2")),
		DeductionAggregationDeductionEntryIDsKey:          getSlice(primitive.NewObjectID().Hex(), primitive.NewObjectID().Hex()),
		DeductionAggregationDeductionTotalPerPeriodKey:    map[intKey]interface{}{"1": json.Number("8900"), "2": json.Number("8900")},
		DeductionAggregationContributionTotalPerPeriodKey: map[intKey]interface{}{"1": json.Number("2000"), "2": json.Number("2000")},
		DeductionAggregationDeductionAmountsPerPeriodKey: map[intKey]interface{}{
			"1": DeductionAmountsMock,
			"2": DeductionAmountsMock,
		},
		DeductionAggregationContributionAmountsPerPeriodKey: map[intKey]interface{}{
			"1": ContributionAmountsMock,
			"2": ContributionAmountsMock,
		},
		DeductionAggregationTotalPerDeductionKey: map[string]interface{}{
			"Deduction 1":        json.Number("8000"),
			"Deduction 2":        json.Number("8000"),
			"Sub-Contribution 1": json.Number("800"),
			"Sub-Contribution 2": json.Number("1000"),
		},
		DeductionAggregationTotalPerContributionKey: map[string]interface{}{
			"Contribution 1": json.Number("2000"),
			"Contribution 2": json.Number("2000"),
		},
		DeductionAggregationTotalPerGroupKey: map[string]interface{}{
			"Federal": json.Number("5000"),
			"State":   json.Number("13600"),
			"Other":   json.Number("5000"),
		},
		DeductionAggregationDeductionAmountsKey:        append(DeductionAmountsMock, DeductionAmountsMock...),
		DeductionAggregationContributionAmountsKey:     append(ContributionAmountsMock, ContributionAmountsMock...),
		DeductionAggregationDeductionTotalAmountKey:    json.Number("17800"),
		DeductionAggregationContributionTotalAmountKey: json.Number("5800"),
		DeductionAggregationTotalAmountKey:             json.Number("23600"),
		DeductionAggregationEmployeeIDsKey:             getSlice(primitive.NewObjectID().Hex(), primitive.NewObjectID().Hex()),
	}
)
