// Description: This package contains the structures to be imported and used by other packages.
package structcup

import "net/http"

// ------------------------------
// TYPES
// ------------------------------

// AuthResponse represents the response from an authentication operation.
//
// It includes a message and user information.
type AuthResponse struct {
	Message string                 `json:"message"`
	User    map[string]interface{} `json:"user"`
}

// ErrorResponse represents a standard error response from the application.
//
// It includes an error code and a message.
type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// HTTPResponse holds the response and error from an HTTP request.
type HTTPResponse struct {
	Resp *http.Response
	Err  error
}

// ResponseData represents a standard response from the application.
//
// It includes a message and an ID.
type ResponseData struct {
	Message string `json:"message"`
	ID      string `json:"_id"`
}

// SearchResponse represents the response from a search operation.
//
// It includes results, total count, current page, and last page.
type SearchResponse struct {
	Results     []interface{} `json:"results"`
	TotalCount  int32         `json:"total_count"`
	CurrentPage int           `json:"current_page"`
	LastPage    int           `json:"last_page"`
}
