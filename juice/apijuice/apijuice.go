// Description: This package provides utility functions for writing JSON responses and checking mandatory query parameters.
package apijuice

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/fruitygo/gojuice/cup/structcup"
	"gitlab.com/fruitygo/gojuice/juice/slicejuice"
)

// ------------------------------
// TYPES
// ------------------------------

// QueryParam represents a query parameter that can be checked for mandatory presence and accepted values.
//
// Key is the name of the query parameter. This is the key that will be used to look up the value in the request.
//
// Value should contain a default value for the query parameter, or a getter function to obtain the value.
// You are free to use this field as you wish.
//
// MultipleValuesAllowed is a boolean that indicates whether multiple values are allowed for the same key.
// If this is set to true, the query parameter can have multiple values in the request.
// If this is set to false, the query parameter should only have one value in the request.
//
// AcceptedValues is a slice of strings that contains the accepted values for the query parameter.
// If this is not nil, the value of the query parameter in the request must be one of these values.
// Set it to nil if you do not want to check for accepted values and allow dynamic values.
type QueryParam struct {
	Key                   string
	Value                 string
	MultipleValuesAllowed bool
	AcceptedValues        []string
	Optional              bool
}

// ------------------------------
// FUNCTION VARIABLES (for testing)
// ------------------------------

var (
	gzipWriteFunc = (*gzip.Writer).Write
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// BuildQueryString constructs a query parameter string from a variadic number of key-value pairs.
//
// The values passed should not already be URL-escaped, as the function will handle escaping.
//
// If an uneven number of parameters is passed, the function will use "N/A" as the value for the last key.
//
// Example usage:
//
//	query := BuildQueryParams("q", "value1", "name", "John Doe", "age")
//	fmt.Println(query) // Output: ?q=value1&name=John+Doe&age=N%2FA
func BuildQueryString(params ...string) string {
	if len(params) == 0 {
		return ""
	}

	// Append N/A in case number of parameters is odd.
	if len(params)%2 != 0 {
		params = append(params, "N/A")
	}

	// Define a slice to store query parameters.
	var queryParams []string

	// Iterate over the parameters and append them to the slice.
	for i := 0; i < len(params); i += 2 {
		queryParams = append(queryParams, fmt.Sprintf("%s=%s", url.QueryEscape(params[i]), url.QueryEscape(params[i+1])))
	}

	// Return the joined query parameters.
	return "?" + strings.Join(queryParams, "&")
}

// WriteJSONResponse writes the JSON data to the response writer with appropriate headers.
//
// The Content-Type header is set to application/json.
//
// The Content-Length header is set with the length of the uncompressed data.
//
// The Content-Length is not set if the Content-Encoding header is set to gzip.
//
// The status code is set based on the provided code parameter.
//
// If the data cannot be serialized to JSON, an error is returned.
//
// If the uncompressed data cannot be written to the response writer, an error is returned.
func WriteJSONResponse(w http.ResponseWriter, data interface{}, code int, escapeHTML bool) error {
	// If the data is nil, set the Content-Type header, write the status code, and return.
	if data == nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(code)
		return nil
	}

	// Create a buffer to store JSON data.
	buf := &bytes.Buffer{}

	// Create a new JSON encoder that writes to the buffer.
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(escapeHTML)

	// Encode the data to the buffer.
	if err := enc.Encode(data); err != nil {
		return err
	}

	// Set the Content-Length header with the length of the uncompressed data.
	// Do not set it if the Content-Encoding header is set to gzip to allow chunked transfer encoding.
	if w.Header().Get("Content-Encoding") != "gzip" {
		w.Header().Set("Content-Length", strconv.Itoa(len(buf.Bytes())))
	}

	// Set the Content-Type header to indicate JSON response.
	w.Header().Set("Content-Type", "application/json")

	// Set the appropriate status based on the provided code.
	w.WriteHeader(code)

	// Write the uncompressed JSON data to the response writer.
	if _, err := w.Write(buf.Bytes()); err != nil {
		return err
	}

	return nil
}

// WriteErrorResponse writes an error response to the response writer with appropriate headers.
//
// It does NOT return an error because it is used to write an error response.
//
// Instead, if an error occurs internally, it writes the error to the response writer with http.Error.
func WriteErrorResponse(w http.ResponseWriter, errorCode int, escapeHTML bool) {
	errorResponse := &structcup.ErrorResponse{
		Code:    errorCode,
		Message: http.StatusText(errorCode),
	}

	if err := WriteJSONResponse(w, errorResponse, errorCode, escapeHTML); err != nil {
		http.Error(w, strings.ToLower(http.StatusText(http.StatusInternalServerError)), http.StatusInternalServerError)
	}
}

// WriteGzipJSONResponse compresses the JSON data and writes it to the response writer with appropriate headers.
//
// The Content-Encoding header is set to gzip and the Content-Type header is set to application/json.
//
// The Content-Length header is set with the length of the compressed data.
//
// The status code is set based on the provided code parameter.
//
// If the data cannot be serialized to JSON, an error is returned.
//
// If the compressed data cannot be written to the response writer, an error is returned.
//
// This method does NOT check for the Accept-Encoding header.
//
// This method overrides the Content-Length header with the length of the compressed data.
//
// Use this method only if you are sure that the client can handle gzip compression.
func WriteGzipJSONResponse(w http.ResponseWriter, data interface{}, code int, escapeHTML bool) error {
	if data == nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(code)
		return nil
	}

	// Create a buffer to store JSON data.
	buf := &bytes.Buffer{}

	// Create a new JSON encoder that writes to the buffer.
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(escapeHTML)

	// Encode the data to the buffer.
	if err := enc.Encode(data); err != nil {
		return err
	}

	// Convert the buffer to a byte slice.
	jsonData := buf.Bytes()

	// Compress the JSON data using gzip.
	var gbuf bytes.Buffer
	gz := gzip.NewWriter(&gbuf)

	// Write the compressed JSON data to the gzip writer.
	if _, err := gzipWriteFunc(gz, jsonData); err != nil {
		gz.Close()
		return err
	}
	gz.Close()

	// Set the Content-Encoding header to indicate gzip compression.
	w.Header().Set("Content-Encoding", "gzip")

	// Set the Content-Type header to indicate JSON response.
	w.Header().Set("Content-Type", "application/json")

	// Set the Content-Length header with the length of the compressed data.
	w.Header().Set("Content-Length", strconv.Itoa(gbuf.Len()))

	// Set the appropriate status based on the provided code.
	w.WriteHeader(code)

	// Write the compressed JSON data to the response writer.
	if _, err := w.Write(gbuf.Bytes()); err != nil {
		return err
	}

	return nil
}

// WriteGzipErrorResponse writes an error response to the response writer with appropriate headers.
//
// It does NOT return an error because it is used to write an error response.
//
// Instead, if an error occurs internally, it writes the error to the response writer with http.Error.
//
// This method does NOT check for the Accept-Encoding header.
//
// This method overrides the Content-Length header with the length of the compressed data.
//
// Use this method only if you are sure that the client can handle gzip compression.
func WriteGzipErrorResponse(w http.ResponseWriter, errorCode int, escapeHTML bool) {
	errorResponse := &structcup.ErrorResponse{
		Code:    errorCode,
		Message: http.StatusText(errorCode),
	}

	// Write the error response to the response writer.
	if err := WriteGzipJSONResponse(w, errorResponse, errorCode, escapeHTML); err != nil {
		http.Error(w, strings.ToLower(http.StatusText(http.StatusInternalServerError)), http.StatusInternalServerError)
	}
}

// CheckMandatoryQueryParams is a function that validates the presence and values of mandatory query parameters in a given request.
// It takes a map of query parameters and a variadic slice of QueryParam pointers as arguments.
//
// The function iterates over each QueryParam in the mandatoryQueryParams slice and checks if its key is present in the queryParams map.
// If the key is not present and the Optional field of the QueryParam is set to false, an error is returned indicating that the query parameter is mandatory.
// If the key is not present but the Optional field of the QueryParam is set to true, the function skips the checks and continues with the next query parameter.
//
// For each query parameter that is present, the function checks if its values are valid according to the following rules:
//   - The value cannot be an empty string.
//   - The value must be one of the accepted values if the AcceptedValues field of the QueryParam is not nil and has at least one element.
//   - The value cannot be a duplicate of a previously seen value for the same key.
//   - If the MultipleValuesAllowed field of the QueryParam is set to false, the query parameter cannot have more than one value.
//
// If any of these checks fail, an error is returned with a message indicating the nature of the failure.
// If all checks pass, the function returns nil, indicating that all mandatory query parameters are present and their values are valid.
func CheckMandatoryQueryParams(queryParams map[string][]string, mandatoryQueryParams ...*QueryParam) error {
	for _, param := range mandatoryQueryParams {
		values, ok := queryParams[param.Key]
		if !ok {
			if !param.Optional {
				return fmt.Errorf("CheckMandatoryQueryParams() query parameter %s is mandatory", param.Key)
			}
			continue
		}

		// Define a map to keep track of seen values.
		seen := make(map[string]bool)

		// Check if the values are empty strings or duplicates, or if they are not in the accepted values list.
		for _, value := range values {
			if value == "" {
				return fmt.Errorf("CheckMandatoryQueryParams() query parameter %s cannot be an empty string", param.Key)
			}

			if len(param.AcceptedValues) > 0 {
				if !slicejuice.ContainsStringMap(param.AcceptedValues, value) {
					return fmt.Errorf("CheckMandatoryQueryParams() query parameter %s should have one of the accepted values: %v", param.Key, param.AcceptedValues)
				}
			}

			if seen[value] {
				return fmt.Errorf("CheckMandatoryQueryParams() query parameter %s should not have duplicate values", param.Key)
			}

			seen[value] = true
		}

		if len(values) > 1 && !param.MultipleValuesAllowed {
			return fmt.Errorf("CheckMandatoryQueryParams() mandatory query parameter %s should not have multiple values", param.Key)
		}
	}

	return nil
}

// ServeHTML writes an HTML response to the provided http.ResponseWriter with the given HTML content and HTTP status code.
//
// Parameters:
//   - w: http.ResponseWriter to write the response to.
//   - htmlContent: HTML content to be sent as the response payload.
//   - code: HTTP status code to be set in the response.
func ServeHTML(w http.ResponseWriter, htmlContent io.Reader, code int) {
	// Set Content-Type header.
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	// If htmlContent is nil, return internal server error.
	if htmlContent == nil {
		WriteErrorResponse(w, http.StatusInternalServerError, false)
		return
	}

	// Read the HTML content.
	html, err := io.ReadAll(htmlContent)
	if err != nil {
		WriteErrorResponse(w, http.StatusInternalServerError, false)
		return
	}

	// Write the status code.
	w.WriteHeader(code)

	// Write the HTML content to the response writer.
	_, err = w.Write(html)
	if err != nil {
		WriteErrorResponse(w, http.StatusInternalServerError, false)
		return
	}
}
