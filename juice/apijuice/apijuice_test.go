package apijuice

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"testing"

	"gitlab.com/fruitygo/gojuice/cup/structcup"
	"gitlab.com/fruitygo/gojuice/testhelpers"
	"gitlab.com/fruitygo/gojuice/testhelpers/mocks"
)

const jsonData = `{"valid":"json", "symbols":"&<>"}`

func TestBuildQueryString(t *testing.T) {
	tests := []struct {
		name   string
		params []string
		want   string
	}{
		{
			name:   "Even number of parameters",
			params: []string{"param1", "val1", "param2", "val2"},
			want:   "?param1=val1&param2=val2",
		},
		{
			name:   "Odd number of parameters",
			params: []string{"param1", "val1", "param2"},
			want:   "?param1=val1&param2=N%2FA",
		},
		{
			name:   "Many parameters",
			params: []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"},
			want:   "?a=b&c=d&e=f&g=h&i=j&k=l&m=n&o=p&q=r&s=t&u=v&w=x&y=z",
		},
		{
			name:   "Single parameter",
			params: []string{"param1"},
			want:   "?param1=N%2FA",
		},
		{
			name:   "No parameters",
			params: []string{},
			want:   "",
		},
		{
			name:   "Numbers",
			params: []string{"1", "2", "3", "4"},
			want:   "?1=2&3=4",
		},
		{
			name:   "Symbols",
			params: []string{"@", "#", "$", "%"},
			want:   "?%40=%23&%24=%25",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BuildQueryString(tt.params...); got != tt.want {
				t.Errorf("BuildQueryString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWriteJSONResponse(t *testing.T) {
	var (
		jsonDataByteCount        = "44"
		escapedJsonDataByteCount = "59"
	)

	type args struct {
		rw         *mocks.MockResponseWriter
		data       interface{}
		code       int
		escapeHTML bool
	}
	tests := []struct {
		name      string
		args      args
		wantErr   bool
		checkMock func(*mocks.MockResponseWriter) error
	}{
		{
			name: "Nil data",
			args: args{
				rw:   mocks.NewMockResponseWriter(),
				data: nil,
				code: 123,
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// The Content-Type header should have been set.
				header := "Content-Type"
				values := mrw.HeaderMap.Values(header)
				if len(values) < 1 {
					return testhelpers.ErrMissingHeader(header, "application/json")
				}
				if values[0] != "application/json" {
					return testhelpers.ErrUnexpectedHeader(header, values[0])
				}
				if len(values) > 1 {
					var err error
					for i := 1; i < len(values); i++ {
						err = errors.Join(testhelpers.ErrUnexpectedHeader(header, values[i]))
					}
					return err
				}

				// The provided status code (123) should have been set.
				if mrw.StatusCode != 123 {
					return testhelpers.ErrUnexpectedStatusCode(123, mrw.StatusCode)
				}

				return nil
			},
		},
		{
			name: "Fail to Encode data",
			args: args{
				rw:   mocks.NewMockResponseWriter(),
				data: make(chan int), // Encoder can't Encode a channel, this will make Encode() return an error.
			},
			wantErr: true,
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				return nil // Nothing to check.
			},
		},
		{
			name: "Fail to Write",
			args: args{
				rw: func() *mocks.MockResponseWriter {
					rw := mocks.NewMockResponseWriter()
					rw.WriteErr = true
					return rw
				}(),
				data: "data",
				code: 123,
			},
			wantErr: true,
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				return nil // Nothing to check.
			},
		},
		{
			name: "Success without escapeHTML",
			args: args{
				rw:         mocks.NewMockResponseWriter(),
				data:       jsonData,
				code:       123,
				escapeHTML: false,
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// The Content-Type header should have been set.
				ctHeader := "Content-Type"
				ctValues := mrw.HeaderMap.Values(ctHeader)
				if len(ctValues) < 1 {
					return testhelpers.ErrMissingHeader(ctHeader, "application/json")
				}
				if ctValues[0] != "application/json" {
					return testhelpers.ErrUnexpectedHeader(ctHeader, ctValues[0])
				}
				if len(ctValues) > 1 {
					var err error
					for i := 1; i < len(ctValues); i++ {
						err = errors.Join(testhelpers.ErrUnexpectedHeader(ctHeader, ctValues[i]))
					}
					return err
				}

				// The Content-Lenth header should get set if the gzip header doesn't exist.
				clHeader := "Content-Length"
				clValues := mrw.HeaderMap.Values(clHeader)
				if len(clValues) < 1 {
					return testhelpers.ErrMissingHeader(clHeader, jsonDataByteCount)
				}
				if clValues[0] != jsonDataByteCount {
					return testhelpers.ErrUnexpectedHeader(clHeader, clValues[0])
				}
				if len(clValues) > 1 {
					var err error
					for i := 1; i < len(clValues); i++ {
						err = errors.Join(testhelpers.ErrUnexpectedHeader(clHeader, clValues[i]))
					}
					return err
				}

				// The provided status code (123) should have been set.
				if mrw.StatusCode != 123 {
					return testhelpers.ErrUnexpectedStatusCode(123, mrw.StatusCode)
				}

				// The encoded json should have been written to the Body.
				decoder := json.NewDecoder(&mrw.Body)
				var decodedData string
				_ = decoder.Decode(&decodedData)
				if decodedData != jsonData {
					return testhelpers.ErrUnexpectedData(jsonData, decodedData)
				}

				return nil
			},
		},
		{
			name: "Success without escapeHTML and with gzip",
			args: args{
				rw: func() *mocks.MockResponseWriter {
					rw := mocks.NewMockResponseWriter()
					rw.HeaderMap.Set("Content-Encoding", "gzip")
					return rw
				}(),
				data:       jsonData,
				code:       123,
				escapeHTML: false,
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// The Content-Type ctHeader should have been set.
				ctHeader := "Content-Type"
				ctValues := mrw.HeaderMap.Values(ctHeader)
				if len(ctValues) < 1 {
					return testhelpers.ErrMissingHeader(ctHeader, "application/json")
				}
				if ctValues[0] != "application/json" {
					return testhelpers.ErrUnexpectedHeader(ctHeader, ctValues[0])
				}
				if len(ctValues) > 1 {
					var err error
					for i := 1; i < len(ctValues); i++ {
						err = errors.Join(testhelpers.ErrUnexpectedHeader(ctHeader, ctValues[i]))
					}
					return err
				}

				// The Content-Encoding header should be set to gzip.
				ceHeader := "Content-Encoding"
				if mrw.HeaderMap.Values(ceHeader)[0] != "gzip" {
					return testhelpers.ErrMissingHeader(ceHeader, "gzip")
				}

				// The Content-Length header shouldn't get set if the gzip header exists.
				clHeader := "Content-Length"
				clValues := mrw.HeaderMap.Values(clHeader)
				if len(clValues) != 0 {
					return testhelpers.ErrUnexpectedHeader(clHeader, clValues[0])
				}

				// The provided status code (123) should have been set.
				if mrw.StatusCode != 123 {
					return testhelpers.ErrUnexpectedStatusCode(123, mrw.StatusCode)
				}

				// The encoded json should have been written to the Body.
				decoder := json.NewDecoder(&mrw.Body)
				var decodedData string
				_ = decoder.Decode(&decodedData)
				if decodedData != jsonData {
					return testhelpers.ErrUnexpectedData(jsonData, decodedData)
				}

				return nil
			},
		},
		{
			name: "Success with escapeHTML",
			args: args{
				rw:         mocks.NewMockResponseWriter(),
				data:       jsonData,
				code:       123,
				escapeHTML: true,
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// The Content-Type header should have been set.
				header := "Content-Type"
				values := mrw.HeaderMap.Values(header)
				if len(values) < 1 {
					return testhelpers.ErrMissingHeader(header, "application/json")
				}
				if values[0] != "application/json" {
					return testhelpers.ErrUnexpectedHeader(header, values[0])
				}
				if len(values) > 1 {
					var err error
					for i := 1; i < len(values); i++ {
						err = errors.Join(testhelpers.ErrUnexpectedHeader(header, values[i]))
					}
					return err
				}

				clHeader := "Content-Length"
				clValues := mrw.HeaderMap.Values(clHeader)
				if len(clValues) < 1 {
					return testhelpers.ErrMissingHeader(clHeader, escapedJsonDataByteCount)
				}
				if clValues[0] != escapedJsonDataByteCount {
					return testhelpers.ErrUnexpectedHeader(clHeader, clValues[0])
				}
				if len(clValues) > 1 {
					var err error
					for i := 1; i < len(clValues); i++ {
						err = errors.Join(testhelpers.ErrUnexpectedHeader(clHeader, clValues[i]))
					}
					return err
				}

				// The provided status code (123) should have been set.
				if mrw.StatusCode != 123 {
					return testhelpers.ErrUnexpectedStatusCode(123, mrw.StatusCode)
				}

				// The encoded json should have been written to the Body.
				decoder := json.NewDecoder(&mrw.Body)
				var decodedData string
				_ = decoder.Decode(&decodedData)
				if decodedData != jsonData {
					return testhelpers.ErrUnexpectedData(jsonData, decodedData)
				}

				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := WriteJSONResponse(tt.args.rw, tt.args.data, tt.args.code, tt.args.escapeHTML); (err != nil) != tt.wantErr {
				t.Errorf("WriteJSONResponse() error = %v, wantErr %v", err, tt.wantErr)
			}

			if err := tt.checkMock(tt.args.rw); err != nil {
				t.Errorf("mock check failed: %v", err)
			}
		})
	}
}

func TestWriteErrorResponse(t *testing.T) {
	type args struct {
		errorCode int
		rw        *mocks.MockResponseWriter
	}
	tests := []struct {
		name      string
		args      args
		checkMock func(*mocks.MockResponseWriter) error
	}{
		{
			name: "Successful write of error",
			args: args{
				errorCode: 400,
				rw:        mocks.NewMockResponseWriter(),
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				errorCode := 400 // This matches the errorCode defined in args.

				// The provided error status code should have been set.
				if mrw.StatusCode != errorCode {
					return testhelpers.ErrUnexpectedStatusCode(errorCode, mrw.StatusCode)
				}

				// The expected data should have been encoded into the response.
				expectedData := &structcup.ErrorResponse{
					// This structure matches what WriteErrorResponse passes to WriteJSONResponse as data.
					Code:    errorCode,
					Message: http.StatusText(errorCode),
				}

				// Encode the expected data so we can compare it to the actual data.
				buffer := &bytes.Buffer{}
				encoder := json.NewEncoder(buffer)
				if err := encoder.Encode(expectedData); err != nil {
					return err
				}

				expectedStr := buffer.String() // Stringify the expected data.
				actualStr := mrw.Body.String() // Stringify the actual data.
				if expectedStr != actualStr {
					return testhelpers.ErrUnexpectedData(expectedStr, actualStr)
				}

				return nil
			},
		},
		{
			name: "Internal error while writing error",
			args: args{
				errorCode: 402,
				rw: func() *mocks.MockResponseWriter {
					rw := mocks.NewMockResponseWriter()
					rw.WriteErr = true // Force an error when the ResponseWriter is used.
					return rw
				}(),
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// Even though errorCode 402 was passed in, we should see
				// errorCode 500 due to the http.Error() call.
				if mrw.StatusCode != 500 {
					return testhelpers.ErrUnexpectedStatusCode(500, mrw.StatusCode)
				}

				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			WriteErrorResponse(tt.args.rw, tt.args.errorCode, false)
			if err := tt.checkMock(tt.args.rw); err != nil {
				t.Errorf("mock check failed: %v", err)
			}
		})
	}
}

func TestWriteGzipJSONResponse(t *testing.T) {
	var (
		compressedJsonDataByteCount = "65"
	)

	type args struct {
		code int
		data interface{}
		rw   *mocks.MockResponseWriter
	}
	tests := []struct {
		name      string
		wantErr   bool
		args      args
		gzWrite   func(z *gzip.Writer, p []byte) (int, error)
		checkMock func(*mocks.MockResponseWriter) error
	}{
		{
			name:    "Nil data",
			wantErr: false,
			args: args{
				code: 123,
				data: nil,
				rw:   mocks.NewMockResponseWriter(),
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// Check that the Content-Type header was set.
				if mrw.HeaderMap.Get("Content-Type") != "application/json" {
					return testhelpers.ErrMissingHeader("Content-Type", "application/json")
				}

				// Check that the expected statusCode was set.
				if mrw.StatusCode != 123 {
					return testhelpers.ErrUnexpectedStatusCode(123, mrw.StatusCode)
				}

				return nil
			},
		},
		{
			name: "Fail to Encode data",
			args: args{
				data: make(chan int), // Encoder can't Encode a channel, this will make Encode() return an error.
				rw:   mocks.NewMockResponseWriter(),
			},
			wantErr: true,
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				return nil // Nothing to check.
			},
		},
		{
			name:    "Fail to compress data",
			wantErr: true,
			args: args{
				code: 123,
				data: "data",
				rw:   mocks.NewMockResponseWriter(),
			},
			gzWrite: func(z *gzip.Writer, p []byte) (int, error) {
				return -1, testhelpers.ErrTest
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				return nil // Nothing to check.
			},
		},
		{
			name:    "Fail to Write",
			wantErr: true,
			args: args{
				code: 123,
				data: "data",
				rw: func() *mocks.MockResponseWriter {
					rw := mocks.NewMockResponseWriter()
					rw.WriteErr = true
					return rw
				}(),
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				return nil // Nothing to check.
			},
		},
		{
			name:    "Success",
			wantErr: false,
			args: args{
				code: 200,
				data: jsonData,
				rw:   mocks.NewMockResponseWriter(),
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// Check that the Content-Encoding header was set.
				if mrw.HeaderMap.Get("Content-Encoding") != "gzip" {
					return testhelpers.ErrMissingHeader("Content-Encoding", "gzip")
				}

				// Check that the Content-Type header was set.
				if mrw.HeaderMap.Get("Content-Type") != "application/json" {
					return testhelpers.ErrMissingHeader("Content-Type", "application/json")
				}

				// Check that the Content-Length header was set.
				if mrw.HeaderMap.Get("Content-Length") != compressedJsonDataByteCount {
					return testhelpers.ErrMissingHeader("Content-Length", compressedJsonDataByteCount)
				}

				// Check that the status code was set.
				if mrw.StatusCode != 200 {
					return testhelpers.ErrUnexpectedStatusCode(200, mrw.StatusCode)
				}

				// Create a reader for the compressed data.
				reader, err := gzip.NewReader(&mrw.Body)
				if err != nil {
					return err
				}
				defer reader.Close()

				// Get the decompressed data.
				data, err := io.ReadAll(reader)
				if err != nil {
					return err
				}

				// Create a decoder for the json data.
				buffer := bytes.NewBuffer(data)
				decoder := json.NewDecoder(buffer)
				var decodedData string

				// Decode the json data.
				err = decoder.Decode(&decodedData)
				if err != nil {
					return err
				}

				// Check that the data matches the input after it has been decompressed and decoded.
				if decodedData != jsonData {
					return testhelpers.ErrUnexpectedData(jsonData, decodedData)
				}

				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.gzWrite != nil {
				gzipWriteFunc = tt.gzWrite                              // Override the function variable.
				defer func() { gzipWriteFunc = (*gzip.Writer).Write }() // Reset the function variable when the test is over.
			}

			if err := WriteGzipJSONResponse(tt.args.rw, tt.args.data, tt.args.code, false); (err != nil) != tt.wantErr {
				t.Errorf("WriteGzipJSONResponse() error = %v, wantErr %v", err, tt.wantErr)
			}

			if err := tt.checkMock(tt.args.rw); err != nil {
				t.Errorf("mock check failed: %v", err)
			}
		})
	}
}

func TestWriteGzipErrorResponse(t *testing.T) {
	type args struct {
		rw        *mocks.MockResponseWriter
		errorCode int
	}
	tests := []struct {
		name      string
		args      args
		checkMock func(mrw *mocks.MockResponseWriter) error
	}{
		{
			name: "Successful write of error",
			args: args{
				errorCode: 400,
				rw:        mocks.NewMockResponseWriter(),
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				errorCode := 400 // This matches the errorCode defined in args.

				// The provided error status code should have been set.
				if mrw.StatusCode != errorCode {
					return testhelpers.ErrUnexpectedStatusCode(errorCode, mrw.StatusCode)
				}

				// The expected data should have been encoded into the response.
				expectedData := &structcup.ErrorResponse{
					// This structure matches what WriteErrorResponse passes to WriteJSONResponse as data.
					Code:    errorCode,
					Message: http.StatusText(errorCode),
				}

				// Encode the expected data so we can compare it to the actual data.
				jsonBuffer := &bytes.Buffer{}
				encoder := json.NewEncoder(jsonBuffer)
				if err := encoder.Encode(expectedData); err != nil {
					return err
				}

				// Compress the expected data so we can compare it to the actual data.
				gzipBuffer := &bytes.Buffer{}
				writer := gzip.NewWriter(gzipBuffer)
				if _, err := writer.Write(jsonBuffer.Bytes()); err != nil {
					writer.Close()
					return err
				}
				writer.Close()

				expectedStr := gzipBuffer.String() // Stringify the compressed expected data.
				actualStr := mrw.Body.String()     // Stringify the compressed actual data.
				if actualStr != expectedStr {
					return testhelpers.ErrUnexpectedData(expectedStr, actualStr)
				}

				return nil
			},
		},
		{
			name: "Internal error while writing error",
			args: args{
				errorCode: 402,
				rw: func() *mocks.MockResponseWriter {
					rw := mocks.NewMockResponseWriter()
					rw.WriteErr = true // Force an error when the ResponseWriter is used.
					return rw
				}(),
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// Even though errorCode 402 was passed in, we should see
				// errorCode 500 due to the http.Error() call.
				if mrw.StatusCode != 500 {
					return testhelpers.ErrUnexpectedStatusCode(500, mrw.StatusCode)
				}

				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			WriteGzipErrorResponse(tt.args.rw, tt.args.errorCode, false)
			if err := tt.checkMock(tt.args.rw); err != nil {
				t.Errorf("mock check failed: %v", err)
			}
		})
	}
}

func TestCheckMandatoryQueryParams(t *testing.T) {
	type args struct {
		queryParams          map[string][]string
		mandatoryQueryParams []*QueryParam
	}
	tests := []struct {
		name    string
		wantErr bool
		args    args
	}{
		// Fail cases.
		{
			name:    "Missing mandatory query param",
			wantErr: true,
			args: args{
				queryParams: map[string][]string{
					"key1": {"val1"},
				},
				mandatoryQueryParams: []*QueryParam{
					{Key: "mandatory"},
				},
			},
		},
		{
			name:    "Invalid mandatory query param: empty string",
			wantErr: true,
			args: args{
				queryParams: map[string][]string{
					"key1": {""},
				},
				mandatoryQueryParams: []*QueryParam{
					{Key: "key1"},
				},
			},
		},
		{
			name:    "Invalid mandatory query param: not an accepted value",
			wantErr: true,
			args: args{
				queryParams: map[string][]string{
					"key1": {"acceptable", "unacceptable"},
				},
				mandatoryQueryParams: []*QueryParam{
					{Key: "key1", AcceptedValues: []string{"acceptable"}, MultipleValuesAllowed: true},
				},
			},
		},
		{
			name:    "Invalid mandatory query param: duplicate",
			wantErr: true,
			args: args{
				queryParams: map[string][]string{
					"key1": {"duplicate", "duplicate"},
				},
				mandatoryQueryParams: []*QueryParam{
					{Key: "key1"},
				},
			},
		},
		{
			name:    "Invalid mandatory query param: duplicate with many values",
			wantErr: true,
			args: args{
				queryParams: map[string][]string{
					"key1": {
						"duplicate",
						"v1",
						"v2",
						"v3",
						"v4",
						"v5",
						"v6",
						"v7",
						"v8",
						"v9",
						"v10",
						"duplicate",
					},
				},
				mandatoryQueryParams: []*QueryParam{
					{Key: "key1", MultipleValuesAllowed: true},
				},
			},
		},
		{
			name:    "Invalid mandatory query param: multiple values not allowed",
			wantErr: true,
			args: args{
				queryParams: map[string][]string{
					"key1": {"val1", "val2"},
				},
				mandatoryQueryParams: []*QueryParam{
					{Key: "key1", MultipleValuesAllowed: false},
				},
			},
		},
		// Success cases.
		{
			name: "Missing mandatory query param but it's optional",
			args: args{
				queryParams: map[string][]string{
					"key1": {"val1", "val2"},
				},
				mandatoryQueryParams: []*QueryParam{
					{Key: "mandatory", Optional: true},
				},
			},
		},
		{
			name: "No parameters",
			args: args{
				queryParams:          map[string][]string{},
				mandatoryQueryParams: []*QueryParam{},
			},
		},
		{
			name: "Many non-mandatory params",
			args: args{
				queryParams: map[string][]string{
					"key1":  {"v1"},
					"key2":  {"v2"},
					"key3":  {"v3"},
					"key4":  {"v4"},
					"key5":  {"v5"},
					"key6":  {"v6"},
					"key7":  {"v7"},
					"key8":  {"v8"},
					"key9":  {"v9"},
					"key10": {"v10"},
				},
				mandatoryQueryParams: []*QueryParam{},
			},
		},
		{
			name: "Many mandatory params",
			args: args{
				queryParams: map[string][]string{
					"k1":  {"v1"},
					"k2":  {"v2"},
					"k3":  {"v3"},
					"k4":  {"v4"},
					"k5":  {"v5"},
					"k6":  {"v6"},
					"k7":  {"v7"},
					"k8":  {"v8"},
					"k9":  {"v9"},
					"k10": {"v10"},
				},
				mandatoryQueryParams: []*QueryParam{
					{Key: "k1"},
					{Key: "k2"},
					{Key: "k3"},
					{Key: "k4"},
					{Key: "k5"},
					{Key: "k6"},
					{Key: "k7"},
					{Key: "k8"},
					{Key: "k9"},
					{Key: "k10"},
				},
			},
		},
		{
			name: "Many values on a non-mandatory param",
			args: args{
				queryParams: map[string][]string{
					"k1": {
						"v1",
						"v2",
						"v3",
						"v4",
						"v5",
						"v6",
						"v7",
						"v8",
						"v9",
						"v10",
					},
				},
			},
		},
		{
			name: "Many values on a mandatory param",
			args: args{
				queryParams: map[string][]string{
					"k1": {
						"v1",
						"v2",
						"v3",
						"v4",
						"v5",
						"v6",
						"v7",
						"v8",
						"v9",
						"v10",
					},
				},
				mandatoryQueryParams: []*QueryParam{
					{Key: "k1", MultipleValuesAllowed: true},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CheckMandatoryQueryParams(tt.args.queryParams, tt.args.mandatoryQueryParams...); (err != nil) != tt.wantErr {
				t.Errorf("CheckMandatoryQueryParams() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestServeHTML(t *testing.T) {
	html := "<html />"

	type args struct {
		rw          *mocks.MockResponseWriter
		htmlContent io.Reader
		code        int
	}
	tests := []struct {
		name      string
		args      args
		checkMock func(*mocks.MockResponseWriter) error
	}{
		{
			name: "Nil htmlContent",
			args: args{
				rw:          mocks.NewMockResponseWriter(),
				htmlContent: nil,
				code:        200,
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// Even though StatusCode 200 was passed in,
				// it should have been overwritten by 500.
				if mrw.StatusCode != 500 {
					return testhelpers.ErrUnexpectedStatusCode(500, mrw.StatusCode)
				}

				return nil
			},
		},
		{
			name: "Fail to ReadAll",
			args: args{
				rw:          mocks.NewMockResponseWriter(),
				htmlContent: &mocks.MockReader{ReadErr: true}, // This mock returns an error on Read().
				code:        200,
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// Even though StatusCode 200 was passed in, it should get overwritten by 500.
				if mrw.StatusCode != 500 {
					return testhelpers.ErrUnexpectedStatusCode(500, mrw.StatusCode)
				}

				return nil
			},
		},
		{
			name: "Fail to Write",
			args: args{
				rw: func() *mocks.MockResponseWriter {
					rw := mocks.NewMockResponseWriter()
					rw.WriteErr = true // Make Write() return an error.
					return rw
				}(),
				htmlContent: &bytes.Reader{},
				code:        200,
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// Even though StatusCode 200 was passed in, it should get overwritten by 500.
				if mrw.StatusCode != 500 {
					return testhelpers.ErrUnexpectedStatusCode(500, mrw.StatusCode)
				}

				return nil
			},
		},
		{
			name: "Success",
			args: args{
				rw:          mocks.NewMockResponseWriter(),
				htmlContent: bytes.NewReader([]byte(html)),
				code:        200,
			},
			checkMock: func(mrw *mocks.MockResponseWriter) error {
				// Check for the Content-Type header.
				if mrw.HeaderMap.Get("Content-Type") != "text/html; charset=utf-8" {
					t.Error(testhelpers.ErrMissingHeader("Content-Type", "text/html; charset=utf-8"))
				}

				// Check the StatusCode.
				if mrw.StatusCode != 200 {
					return testhelpers.ErrUnexpectedStatusCode(200, mrw.StatusCode)
				}

				// Check that the htmlContent was written.
				if mrw.Body.String() != html {
					return testhelpers.ErrUnexpectedData(html, mrw.Body.String())
				}

				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ServeHTML(tt.args.rw, tt.args.htmlContent, tt.args.code)
			if err := tt.checkMock(tt.args.rw); err != nil {
				t.Errorf("mock check failed: %v", err)
			}
		})
	}
}
