package authorizationjuice

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// ------------------------------
// CONSTS
// ------------------------------

const (
	// Struct tags.
	RoleIntRange StructTag = "roleintrange"

	// Various keys.
	EntityIDsKey         Key = "entity_ids"
	EntityIDKey          Key = "entity_id"
	PrincipalIDKey       Key = "principal_id"
	RoleKey              Key = "role"
	UnallowedEntitiesKey Key = "unallowed_entities"
)

// ------------------------------
// TYPES
// ------------------------------

// Aliases.
type (
	StructTag = string
	Key       = string
)

// AuthorizeResponse represents an authorization response for a single entity where a simple status code is returned.
// swagger:model
type AuthorizeResponse struct {
	// The status code.
	// example: 200
	Code int `json:"code"`
}

// AuthorizeManyResponse represents an authorization response for the authorization of multiple entities where a list of unauthorized entity IDs is returned.
// swagger:model
type AuthorizeManyResponse struct {
	// The unauthorized entity IDs.
	// example: [5f6d4b9b9c6f9f0001b9c6f9]
	UnallowedEntities []primitive.ObjectID `json:"unallowed_entities"`
}

// AuthorizeMany represents the payload required to authorize multiple entities.
// swagger:model
type AuthorizeMany struct {
	// A list of MongoDB Object IDs representing the entities to be authorized.
	// example: [5f8f9a9a9a9a9a9a9a9a9a9a, 5f8f9a9a9a9a9a9a9a9a9a9a]
	// required: true
	EntityIDs *[]primitive.ObjectID `json:"entity_ids" validate:"nonnilpointer"`

	// The MongoDB Object ID of the principal being authorized to access the entities.
	// example: 5f8f9a9a9a9a9a9a9a9a9a9a
	// required: true
	PrincipalID *primitive.ObjectID `json:"principal_id" validate:"nonnilpointer,nonzeropointerelem"`

	// The role assigned to the principal for the authorized entities. Must be a valid integer role as defined in authorization microservice.
	// example: 1
	// required: true
	Role *int64 `json:"role" validate:"nonnilpointer,roleintrange"`
}

// ------------------------------
// CONSTRUCTOR
// ------------------------------

func NewAuthorizeMany(
	entityIDs []primitive.ObjectID,
	principalID primitive.ObjectID,
	role int64,
) *AuthorizeMany {
	authorizeMany := new(AuthorizeMany)
	authorizeMany.SetEntityIDs(entityIDs)
	authorizeMany.SetPrincipalID(principalID)
	authorizeMany.SetRole(role)
	return authorizeMany
}

// ------------------------------
// GETTERS
// ------------------------------

func (a *AuthorizeMany) GetEntityIDs() []primitive.ObjectID {
	if a.EntityIDs == nil {
		return []primitive.ObjectID{}
	}
	return *a.EntityIDs
}

func (a *AuthorizeMany) GetPrincipalID() primitive.ObjectID {
	if a.PrincipalID == nil {
		return primitive.NilObjectID
	}
	return *a.PrincipalID
}

func (a *AuthorizeMany) GetRole() int64 {
	if a.Role == nil {
		return 0
	}
	return *a.Role
}

// ------------------------------
// SETTERS
// ------------------------------

func (a *AuthorizeMany) SetEntityIDs(entityIDs []primitive.ObjectID) {
	if a.EntityIDs == nil {
		a.EntityIDs = new([]primitive.ObjectID)
	}
	*a.EntityIDs = entityIDs
}

func (a *AuthorizeMany) SetPrincipalID(principalID primitive.ObjectID) {
	if a.PrincipalID == nil {
		a.PrincipalID = new(primitive.ObjectID)
	}
	*a.PrincipalID = principalID
}

func (a *AuthorizeMany) SetRole(role int64) {
	if a.Role == nil {
		a.Role = new(int64)
	}
	*a.Role = role
}
