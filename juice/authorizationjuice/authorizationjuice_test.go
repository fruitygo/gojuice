package authorizationjuice

import (
	"reflect"
	"testing"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

var (
	pID  = primitive.NewObjectID()
	oID1 = primitive.NewObjectID()
	oID2 = primitive.NewObjectID()
	oID3 = primitive.NewObjectID()
	r    = int64(1)
)

func TestNewAuthorizeMany(t *testing.T) {
	type args struct {
		entityIDs   []primitive.ObjectID
		principalID primitive.ObjectID
		role        int64
	}
	tests := []struct {
		name string
		args args
		want *AuthorizeMany
	}{
		{
			name: "Many IDs",
			args: args{
				entityIDs:   []primitive.ObjectID{oID1, oID2, oID3},
				principalID: pID,
				role:        r,
			},
			want: &AuthorizeMany{
				EntityIDs:   &[]primitive.ObjectID{oID1, oID2, oID3},
				PrincipalID: &pID,
				Role:        &r,
			},
		},
		{
			name: "One ID",
			args: args{
				entityIDs:   []primitive.ObjectID{oID1},
				principalID: pID,
				role:        r,
			},
			want: &AuthorizeMany{
				EntityIDs:   &[]primitive.ObjectID{oID1},
				PrincipalID: &pID,
				Role:        &r,
			},
		},
		{
			name: "No IDs",
			args: args{
				entityIDs:   []primitive.ObjectID{},
				principalID: pID,
				role:        r,
			},
			want: &AuthorizeMany{
				EntityIDs:   &[]primitive.ObjectID{},
				PrincipalID: &pID,
				Role:        &r,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewAuthorizeMany(tt.args.entityIDs, tt.args.principalID, tt.args.role); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAuthorizeMany() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAuthorizeMany_GetEntityIDs(t *testing.T) {
	tests := []struct {
		name      string
		entityIDs *[]primitive.ObjectID
		want      []primitive.ObjectID
	}{
		{
			name:      "Many IDs",
			entityIDs: &[]primitive.ObjectID{oID1, oID2, oID3},
			want:      []primitive.ObjectID{oID1, oID2, oID3},
		},
		{
			name:      "One ID",
			entityIDs: &[]primitive.ObjectID{oID1},
			want:      []primitive.ObjectID{oID1},
		},
		{
			name:      "No IDs",
			entityIDs: &[]primitive.ObjectID{},
			want:      []primitive.ObjectID{},
		},
		{
			name:      "Nil EntityIDs",
			entityIDs: nil,
			want:      []primitive.ObjectID{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AuthorizeMany{EntityIDs: tt.entityIDs}
			if got := a.GetEntityIDs(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AuthorizeMany.GetEntityIDs() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAuthorizeMany_GetPrincipalID(t *testing.T) {
	tests := []struct {
		name        string
		principalID *primitive.ObjectID
		want        primitive.ObjectID
	}{
		{
			name:        "Returns the PrincipalID",
			principalID: &pID,
			want:        pID,
		},
		{
			name:        "Nil PrincipalID",
			principalID: nil,
			want:        primitive.NilObjectID,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AuthorizeMany{PrincipalID: tt.principalID}
			if got := a.GetPrincipalID(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AuthorizeMany.GetPrincipalID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAuthorizeMany_GetRole(t *testing.T) {
	tests := []struct {
		name string
		role *int64
		want int64
	}{
		{
			name: "Returns the Role",
			role: &r,
			want: r,
		},
		{
			name: "Nil Role",
			role: nil,
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AuthorizeMany{Role: tt.role}
			if got := a.GetRole(); got != tt.want {
				t.Errorf("AuthorizeMany.GetRole() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAuthorizeMany_SetEntityIDs(t *testing.T) {
	var (
		many = []primitive.ObjectID{oID1, oID2, oID3}
		one  = []primitive.ObjectID{oID1}
		none = []primitive.ObjectID{}
	)

	tests := []struct {
		name      string
		entityIDs []primitive.ObjectID
		want      *[]primitive.ObjectID
	}{
		{
			name:      "Set Many IDs",
			entityIDs: many,
			want:      &many,
		},
		{
			name:      "Set One ID",
			entityIDs: one,
			want:      &one,
		},
		{
			name:      "Set No IDs",
			entityIDs: none,
			want:      &none,
		},
		{
			name:      "Set Nil",
			entityIDs: nil,
			want:      &[]primitive.ObjectID{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AuthorizeMany{}

			// Check that a.EntityIDs gets initialized to nil.
			if a.EntityIDs != nil {
				t.Error("a.EntityIDs wasn't initialized to nil")
			}

			a.SetEntityIDs(tt.entityIDs)

			// The nil case has to be handled differently.
			if tt.entityIDs == nil {
				// Check that a.EntityIDs is no longer nil,
				// meaning memory was allocated for it.
				if a.EntityIDs == nil {
					t.Errorf("memory wasn't allocated for a.EntityIDs")
				}

				return // Return early so we don't do the equality check.
			}

			if got := a.EntityIDs; !reflect.DeepEqual(got, tt.want) {
				t.Errorf("a.EntityIDs = %v, want %v", a.EntityIDs, tt.want)
			}
		})
	}
}

func TestAuthorizeMany_SetPrincipalID(t *testing.T) {
	tests := []struct {
		name        string
		principalID primitive.ObjectID
		want        *primitive.ObjectID
	}{
		{
			name:        "Set PrincipalID",
			principalID: pID,
			want:        &pID,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AuthorizeMany{}
			a.SetPrincipalID(tt.principalID)
			if got := a.PrincipalID; !reflect.DeepEqual(got, tt.want) {
				t.Errorf("a.PrincipalID = %v, want %v", a.PrincipalID, tt.want)
			}
		})
	}
}

func TestAuthorizeMany_SetRole(t *testing.T) {
	tests := []struct {
		name string
		role int64
		want *int64
	}{
		{
			name: "Set Role",
			role: r,
			want: &r,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AuthorizeMany{}
			a.SetRole(tt.role)
			if got := a.Role; !reflect.DeepEqual(got, tt.want) {
				t.Errorf("a.Role = %v, want %v", a.Role, tt.want)
			}
		})
	}
}
