// Description: This package provides a set of functions to send emails using Fabriktor's email templates.
// It follows the Google Cloud Pub/Sub message format to send emails asynchronously.
package emailjuice

import (
	"encoding/json"

	"cloud.google.com/go/pubsub"

	"gitlab.com/fruitygo/gojuice/juice/validationjuice"
)

// ------------------------------
// CONSTANTS
// ------------------------------

// Keys represent the key of the attributes of a Google Pub/Sub message to send an email.
const (
	PubSubTemplateTypeKey      Key = "templateType"
	PubSubRecipientUsernameKey Key = "recipientUsername"
	PubSubRecipientEmailKey    Key = "recipientEmail"
	PubSubTargetLanguageKey    Key = "targetLanguage"
	PubSubSpecificsKey         Key = "specifics"
)

// TemplateName represent the name of Fabriktor's email templates.
// To be assigned to the `templateType` attribute of a Google Pub/Sub message.
const (
	WelcomeTemplate    TemplateName = "welcome"    // To greet new users.
	SupportTemplate    TemplateName = "support"    // To respond to tickets.
	DeletionTemplate   TemplateName = "deletion"   // To notify users of account deletion.
	ForbiddenTemplate  TemplateName = "forbidden"  // To notify that forbidden content has been deleted.
	WarningTemplate    TemplateName = "warning"    // To warn a user about to be suspended.
	SuspensionTemplate TemplateName = "suspension" // To notify a user that their account has been suspended.
	ConnectionTemplate TemplateName = "connection" // To notify a user that a suspicious connection has been detected.
)

// Pub/Sub related constants to be used in the `specifics` attribute of a Google Pub/Sub message.
const (
	// For support template.
	PubSubSpecificsMessageKey SpecificsKey = "msg"
	PubSubSpecificsURLKey     SpecificsKey = "url"

	// For suspension template.
	PubSubSpecificsCountKey SpecificsKey = "count"

	// For suspension template.
	PubSubSpecificsReasonKey SpecificsKey = "reason"

	// For suspicious connection template.
	PubSubSpecificsRequestIPKey    SpecificsKey = "request_ip"
	PubSubSpecificsUserAgentKey    SpecificsKey = "user_agent"
	PubSubSpecificsLocationKey     SpecificsKey = "location"
	PubSubSpecificsConnectionIDKey SpecificsKey = "connection_id"
)

// SuspensionReason represent predefined values for the `reason` subfield of `specifics` attribute of a Google Pub/Sub message.
const (
	// MaxInfringementsReached represents the reason for suspension when the maximum number of infringements is reached.
	MaxInfringementsReached SuspensionReason = "Maximum number of infringements reached"

	// IdentityUsurpation represents the reason for suspension due to identity usurpation.
	IdentityUsurpation SuspensionReason = "Identity usurpation"

	// ProfileDetectedFake represents the reason for suspension when a profile is detected to be fake.
	ProfileDetectedFake SuspensionReason = "Profile detected to be fake"

	// ProfileDetectedNotHuman represents the reason for suspension when a profile is detected to be non-human, potentially AI, spam, bot, etc.
	ProfileDetectedNotHuman SuspensionReason = "Profile detected to be not human, maybe AI, spam, bot, etc."
)

// Support related constants.
const (
	TicketsClosureMessage = `This ticket has now been closed. 
	
Please note that any further replies to this ticket will not be seen. I've personally made sure to address the issues, questions, or problems you encountered to the best of my ability.

It would be my pleasure to assist you again in the future. If you require further assistance, please don't hesitate to open a new ticket. 
`
)

// ------------------------------
// ENUMS
// ------------------------------

// To allow to easily pass and retrieve suspension reason from query parameters.
const (
	NA = iota
	MaxInfringementsReachedReason
	IdentityUsurpationReason
	ProfileDetectedFakeReason
	ProfileDetectedNotHumanReason
)

// ------------------------------
// TYPES
// ------------------------------

type (
	TemplateName     string
	Key              string
	SpecificsKey     string
	SuspensionReason string
	Specifics        map[SpecificsKey]string
)

// ------------------------------
// FUNCTION VARIABLES (for testing)
// ------------------------------

var (
	jsonMarshalFunc    = json.Marshal
	checkNilValuesFunc = validationjuice.CheckNilValues
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// NewPubSubEmail mimicks the reception of a PubSub email message for testing and previewing purposes.
//
// Do not use this function in production code.
func NewPubSubEmail(templateType TemplateName, targetLanguage, recipientEmail, recipientUsername Key, specifics Specifics) (*pubsub.Message, error) {
	m := &pubsub.Message{
		ID: "1234567890",
		Attributes: map[string]string{
			string(PubSubTemplateTypeKey):      string(templateType),
			string(PubSubTargetLanguageKey):    string(targetLanguage),
			string(PubSubRecipientEmailKey):    string(recipientEmail),
			string(PubSubRecipientUsernameKey): string(recipientUsername),
		},
	}

	// Convert specifics to a JSON encoded string.
	if specifics != nil {
		specificsStr, err := EncodeSpecifics(specifics)
		if err != nil {
			return nil, err
		}

		m.Attributes[string(PubSubSpecificsKey)] = specificsStr
	}

	return m, nil
}

// EncodeSpecifics encodes a map of specifics to a JSON string.
func EncodeSpecifics(specifics Specifics) (string, error) {
	err := checkNilValuesFunc(specifics)
	if err != nil {
		return "", err
	}

	// Marshal specifics to JSON string.
	specificsBytes, err := jsonMarshalFunc(specifics)
	if err != nil {
		return "", err
	}

	return string(specificsBytes), nil
}

// GetSuspensionSentence returns the suspension reason as a string.
func GetSuspensionSentence(enumValue int) SuspensionReason {
	switch enumValue {
	case MaxInfringementsReachedReason:
		return MaxInfringementsReached
	case IdentityUsurpationReason:
		return IdentityUsurpation
	case ProfileDetectedFakeReason:
		return ProfileDetectedFake
	case ProfileDetectedNotHumanReason:
		return ProfileDetectedNotHuman
	default:
		return ""
	}
}

// ------------------------------
// INTERFACE IMPLEMENTATIONS
// ------------------------------

// Implementation of json.Marshaler interface for Specifics type.
func (s Specifics) MarshalJSON() ([]byte, error) {
	// Convert Specifics to map[string]string before marshaling
	temp := make(map[string]string)
	for k, v := range s {
		temp[string(k)] = v
	}

	return jsonMarshalFunc(temp)
}
