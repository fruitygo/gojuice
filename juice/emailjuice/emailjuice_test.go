package emailjuice

import (
	"encoding/json"
	"reflect"
	"testing"

	"cloud.google.com/go/pubsub"

	"gitlab.com/fruitygo/gojuice/juice/validationjuice"
	"gitlab.com/fruitygo/gojuice/testhelpers"
)

var specs = Specifics{
	PubSubSpecificsCountKey:   "123",
	PubSubSpecificsMessageKey: "This message is neat?",
	PubSubSpecificsReasonKey:  "Good reason!",
	PubSubSpecificsURLKey:     "https://fakeurl.org",
}

func TestNewPubSubEmail(t *testing.T) {
	var (
		lang  = "en"
		email = "recipient@email.com"
		user  = "recipient"
	)

	type args struct {
		templateType      TemplateName
		targetLanguage    Key
		recipientEmail    Key
		recipientUsername Key
		specifics         Specifics
	}
	tests := []struct {
		name    string
		wantErr bool
		args    args
		want    *pubsub.Message
		setup   func()
	}{
		{
			name:    "Success",
			wantErr: false,
			args: args{
				templateType:      WelcomeTemplate,
				targetLanguage:    Key(lang),
				recipientEmail:    Key(email),
				recipientUsername: Key(user),
				specifics:         specs,
			},
			want: func() *pubsub.Message {
				encodedSpecs, err := EncodeSpecifics(specs)
				if err != nil {
					t.Error(err)
				}

				wantMsg := &pubsub.Message{
					ID: "1234567890", // This is a test function, so the ID is hardcoded.
					Attributes: map[string]string{
						string(PubSubTemplateTypeKey):      string(WelcomeTemplate),
						string(PubSubTargetLanguageKey):    lang,
						string(PubSubRecipientEmailKey):    email,
						string(PubSubRecipientUsernameKey): user,
						string(PubSubSpecificsKey):         encodedSpecs,
					},
				}

				return wantMsg
			}(),
			setup: func() {},
		},
		{
			name:    "Success: Nil Specifics",
			wantErr: false,
			args: args{
				templateType:      WelcomeTemplate,
				targetLanguage:    Key(lang),
				recipientEmail:    Key(email),
				recipientUsername: Key(user),
				specifics:         nil,
			},
			want: &pubsub.Message{
				ID: "1234567890", // This is a test function, so the ID is hardcoded.
				Attributes: map[string]string{
					string(PubSubTemplateTypeKey):      string(WelcomeTemplate),
					string(PubSubTargetLanguageKey):    lang,
					string(PubSubRecipientEmailKey):    email,
					string(PubSubRecipientUsernameKey): user,
					// No Specifics Attribute.
				},
			},
			setup: func() {},
		},
		{
			name:    "Fail to EncodeSpecifics",
			wantErr: true,
			args: args{
				templateType:      WelcomeTemplate,
				targetLanguage:    Key(lang),
				recipientEmail:    Key(email),
				recipientUsername: Key(user),
				specifics:         specs,
			},
			want: nil,
			setup: func() {
				// Force EncodeSpecifics to error by overriding json.Marshal.
				jsonMarshalFunc = func(any) ([]byte, error) { return nil, testhelpers.ErrTest }
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { jsonMarshalFunc = json.Marshal }() // Reset function variable after each test.

			got, err := NewPubSubEmail(tt.args.templateType, tt.args.targetLanguage, tt.args.recipientEmail, tt.args.recipientUsername, tt.args.specifics)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewPubSubEmail() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewPubSubEmail() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEncodeSpecifics(t *testing.T) {
	tests := []struct {
		name      string
		wantErr   bool
		specifics Specifics
		want      string
		setup     func()
	}{
		{
			name:      "Success",
			wantErr:   false,
			specifics: specs,
			want:      `{"count":"123","msg":"This message is neat?","reason":"Good reason!","url":"https://fakeurl.org"}`,
			setup:     func() {},
		},
		{
			name:      "Fail: CheckNilValues",
			wantErr:   true,
			specifics: Specifics{},
			want:      "",
			setup: func() { // Override CheckNilValues to return an error
				checkNilValuesFunc = func(params ...interface{}) error { return testhelpers.ErrTest }
			},
		},
		{
			name:      "Fail: json.Marshal",
			wantErr:   true,
			specifics: Specifics{},
			want:      "",
			setup: func() { // Override json.Marshal to return an error
				jsonMarshalFunc = func(v any) ([]byte, error) { return nil, testhelpers.ErrTest }
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { // Reset function variables after each test.
				jsonMarshalFunc = json.Marshal
				checkNilValuesFunc = validationjuice.CheckNilValues
			}()

			got, err := EncodeSpecifics(tt.specifics)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncodeSpecifics() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("EncodeSpecifics() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetSuspensionSentence(t *testing.T) {
	tests := []struct {
		name      string
		enumValue int
		want      SuspensionReason
	}{
		{
			name:      "NA",
			enumValue: 0,
			want:      "",
		},
		{
			name:      "Max Infringement Reached",
			enumValue: 1,
			want:      MaxInfringementsReached,
		},
		{
			name:      "Identity Usurpation",
			enumValue: 2,
			want:      IdentityUsurpation,
		},
		{
			name:      "Profile Detected Fake",
			enumValue: 3,
			want:      ProfileDetectedFake,
		},
		{
			name:      "Profile Detected Not Human",
			enumValue: 4,
			want:      ProfileDetectedNotHuman,
		},
		{
			// This one should fail if a new enum is added.
			name:      "One more than the expected enum",
			enumValue: 5,
			want:      "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetSuspensionSentence(tt.enumValue); got != tt.want {
				t.Errorf("GetSuspensionSentence() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSpecifics_MarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
		s       Specifics
		want    []byte
		setup   func()
	}{
		{
			name:    "Success",
			wantErr: false,
			s:       specs,
			want: func() []byte {
				bytes, err := json.Marshal(specs)
				if err != nil {
					t.Error(err)
				}
				return bytes
			}(),
			setup: func() {},
		},
		{
			name:    "Fail",
			wantErr: true,
			s:       specs,
			want:    nil,
			setup: func() { // Override the jsonMarshalFunc to return an error.
				jsonMarshalFunc = func(v any) ([]byte, error) { return nil, testhelpers.ErrTest }
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { jsonMarshalFunc = json.Marshal }() // Reset the function variable between tests.

			got, err := tt.s.MarshalJSON()
			if (err != nil) != tt.wantErr {
				t.Errorf("Specifics.MarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Specifics.MarshalJSON() = %v, want %v", got, tt.want)
			}
		})
	}
}
