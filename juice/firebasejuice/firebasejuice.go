// Description: This package provides a set of functions to interact with the Firebase API and the Firebase Admin SDK.
package firebasejuice

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/fs"
	"net/http"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
	"google.golang.org/api/option"

	"gitlab.com/fruitygo/gojuice/contracts"
)

// ------------------------------
// CONSTANTS
// ------------------------------

const (
	firebaseAPIURL  = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword"
	contentTypeJSON = "application/json"
)

// ------------------------------
// TYPES
// ------------------------------

// exchangeRequest is the model for the request body to exchange an email and password for a Firebase ID token.
type exchangeRequest struct {
	Email       string `json:"email"`
	Password    string `json:"password"`
	SecureToken bool   `json:"returnSecureToken"`
}

// exchangeResponse is the model for the response body from exchanging an email and password for a Firebase ID token.
type exchangeResponse struct {
	IDToken string `json:"idToken"`
}

// FirebaseErrorDetail is the model for the error details in a Firebase error response.k
type FirebaseErrorDetail struct {
	Domain  string `json:"domain"`
	Reason  string `json:"reason"`
	Message string `json:"message"`
}

type FirebaseError struct {
	Errors  []FirebaseErrorDetail `json:"errors"`
	Code    int                   `json:"code"`
	Message string                `json:"message"`
}

type FirebaseErrorResponse struct {
	Error FirebaseError `json:"error"`
}

// ------------------------------
// FUNCTION VARIABLES (for testing)
// ------------------------------

var (
	jsonMarshalFunc          = json.Marshal
	httpNewRequestFunc       = http.NewRequest
	httpClientDoFunc         = (*http.Client).Do
	jsonDecodeFunc           = (*json.Decoder).Decode
	firebaseNewAppFunc       = firebase.NewApp
	clientGetUserByEmailFunc = (*auth.Client).GetUserByEmail
	clientDeleteUserFunc     = (*auth.Client).DeleteUser
	clientCreateUserFunc     = (*auth.Client).CreateUser
	randReadFunc             = rand.Read
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// GetFirebaseIDToken exchanges an email and password of an active Firebase user for its Firebase UID.
//
// The email and password are used to authenticate with Firebase and represent the user's credentials.
//
// A valid Firebase API key is required to authenticate with the Firebase API.
//
// The Firebase ID token is returned.
//
// If the request fails, an error is returned.
func GetFirebaseIDToken(email, password, firebaseAPIKey string) (string, error) {
	requestData := exchangeRequest{
		Email:       email,
		Password:    password,
		SecureToken: true,
	}

	// Marshal the request data.
	requestDataBytes, err := jsonMarshalFunc(requestData)
	if err != nil {
		return "", fmt.Errorf("error marshalling request data: %v", err)
	}

	// Create the HTTP request.
	req, err := httpNewRequestFunc("POST", fmt.Sprintf("%s?key=%s", firebaseAPIURL, firebaseAPIKey), bytes.NewReader(requestDataBytes))
	if err != nil {
		return "", fmt.Errorf("error creating request: %v", err)
	}

	// Set the request headers.
	req.Header.Set("Content-Type", contentTypeJSON)

	// Define the HTTP client.
	httpClient := &http.Client{}

	resp, err := httpClientDoFunc(httpClient, req)
	if err != nil {
		return "", fmt.Errorf("error sending request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		var errResp FirebaseErrorResponse
		if err := jsonDecodeFunc(json.NewDecoder(resp.Body), &errResp); err != nil {
			return "", fmt.Errorf("error reading error response body: %v", err)
		}
		return "", fmt.Errorf("firebase error: %s", errResp.Error.Message)
	}

	// Define the model for the response body.
	var exchangeResponseData exchangeResponse

	// Decode the response body.
	if err := jsonDecodeFunc(json.NewDecoder(resp.Body), &exchangeResponseData); err != nil {
		return "", fmt.Errorf("error reading response body: %v", err)
	}

	return exchangeResponseData.IDToken, nil
}

// InitFirebaseApp initializes the Firebase Admin SDK and returns the app instance.
func InitFirebaseApp(ctx context.Context, FS fs.ReadFileFS, accountCredentialsPath string) (*firebase.App, error) {
	// Read the Firebase credentials content from the embedded file system.
	credentialsContent, err := FS.ReadFile(accountCredentialsPath)
	if err != nil {
		return nil, fmt.Errorf("error reading Firebase credentials: %v", err)
	}

	// Check if credentials content is empty.
	if len(credentialsContent) == 0 {
		return nil, fmt.Errorf("empty Firebase credentials content")
	}

	// Initialize the Firebase Admin SDK.
	app, err := firebaseNewAppFunc(ctx, nil, option.WithCredentialsJSON(credentialsContent))
	if err != nil {
		return nil, fmt.Errorf("error initializing Firebase app: %v", err)
	}

	return app, nil
}

// CreateFirebaseUser creates a Firebase user and returns the user record.
//
// An embedded filesystem and the path to the credentials file are required.
//
// User is created with the specified email and password only.
//
// The user to be passed to the function must be a pointer to a struct of type auth.UserToCreate.
//
// - user := auth.UserToCreate{}
//
// - user.Email(mockEmail)
//
// - user.Password(mockPwd)
func CreateFirebaseUser(ctx context.Context, app contracts.FirebaseAppInterface, httpClient *http.Client, user *auth.UserToCreate, email string) (*auth.UserRecord, error) {
	// Get a client to interact with the Firebase Auth service.
	client, err := app.Auth(ctx)
	if err != nil {
		return nil, fmt.Errorf("error getting auth client: %v", err)
	}

	// Check if user already exists.
	existingUser, err := clientGetUserByEmailFunc(client, ctx, email)
	if err == nil {
		deleteErr := clientDeleteUserFunc(client, ctx, existingUser.UID)
		if deleteErr != nil {
			return nil, fmt.Errorf("error deleting existing user: %v", deleteErr)
		}
	}

	// Create the user.
	createdUser, err := clientCreateUserFunc(client, ctx, user)
	if err != nil {
		return nil, fmt.Errorf("error creating user: %v", err)
	}

	// Return the user record.
	return createdUser, nil
}

// CleanupMock deletes the Firebase user.
//
// Typically utilized in integration tests to clean up the user after the test is complete.
//
// Use "defer" to call this function after the test is complete to delete the mock user from Firebase.
func CleanupMock(ctx context.Context, app contracts.FirebaseAppInterface, user *auth.UserRecord) error {
	if user != nil && app != nil {
		client, err := app.Auth(ctx)
		if err != nil {
			return fmt.Errorf("error getting auth client: %v", err)
		}

		if err := clientDeleteUserFunc(client, ctx, user.UID); err != nil {
			return fmt.Errorf("error deleting user: %v", err)
		}
	}

	return nil
}

// GenerateMockFirebaseUID generates a random Firebase UID of the specified length.
//
// Typically utilized in integration tests to create a mock user.
func GenerateMockFirebaseUID(length int) string {
	// Ensure the length is positive.
	if length < 0 {
		length = length * -1
	}

	// Limit the length to 128 characters.
	if length > 128 {
		length = 128
	}

	// Generate a random byte slice of the specified length.
	b := make([]byte, length)

	if _, err := randReadFunc(b); err != nil {
		panic(err)
	}

	return base64.URLEncoding.EncodeToString(b)
}
