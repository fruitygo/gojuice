package firebasejuice

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"io"
	"io/fs"
	"net/http"
	"reflect"
	"strings"
	"testing"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
	"gitlab.com/fruitygo/gojuice/contracts"
	"gitlab.com/fruitygo/gojuice/testhelpers"
	"gitlab.com/fruitygo/gojuice/testhelpers/mocks"
	"google.golang.org/api/option"
)

func TestGetFirebaseIDToken(t *testing.T) {
	var (
		email = "fake@email.com"
		pass  = "password"
		key   = "key"
	)

	type args struct {
		email          string
		password       string
		firebaseAPIKey string
	}
	tests := []struct {
		name           string
		args           args
		want           string
		wantErr        bool
		expectedErrMsg string
		setup          func()
	}{
		{
			name: "Fail: json.Marshal returns an error",
			args: args{
				email:          email,
				password:       pass,
				firebaseAPIKey: key,
			},
			want:           "",
			wantErr:        true,
			expectedErrMsg: "error marshalling request data",
			setup: func() { // Override json.Marshal to return an error.
				jsonMarshalFunc = func(any) ([]byte, error) { return nil, testhelpers.ErrTest }
			},
		},
		{
			name: "Fail: http.NewRequest returns an error",
			args: args{
				email:          email,
				password:       pass,
				firebaseAPIKey: key,
			},
			want:           "",
			wantErr:        true,
			expectedErrMsg: "error creating request",
			setup: func() { // Override http.NewRequest to return an error.
				httpNewRequestFunc = func(_, _ string, _ io.Reader) (*http.Request, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
		{
			name: "Fail: http.Client.Do returns an error",
			args: args{
				email:          email,
				password:       pass,
				firebaseAPIKey: key,
			},
			want:           "",
			wantErr:        true,
			expectedErrMsg: "error sending request",
			setup: func() { // Override http.Client.Do to return an error.
				httpClientDoFunc = func(*http.Client, *http.Request) (*http.Response, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
		{
			name: "Fail: json.Decoder.Decode (after bad response status code) returns an error",
			args: args{
				email:          email,
				password:       pass,
				firebaseAPIKey: key,
			},
			want:           "",
			wantErr:        true,
			expectedErrMsg: "error reading error response body",
			setup: func() {
				// Override http.Client.Do to return a response with a bad StatusCode.
				httpClientDoFunc = func(*http.Client, *http.Request) (*http.Response, error) {
					return &http.Response{StatusCode: 400, Body: mocks.NewMockReader()}, nil
				}

				// Override json.Decoder.Decode to return an error.
				jsonDecodeFunc = func(*json.Decoder, any) error { return testhelpers.ErrTest }
			},
		},
		{
			name: "Fail: Firebase error",
			args: args{
				email:          email,
				password:       pass,
				firebaseAPIKey: key,
			},
			want:           "",
			wantErr:        true,
			expectedErrMsg: "firebase error",
			setup: func() {
				// Override http.Client.Do to return a response with a bad StatusCode.
				httpClientDoFunc = func(*http.Client, *http.Request) (*http.Response, error) {
					return &http.Response{StatusCode: 400, Body: mocks.NewMockReader()}, nil
				}

				// Override json.Decoder.Decode so it doesn't error.
				jsonDecodeFunc = func(dec *json.Decoder, v any) error {
					return nil
				}
			},
		},
		{
			name: "Fail: json.Decoder.Decode (after good response status code) returns an error",
			args: args{
				email:          email,
				password:       pass,
				firebaseAPIKey: key,
			},
			want:           "",
			wantErr:        true,
			expectedErrMsg: "error reading response body",
			setup: func() {
				// Override http.Client.Do to return a response with a good StatusCode.
				httpClientDoFunc = func(*http.Client, *http.Request) (*http.Response, error) {
					return &http.Response{StatusCode: 200, Body: mocks.NewMockReader()}, nil
				}

				// Override json.Decoder.Decode to return an error.
				jsonDecodeFunc = func(dec *json.Decoder, v any) error {
					return testhelpers.ErrTest // Error on the second call.
				}
			},
		},
		{
			// Had to override some function pointers on the Success case because we can't get a real response from Firebase.
			name: "Success",
			args: args{
				email:          email,
				password:       pass,
				firebaseAPIKey: key,
			},
			want:    "test token",
			wantErr: false,
			setup: func() {
				// Override http.Client.Do to return a response with a good StatusCode.
				httpClientDoFunc = func(*http.Client, *http.Request) (*http.Response, error) {
					return &http.Response{StatusCode: 200, Body: mocks.NewMockReader()}, nil
				}

				// Override json.Decoder.Decode so it doesn't error.
				jsonDecodeFunc = func(dec *json.Decoder, v any) error {
					v.(*exchangeResponse).IDToken = "test token"
					return nil
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { // Reset the function variables after each test.
				jsonMarshalFunc = json.Marshal
				httpNewRequestFunc = http.NewRequest
				httpClientDoFunc = (*http.Client).Do
				jsonDecodeFunc = (*json.Decoder).Decode
			}()

			got, err := GetFirebaseIDToken(tt.args.email, tt.args.password, tt.args.firebaseAPIKey)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFirebaseIDToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantErr && !strings.Contains(err.Error(), tt.expectedErrMsg) {
				t.Errorf("unexpected error. got: %s, want: %s", err.Error(), tt.expectedErrMsg)
			}
			if got != tt.want {
				t.Errorf("GetFirebaseIDToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInitFirebaseApp(t *testing.T) {
	var (
		ctx     = context.Background()
		testApp = &firebase.App{}
		path    = "path"
		creds   = `{"fake" : "credentials"}`
	)

	tests := []struct {
		name                   string
		FS                     fs.ReadFileFS
		want                   *firebase.App
		wantErr                bool
		expectedErrMsg         string
		accountCredentialsPath string
		setup                  func()
	}{
		{
			name:                   "Fail: FS.ReadFile returns an error",
			FS:                     &mocks.MockReadFileFS{ReadFileErr: true},
			want:                   nil,
			wantErr:                true,
			expectedErrMsg:         "error reading Firebase credentials",
			accountCredentialsPath: "bad path", // Force FS.ReadFile to error.
			setup:                  func() {},
		},
		{
			name:                   "Fail: empty credentialsContent",
			FS:                     &mocks.MockReadFileFS{FileContents: map[string][]byte{path: []byte("")}},
			want:                   nil,
			wantErr:                true,
			expectedErrMsg:         "empty Firebase credentials content",
			accountCredentialsPath: path,
			setup:                  func() {},
		},
		{
			name:                   "Fail: firebase.NewApp returns an error",
			FS:                     &mocks.MockReadFileFS{FileContents: map[string][]byte{path: []byte(creds)}},
			want:                   nil,
			wantErr:                true,
			expectedErrMsg:         "error initializing Firebase app",
			accountCredentialsPath: path,
			setup: func() { // Override firebase.NewApp to return an error.
				firebaseNewAppFunc = func(context.Context, *firebase.Config, ...option.ClientOption) (*firebase.App, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
		{
			name:                   "Success",
			FS:                     &mocks.MockReadFileFS{FileContents: map[string][]byte{path: []byte(creds)}},
			want:                   testApp,
			wantErr:                false,
			accountCredentialsPath: path,
			setup: func() { // Override firebase.NewApp to return testApp.
				firebaseNewAppFunc = func(c context.Context, _ *firebase.Config, _ ...option.ClientOption) (*firebase.App, error) {
					// Check that the correct context was passed in.
					if c != ctx {
						t.Error("Unexpected context was passed to firebase.NewApp")
					}
					return testApp, nil
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { firebaseNewAppFunc = firebase.NewApp }() // Reset firebase.NewApp after each test.

			got, err := InitFirebaseApp(ctx, tt.FS, tt.accountCredentialsPath)
			if (err != nil) != tt.wantErr {
				t.Errorf("InitFirebaseApp() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantErr && !strings.Contains(err.Error(), tt.expectedErrMsg) {
				t.Errorf("unexpected error. got: %s, want: %s", err.Error(), tt.expectedErrMsg)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitFirebaseApp() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCreateFirebaseUser(t *testing.T) {
	var (
		ctx          = context.Background()
		client       = &http.Client{}
		userToCreate = &auth.UserToCreate{}
		user         = &auth.UserRecord{}
		email        = "test@mail.com"
	)

	tests := []struct {
		name           string
		mockApp        contracts.FirebaseAppInterface
		want           *auth.UserRecord
		wantErr        bool
		expectedErrMsg string
		setup          func()
	}{
		{
			name:           "Fail: app.Auth returns an error",
			mockApp:        &mocks.MockFirebaseApp{AuthErr: true}, // Force app.Auth to return an error.
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "error getting auth client",
			setup:          func() {},
		},
		{
			name:           "Fail: client.DeleteUser returns an error",
			mockApp:        &mocks.MockFirebaseApp{AuthClient: &auth.Client{}},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "error deleting existing user",
			setup: func() {
				// Override client.GetUserByEmail to return an existing user.
				clientGetUserByEmailFunc = func(*auth.Client, context.Context, string) (*auth.UserRecord, error) {
					existingUser := &auth.UserRecord{}
					existingUser.UserInfo = &auth.UserInfo{UID: "id"}
					return existingUser, nil
				}

				// Override client.DeleteUser to return an error.
				clientDeleteUserFunc = func(*auth.Client, context.Context, string) error {
					return testhelpers.ErrTest
				}
			},
		},
		{
			name:           "Fail: client.CreateUser returns an error",
			mockApp:        &mocks.MockFirebaseApp{AuthClient: &auth.Client{}},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "error creating user",
			setup: func() {
				// Override client.GetUserByEmail to return an existing user.
				clientGetUserByEmailFunc = func(*auth.Client, context.Context, string) (*auth.UserRecord, error) {
					existingUser := &auth.UserRecord{}
					existingUser.UserInfo = &auth.UserInfo{UID: "id"}
					return existingUser, nil
				}

				// Override client.DeleteUser to succeed.
				clientDeleteUserFunc = func(*auth.Client, context.Context, string) error {
					return nil
				}

				// Override client.CreateUser to return an error.
				clientCreateUserFunc = func(*auth.Client, context.Context, *auth.UserToCreate) (*auth.UserRecord, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
		{
			name:    "Success",
			mockApp: &mocks.MockFirebaseApp{AuthClient: &auth.Client{}},
			want:    user,
			wantErr: false,
			setup: func() {
				// Override client.GetUserByEmail to return an existing user.
				clientGetUserByEmailFunc = func(*auth.Client, context.Context, string) (*auth.UserRecord, error) {
					existingUser := &auth.UserRecord{}
					existingUser.UserInfo = &auth.UserInfo{UID: "id"}
					return existingUser, nil
				}

				// Override client.DeleteUser to succeed.
				clientDeleteUserFunc = func(*auth.Client, context.Context, string) error {
					return nil
				}

				// Override client.CreateUser to succeed.
				clientCreateUserFunc = func(*auth.Client, context.Context, *auth.UserToCreate) (*auth.UserRecord, error) {
					return user, nil
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { // Reset all function variables after each test.
				clientGetUserByEmailFunc = (*auth.Client).GetUserByEmail
				clientDeleteUserFunc = (*auth.Client).DeleteUser
				clientCreateUserFunc = (*auth.Client).CreateUser
			}()

			got, err := CreateFirebaseUser(ctx, tt.mockApp, client, userToCreate, email)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateFirebaseUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantErr && !strings.Contains(err.Error(), tt.expectedErrMsg) {
				t.Errorf("unexpected error. got: %s, want: %s", err.Error(), tt.expectedErrMsg)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateFirebaseUser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCleanupMock(t *testing.T) {
	var (
		ctx    = context.Background()
		id     = "id"
		user   = &auth.UserRecord{UserInfo: &auth.UserInfo{UID: id}}
		client = &auth.Client{}
	)

	tests := []struct {
		name           string
		mockApp        contracts.FirebaseAppInterface
		wantErr        bool
		expectedErrMsg string
		setup          func()
	}{
		{
			name:           "Fail: app.Auth returns an error",
			mockApp:        &mocks.MockFirebaseApp{AuthErr: true},
			wantErr:        true,
			expectedErrMsg: "error getting auth client",
			setup:          func() {},
		},
		{
			name:           "Fail: client.DeleteUser returns an error",
			mockApp:        &mocks.MockFirebaseApp{AuthClient: client},
			wantErr:        true,
			expectedErrMsg: "error deleting user",
			setup: func() {
				// Override client.DeleteUser to return an error.
				clientDeleteUserFunc = func(*auth.Client, context.Context, string) error {
					return testhelpers.ErrTest
				}
			},
		},
		{
			name:    "Success",
			mockApp: &mocks.MockFirebaseApp{AuthClient: client},
			wantErr: false,
			setup: func() {
				// Override client.DeleteUser to succeed and check its parameters.
				clientDeleteUserFunc = func(ac *auth.Client, c context.Context, uid string) error {
					// Check that the expected client was passed in.
					if ac != client {
						t.Error("Unexpected client was passed into DeleteUserFunc")
					}
					// Check that the expected context was passed in.
					if c != ctx {
						t.Error("Unexpected context was passed into AuthFunc")
					}
					// Check that the expected UID was passed in.
					if uid != id {
						t.Error("Unexpected UID was passed into AuthFunc")
					}
					return nil
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { // Reset all function variables after each test.
				clientDeleteUserFunc = (*auth.Client).DeleteUser
			}()

			err := CleanupMock(ctx, tt.mockApp, user)
			if (err != nil) != tt.wantErr {
				t.Errorf("CleanupMock() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr && !strings.Contains(err.Error(), tt.expectedErrMsg) {
				t.Errorf("unexpected error. got: %s, want: %s", err.Error(), tt.expectedErrMsg)
			}
		})
	}
}

func TestGenerateMockFirebaseUID(t *testing.T) {
	tests := []struct {
		name           string
		length         int
		expectedLength int
		shouldPanic    bool
	}{
		{
			name:        "Fail: rand.Read returns an error causing a panic",
			length:      0,
			shouldPanic: true,
		},
		{
			name:           "Success: zero",
			length:         0,
			expectedLength: 0,
		},
		{
			name:           "Success: Min",
			length:         1,
			expectedLength: 4,
		},
		{
			name:           "Success: One",
			length:         1,
			expectedLength: 4,
		},
		{
			name:           "Success: Three",
			length:         3,
			expectedLength: 4,
		},
		{
			name:           "Success: Eight",
			length:         8,
			expectedLength: 12,
		},
		{
			name:           "Success: Max",
			length:         128,
			expectedLength: 172,
		},
		{
			name:           "Success: Negative",
			length:         -3,
			expectedLength: 4,
		},
		{
			name:           "Success: Negative min",
			length:         -1,
			expectedLength: 4,
		},
		{
			name:           "Success: Negative max",
			length:         -128,
			expectedLength: 172,
		},
		{
			name:           "Success: Just over max",
			length:         129,
			expectedLength: 172,
		},
		{
			name:           "Succes: Way over max",
			length:         99999,
			expectedLength: 172,
		},
		{
			name:           "Success: Negative over max",
			length:         -129,
			expectedLength: 172,
		},
		{
			name:           "Succes: Negative way over max",
			length:         -99999,
			expectedLength: 172,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.shouldPanic {
				// Override rand.Read to return an error causing a panic and reset it after the test.
				randReadFunc = func([]byte) (int, error) { return 0, testhelpers.ErrTest }
				defer func() { randReadFunc = rand.Read }()
			}

			defer func() { // Check for a panic.
				if r := recover(); r != nil {
					if !tt.shouldPanic {
						t.Errorf("unexpected panic: %v", r)
					}
				} else if tt.shouldPanic {
					t.Errorf("expected a panic but did not get one")
				}
			}()

			if got := GenerateMockFirebaseUID(tt.length); len(got) != tt.expectedLength {
				t.Errorf("GenerateMockFirebaseUID() = %v, want %v", len(got), tt.expectedLength)
			}
		})
	}
}
