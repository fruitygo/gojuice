// Description: This package provides a set of functions to interact with Google Cloud services such as Secret Manager, Pub/Sub, and Maps.
package gcloudjuice

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"net/http"
	"strings"

	"gitlab.com/fruitygo/gojuice/cup/structcup"
	"golang.org/x/oauth2"
	"google.golang.org/api/idtoken"
)

// ------------------------------
// FUNCTION VARIABLES (for testing)
// ------------------------------

var (
	authenticateOIDCTokenFunc = authenticateOIDCToken
	idtokenNewValidatorFunc   = idtoken.NewValidator
	validatorValidateFunc     = (*idtoken.Validator).Validate
	idtokenNewTokenSourceFunc = idtoken.NewTokenSource
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// AuthenticateOIDCToken verifies a JWT token's authenticity based on the provided audience and issuer.
//
// It assumes the token is passed with the Bearer prefix.
//
// It requires an HTTP response writer (w), an HTTP request (r), an authorization token key (authTokenKey),
// an expected audience (expectedAudience), and an expected issuer (expectedIssuer).
//
// It returns a success response if the token is valid or an error if the token is invalid or does not match the expected audience and issuer.
func AuthenticateOIDCToken(ctx context.Context, r *http.Request, authTokenKey, expectedAudience, expectedIssuer string) (*structcup.AuthResponse, error) {
	return authenticateOIDCTokenFunc(ctx, r, authTokenKey, "Bearer", expectedAudience, expectedIssuer)
}

// AuthenticateOIDCToken verifies a JWT token's authenticity based on the provided audience and issuer.
//
// It allows to specify a custom prefix for the authorization token instead of the default 'Bearer'.
//
// It requires an HTTP response writer (w), an HTTP request (r), an authorization token key (authTokenKey),
// an expected audience (expectedAudience), and an expected issuer (expectedIssuer).
//
// It returns a success response if the token is valid or an error if the token is invalid or does not match the expected audience and issuer.
func AuthenticateOIDCTokenCustomPrefix(ctx context.Context, r *http.Request, authTokenKey, prefix, expectedAudience, expectedIssuer string) (*structcup.AuthResponse, error) {
	return authenticateOIDCTokenFunc(ctx, r, authTokenKey, prefix, expectedAudience, expectedIssuer)
}

func authenticateOIDCToken(ctx context.Context, r *http.Request, authTokenKey, prefix, expectedAudience, expectedIssuer string) (*structcup.AuthResponse, error) {
	// Retrieve the "Authorization" header value.
	authHeader := r.Header.Get(authTokenKey)

	// Check if the Authorization header is not empty and starts with prefix.
	if authHeader == "" || !strings.HasPrefix(authHeader, prefix) {
		return nil, errors.New(strings.ToLower(http.StatusText(http.StatusUnauthorized)))
	}

	// Add space to prefix.
	prefix = prefix + " "

	// Extract the token from the Authorization header.
	authToken := strings.TrimPrefix(authHeader, prefix)

	// Create a JWT validator.
	validator, err := idtokenNewValidatorFunc(ctx)
	if err != nil {
		return nil, err
	}

	// Validate the JWT token using the configured Validator.
	token, err := validatorValidateFunc(validator, ctx, authToken, expectedAudience)
	if err != nil {
		return nil, err
	}

	// Verify the issuer (the expected issuer of the token).
	if token.Issuer != expectedIssuer {
		return nil, fmt.Errorf("unauthorized attempt")
	}

	// Extract token information
	tokenInfo := map[string]interface{}{
		"issuer":   token.Issuer,
		"audience": token.Audience,
		"subject":  token.Subject,
	}

	// Return success response.
	response := &structcup.AuthResponse{
		Message: "Authentication successful",
		User:    tokenInfo,
	}

	return response, nil
}

// CreateOIDCToken creates an OIDC token with the given audience.
//
// It requires an embedded filesystem (FS), a path to the credentials file (credentialsPath), and an audience (audience).
//
// It returns the OIDC token or an error if the operation fails.
func CreateOIDCToken(ctx context.Context, FS fs.ReadFileFS, credentialsPath string, audience string) (*oauth2.Token, error) {
	// Read the credentials file.
	credentialsContent, err := FS.ReadFile(credentialsPath)
	if err != nil {
		return nil, err
	}

	// Create a token source with the specified audience.
	ts, err := idtokenNewTokenSourceFunc(ctx, audience, idtoken.WithCredentialsJSON(credentialsContent))
	if err != nil {
		return nil, err
	}

	// Retrieve the OIDC token.
	token, err := ts.Token()
	if err != nil {
		return nil, err
	}

	// Check if the token is empty or nil.
	if token == nil || token.AccessToken == "" {
		return nil, fmt.Errorf("OIDC token is empty or nil")
	}

	// Return the OIDC token.
	return token, nil
}
