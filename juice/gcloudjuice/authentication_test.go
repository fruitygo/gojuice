package gcloudjuice

import (
	"context"
	"fmt"
	"io/fs"
	"net/http"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/fruitygo/gojuice/cup/structcup"
	"gitlab.com/fruitygo/gojuice/testhelpers"
	"gitlab.com/fruitygo/gojuice/testhelpers/mocks"
	"golang.org/x/oauth2"
	"google.golang.org/api/idtoken"
)

func TestAuthenticateOIDCToken(t *testing.T) {
	var (
		ctx              = context.Background()
		req              = &http.Request{}
		authTokenKey     = "key"
		expectedAudience = "audience"
		expectedIssuer   = "issuer"
		resp             = &structcup.AuthResponse{}
	)

	tests := []struct {
		name        string
		want        *structcup.AuthResponse
		wantErr     bool
		expectedErr error
		setup       func()
	}{
		{
			name: "Success: Bearer should be assumed for the prefix",
			want: resp,
			setup: func() {
				authenticateOIDCTokenFunc = func(c context.Context, r *http.Request, atk, prefix, ea, ei string) (*structcup.AuthResponse, error) {
					// Check that `Bearer` was passed in as the prefix.
					b := "Bearer"
					if prefix != b {
						t.Errorf("Unexpected prefix: got = %s, want = %s", prefix, b)
					}

					// Check that all the parameters were passed through.
					if c != ctx {
						t.Errorf("Unexpected context: got = %v, want = %v", c, ctx)
					}
					if r != req {
						t.Errorf("Unexpected request: got = %v, want = %v", r, req)
					}
					if atk != authTokenKey {
						t.Errorf("Unexpected key: got = %s, want = %s", atk, authTokenKey)
					}
					if ea != expectedAudience {
						t.Errorf("Unexpected audience: got = %s, want = %s", ea, expectedAudience)
					}
					if ei != expectedIssuer {
						t.Errorf("Unexpected issuer: got = %s, want = %s", ei, expectedIssuer)
					}
					return resp, nil
				}
			},
		},
		{
			name:        "Fail: expected error should be returned",
			want:        nil,
			wantErr:     true,
			expectedErr: testhelpers.ErrTest,
			setup: func() {
				// Force authenticateOIDCToken to return an error.
				authenticateOIDCTokenFunc = func(_ context.Context, _ *http.Request, _, _, _, _ string) (*structcup.AuthResponse, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { authenticateOIDCTokenFunc = authenticateOIDCToken }() // Reset function variable after each test.

			got, err := AuthenticateOIDCToken(ctx, req, authTokenKey, expectedAudience, expectedIssuer)
			if (err != nil) != tt.wantErr {
				t.Errorf("AuthenticateOIDCToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AuthenticateOIDCToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAuthenticateOIDCTokenCustomPrefix(t *testing.T) {
	var (
		ctx              = context.Background()
		req              = &http.Request{}
		authTokenKey     = "key"
		prefix           = "prefix"
		expectedAudience = "audience"
		expectedIssuer   = "issuer"
		resp             = &structcup.AuthResponse{}
	)

	tests := []struct {
		name        string
		want        *structcup.AuthResponse
		wantErr     bool
		expectedErr error
		setup       func()
	}{
		{
			name: "Success",
			want: resp,
			setup: func() {
				authenticateOIDCTokenFunc = func(c context.Context, r *http.Request, atk, p, ea, ei string) (*structcup.AuthResponse, error) {
					// Check that all the parameters were passed through.
					if c != ctx {
						t.Errorf("Unexpected context: got = %v, want = %v", c, ctx)
					}
					if r != req {
						t.Errorf("Unexpected request: got = %v, want = %v", r, req)
					}
					if atk != authTokenKey {
						t.Errorf("Unexpected key: got = %s, want = %s", atk, authTokenKey)
					}
					if p != prefix {
						t.Errorf("Unexpected prefix: got = %s, want = %s", p, prefix)
					}
					if ea != expectedAudience {
						t.Errorf("Unexpected audience: got = %s, want = %s", ea, expectedAudience)
					}
					if ei != expectedIssuer {
						t.Errorf("Unexpected issuer: got = %s, want = %s", ei, expectedIssuer)
					}
					return resp, nil
				}
			},
		},
		{
			name:        "Fail",
			want:        nil,
			wantErr:     true,
			expectedErr: testhelpers.ErrTest,
			setup: func() {
				// Force authenticateOIDCToken to return an error.
				authenticateOIDCTokenFunc = func(_ context.Context, _ *http.Request, _, _, _, _ string) (*structcup.AuthResponse, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { authenticateOIDCTokenFunc = authenticateOIDCToken }() // Reset function variable after each test.

			got, err := AuthenticateOIDCTokenCustomPrefix(ctx, req, authTokenKey, prefix, expectedAudience, expectedIssuer)
			if (err != nil) != tt.wantErr {
				t.Errorf("AuthenticateOIDCTokenCustomPrefix() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AuthenticateOIDCTokenCustomPrefix() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_authenticateOIDCToken(t *testing.T) {
	var (
		ctx      = context.Background()
		key      = "key"
		pre      = "prefix"
		val      = "value"
		audience = "audience"
		issuer   = "issuer"
		subject  = "subject"
		req      = &http.Request{Header: http.Header{}}
	)
	req.Header.Add(key, fmt.Sprintf("%s %s", pre, val))

	type args struct {
		r                *http.Request
		authTokenKey     string
		prefix           string
		expectedAudience string
		expectedIssuer   string
	}
	tests := []struct {
		name           string
		args           args
		want           *structcup.AuthResponse
		wantErr        bool
		expectedErrMsg string
		setup          func()
	}{
		{
			name: "Fail: Missing auth header",
			args: args{
				r:                &http.Request{Header: http.Header{}}, // No headers.
				authTokenKey:     key,
				prefix:           pre,
				expectedAudience: audience,
				expectedIssuer:   issuer,
			},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "unauthorized",
			setup:          func() {},
		},
		{
			name: "Fail: Empty auth header value",
			args: args{
				r: func() *http.Request {
					h := http.Header{}
					h.Add(key, "") // Empty value.
					return &http.Request{Header: h}
				}(),
				authTokenKey:     key,
				prefix:           pre,
				expectedAudience: audience,
				expectedIssuer:   issuer,
			},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "unauthorized",
			setup:          func() {},
		},
		{
			name: "Fail: Auth header has incorrect prefix",
			args: args{
				r: func() *http.Request {
					h := http.Header{}
					h.Add(key, val) // No prefix.
					return &http.Request{Header: h}
				}(),
				authTokenKey:     key,
				prefix:           pre,
				expectedAudience: audience,
				expectedIssuer:   issuer,
			},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "unauthorized",
			setup:          func() {},
		},
		{
			name: "Fail: idtoken.NewValidator returns an error",
			args: args{
				r:                req,
				authTokenKey:     key,
				prefix:           pre,
				expectedAudience: audience,
				expectedIssuer:   issuer,
			},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup: func() { // Override idtokenNewValidatorFunc to return an error.
				idtokenNewValidatorFunc = func(context.Context, ...idtoken.ClientOption) (*idtoken.Validator, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
		{
			name: "Fail: validator.Validate returns an error",
			args: args{
				r:                req,
				authTokenKey:     key,
				prefix:           pre,
				expectedAudience: audience,
				expectedIssuer:   issuer,
			},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup: func() {
				// Override idtokenNewValidatorFunc to succeed.
				idtokenNewValidatorFunc = func(context.Context, ...idtoken.ClientOption) (*idtoken.Validator, error) {
					return &idtoken.Validator{}, nil
				}

				// Override validatorValidateFunc to return an error.
				validatorValidateFunc = func(_ *idtoken.Validator, _ context.Context, _, _ string) (*idtoken.Payload, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
		{
			name: "Fail: Incorrect token issuer",
			args: args{
				r:                req,
				authTokenKey:     key,
				prefix:           pre,
				expectedAudience: audience,
				expectedIssuer:   issuer,
			},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "unauthorized attempt",
			setup: func() {
				// Override idtokenNewValidatorFunc to succeed.
				idtokenNewValidatorFunc = func(context.Context, ...idtoken.ClientOption) (*idtoken.Validator, error) {
					return &idtoken.Validator{}, nil
				}

				// Override validatorValidateFunc to return an incorrect Issuer.
				validatorValidateFunc = func(_ *idtoken.Validator, _ context.Context, _, _ string) (*idtoken.Payload, error) {
					return &idtoken.Payload{Issuer: "wrong"}, nil
				}
			},
		},
		{
			name: "Fail: Missing token issuer",
			args: args{
				r:                req,
				authTokenKey:     key,
				prefix:           pre,
				expectedAudience: audience,
				expectedIssuer:   issuer,
			},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "unauthorized attempt",
			setup: func() {
				// Override idtokenNewValidatorFunc to succeed.
				idtokenNewValidatorFunc = func(context.Context, ...idtoken.ClientOption) (*idtoken.Validator, error) {
					return &idtoken.Validator{}, nil
				}

				// Override validatorValidateFunc to return a payload without an Issuer.
				validatorValidateFunc = func(_ *idtoken.Validator, _ context.Context, _, _ string) (*idtoken.Payload, error) {
					return &idtoken.Payload{}, nil
				}
			},
		},
		{
			name: "Success",
			args: args{
				r:                req,
				authTokenKey:     key,
				prefix:           pre,
				expectedAudience: audience,
				expectedIssuer:   issuer,
			},
			want: &structcup.AuthResponse{
				Message: "Authentication successful",
				User: map[string]interface{}{
					"issuer":   issuer,
					"audience": audience,
					"subject":  subject,
				},
			},
			setup: func() {
				// Override idtokenNewValidatorFunc to succeed.
				idtokenNewValidatorFunc = func(context.Context, ...idtoken.ClientOption) (*idtoken.Validator, error) {
					return &idtoken.Validator{}, nil
				}

				// Override validatorValidateFunc to succeed.
				validatorValidateFunc = func(_ *idtoken.Validator, _ context.Context, _, _ string) (*idtoken.Payload, error) {
					return &idtoken.Payload{Issuer: issuer, Audience: audience, Subject: subject}, nil
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { // Reset function variables after each test.
				idtokenNewValidatorFunc = idtoken.NewValidator
				validatorValidateFunc = (*idtoken.Validator).Validate
			}()

			got, err := authenticateOIDCToken(ctx, tt.args.r, tt.args.authTokenKey, tt.args.prefix, tt.args.expectedAudience, tt.args.expectedIssuer)
			if (err != nil) != tt.wantErr {
				t.Errorf("authenticateOIDCToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.expectedErrMsg != "" && !strings.Contains(err.Error(), tt.expectedErrMsg) {
				t.Errorf("Unexpected error msg: got = %s, want = %s", err.Error(), tt.expectedErrMsg)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("authenticateOIDCToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCreateOIDCToken(t *testing.T) {
	var (
		ctx      = context.Background()
		path     = "path"
		audience = "audience"
		token    = &oauth2.Token{AccessToken: "valid token"}
	)

	tests := []struct {
		name           string
		fs             fs.ReadFileFS
		want           *oauth2.Token
		wantErr        bool
		expectedErrMsg string
		setup          func()
	}{
		{
			name:           "Fail: FS.ReadFile returns an error",
			fs:             &mocks.MockReadFileFS{ReadFileErr: true},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup:          func() {},
		},
		{
			name:           "Fail: idtoken.NewTokenSource returns an error",
			fs:             &mocks.MockReadFileFS{},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup: func() { // Override idtokenNewTokenSourceFunc to return an error.
				idtokenNewTokenSourceFunc = func(context.Context, string, ...idtoken.ClientOption) (oauth2.TokenSource, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
		{
			name:           "Fail: oauth2.TokenSource.Token returns an error",
			fs:             &mocks.MockReadFileFS{},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup: func() { // Override idtokenNewTokenSourceFunc to return a TokenSource that errors on Token().
				idtokenNewTokenSourceFunc = func(context.Context, string, ...idtoken.ClientOption) (oauth2.TokenSource, error) {
					return &mocks.MockTokenSource{TokenErr: true}, nil
				}
			},
		},
		{
			name:           "Fail: oauth2.TokenSource.Token returns nil token",
			fs:             &mocks.MockReadFileFS{},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "OIDC token is empty or nil",
			setup: func() { // Override idtokenNewTokenSourceFunc to return a TokenSource that errors on Token().
				idtokenNewTokenSourceFunc = func(context.Context, string, ...idtoken.ClientOption) (oauth2.TokenSource, error) {
					return &mocks.MockTokenSource{T: nil}, nil
				}
			},
		},
		{
			name:           "Fail: oauth2.TokenSource.Token returns empty token",
			fs:             &mocks.MockReadFileFS{},
			want:           nil,
			wantErr:        true,
			expectedErrMsg: "OIDC token is empty or nil",
			setup: func() { // Override idtokenNewTokenSourceFunc to return a TokenSource that errors on Token().
				idtokenNewTokenSourceFunc = func(context.Context, string, ...idtoken.ClientOption) (oauth2.TokenSource, error) {
					return &mocks.MockTokenSource{T: &oauth2.Token{AccessToken: ""}}, nil
				}
			},
		},
		{
			name: "Success",
			fs:   &mocks.MockReadFileFS{},
			want: token,
			setup: func() { // Override idtokenNewTokenSourceFunc to return a valid Token.
				idtokenNewTokenSourceFunc = func(context.Context, string, ...idtoken.ClientOption) (oauth2.TokenSource, error) {
					return &mocks.MockTokenSource{T: token}, nil
				}

			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { // Reset function variables after each test.
				idtokenNewTokenSourceFunc = idtoken.NewTokenSource
			}()

			got, err := CreateOIDCToken(ctx, tt.fs, path, audience)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateOIDCToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.expectedErrMsg != "" && !strings.Contains(err.Error(), tt.expectedErrMsg) {
				t.Errorf("Unexpected error msg: got = %s, want = %s", err.Error(), tt.expectedErrMsg)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateOIDCToken() = %v, want %v", got, tt.want)
			}
		})
	}
}
