// Description: This package provides a set of functions to interact with Google Cloud services such as Secret Manager, Pub/Sub, and Maps.
package gcloudjuice

import (
	"context"
	"errors"
	"time"

	"googlemaps.github.io/maps"

	"gitlab.com/fruitygo/gojuice/contracts"
)

// ------------------------------
// ERRORS
// ------------------------------

// ErrNoResultForCoordinates is returned when no results are found for the given coordinates.
var ErrNoResultForCoordinates = errors.New("no results found for the provided coordinates")

// ------------------------------
// FUNCTION VARIABLES (for testing)
// ------------------------------

var mapsNewClientFunc = maps.NewClient

// ------------------------------
// FUNCTIONS
// ------------------------------

// NewMapsClient creates a new Google Maps client.
func NewMapsClient(ctx context.Context, mapsApiKey string, qps int) (*maps.Client, error) {
	c, err := mapsNewClientFunc(maps.WithAPIKey(mapsApiKey), maps.WithRateLimit(qps))
	if err != nil {
		return nil, err
	}

	return c, nil
}

// GetTimezoneFromCoordinates retrieves the timezone from the given coordinates.
func GetTimezoneFromCoordinates(ctx context.Context, c contracts.MapsClientInterface, latitude float64, longitude float64) (string, error) {
	// Build TimezoneRequest.
	request := &maps.TimezoneRequest{
		Location: &maps.LatLng{
			Lat: latitude,
			Lng: longitude,
		},
		Timestamp: time.Now().UTC(),
		Language:  "en",
	}

	// Call Timezone.
	r, err := c.Timezone(ctx, request)
	if err != nil {
		return "", err
	}

	return r.TimeZoneID, nil
}

// GetCityFromCoordinates retrieves the city name from the given latitude and longitude using Google Maps API.
func GetCityFromCoordinates(ctx context.Context, c contracts.MapsClientInterface, latitude float64, longitude float64) (string, error) {
	request := &maps.GeocodingRequest{
		LatLng: &maps.LatLng{
			Lat: latitude,
			Lng: longitude,
		},
		ResultType: []string{"locality"},
		Language:   "en",
	}

	results, err := c.Geocode(ctx, request)
	if err != nil {
		return "", err
	}

	if len(results) > 0 {
		return results[0].FormattedAddress, nil
	}

	return "", ErrNoResultForCoordinates
}
