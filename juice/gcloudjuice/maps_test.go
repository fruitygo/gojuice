package gcloudjuice

import (
	"context"
	"errors"
	"reflect"
	"testing"

	"gitlab.com/fruitygo/gojuice/contracts"
	"gitlab.com/fruitygo/gojuice/testhelpers"
	"gitlab.com/fruitygo/gojuice/testhelpers/mocks"
	"googlemaps.github.io/maps"
)

func TestNewMapsClient(t *testing.T) {
	var (
		ctx        = context.Background()
		mapsApiKey = "key"
		qps        = 1
		client     = &maps.Client{}
	)

	tests := []struct {
		name          string
		want          *maps.Client
		wantErr       bool
		newClientFunc func(...maps.ClientOption) (*maps.Client, error)
	}{
		{
			name:    "Fail: maps.NewClient returns an error",
			wantErr: true,
			newClientFunc: func(...maps.ClientOption) (*maps.Client, error) {
				// Override maps.NewClient to return an error.
				return nil, testhelpers.ErrTest
			},
		},
		{
			name: "Success",
			want: client,
			newClientFunc: func(...maps.ClientOption) (*maps.Client, error) {
				// Override maps.NewClient to return a valid client.
				return client, nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mapsNewClientFunc = tt.newClientFunc
			defer func() { mapsNewClientFunc = maps.NewClient }() // Reset function variable after each test.

			got, err := NewMapsClient(ctx, mapsApiKey, qps)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewMapsClient() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMapsClient() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetTimezoneFromCoordinates(t *testing.T) {
	var (
		ctx = context.Background()
		lat = float64(23)
		lng = float64(24)
		id  = "id"
		tr  = &maps.TimezoneResult{TimeZoneID: id}
	)

	tests := []struct {
		name    string
		c       contracts.MapsClientInterface
		want    string
		wantErr bool
	}{
		{
			name:    "Fail: c.Timezone returns an error",
			c:       &mocks.MockMapsClient{TimezoneErr: true},
			wantErr: true,
		},
		{
			name: "Success",
			c:    &mocks.MockMapsClient{TimezoneResult: tr},
			want: id,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetTimezoneFromCoordinates(ctx, tt.c, lat, lng)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTimezoneFromCoordinates() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetTimezoneFromCoordinates() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetCityFromCoordinates(t *testing.T) {
	var (
		ctx          = context.Background()
		lat  float64 = 23
		lng  float64 = 24
		addr         = "address"
	)

	tests := []struct {
		name    string
		c       contracts.MapsClientInterface
		want    string
		wantErr error
	}{
		{
			name:    "Fail: maps.Client.Geocode returns an error",
			c:       &mocks.MockMapsClient{GeocodeErr: true},
			wantErr: testhelpers.ErrTest,
		},
		{
			name:    "Fail: Empty Geocode results",
			c:       &mocks.MockMapsClient{GeocodeResult: []maps.GeocodingResult{}},
			wantErr: ErrNoResultForCoordinates,
		},
		{
			name: "Success",
			c: &mocks.MockMapsClient{
				GeocodeResult: []maps.GeocodingResult{
					{FormattedAddress: addr},
					{FormattedAddress: "Don't"},
					{FormattedAddress: "Care"},
				},
			},
			want: addr,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetCityFromCoordinates(ctx, tt.c, lat, lng)
			if !errors.Is(err, tt.wantErr) {
				t.Errorf("GetCityFromCoordinates() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetCityFromCoordinates() = %v, want %v", got, tt.want)
			}
		})
	}
}
