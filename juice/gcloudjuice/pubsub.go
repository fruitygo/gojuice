// Description: This package provides a set of functions to interact with Google Cloud services such as Secret Manager, Pub/Sub, and Maps.
package gcloudjuice

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"cloud.google.com/go/pubsub"
)

// ------------------------------
// ALIASES
// ------------------------------

type (
	Topic            = string
	Subscription     = string
	PubSubMessageKey = string
	Value            = string
)

// ------------------------------
// CONST
// ------------------------------

const (
	// Common.
	IsTestKey PubSubMessageKey = "is_test"

	// SMS.
	// This section contains keys to perform the sending of SMS messages.
	// Webhook is located in sms microservice.
	SMSTopicKey     Topic            = "sms1"
	SMSSubKey       Subscription     = "sms1"
	SMSRecipientKey PubSubMessageKey = "recipient"
	SMSCodeKey      PubSubMessageKey = "code"

	// Reports.
	// This section contains keys to perform the generation of PDF reports.
	// Webhook is located in reports microservice.
	ReportsPDFTopicKey                 Topic            = "pdf1"
	ReportsPDFSubKey                   Subscription     = "pdf1"
	ReportsPDFDocIDKey                 PubSubMessageKey = "_id"
	ReportsPDFTypeKey                  PubSubMessageKey = "pdf_type"
	ReportsPDFCurrencyKey              PubSubMessageKey = "currency"
	ReportsPDFIsTestKey                PubSubMessageKey = IsTestKey
	ReportsPDFTypeBill                 Value            = "bills"
	ReportsPDFTypePayroll              Value            = "payrolls"
	ReportsPDFTypeDeductionAggregation Value            = "deductionaggregations"

	// Emails.
	// This section contains keys to send emails.
	// More keys are defined directly in gojuice/emailjuice package.
	// Webhook is located in email microservice.
	EmailsTopicKey Topic        = "email"
	EmailsSubKey   Subscription = "email"

	// Notifications.
	// This section contains keys to send notifications.
	// Webhook is located in notifications microservice.
	NotificationsTopicKey     Topic            = "notification"
	NotificationsSubKey       Subscription     = "notification"
	NotificationsLanguage     PubSubMessageKey = "language"
	NotificationsUserIDs      PubSubMessageKey = "user_ids" // Comma-separated list of user IDs.
	NotificationsEnglishTitle PubSubMessageKey = "english_title"
	NotificationsEnglishBody  PubSubMessageKey = "english_body"
	NotificationsCustomImage  PubSubMessageKey = "custom_image"
	NotificationsCustomSound  PubSubMessageKey = "custom_sound"
	NotificationsCustomData   PubSubMessageKey = "custom_data" // JSON-encoded map.
	NotificationsIsTestKey    PubSubMessageKey = IsTestKey

	// DEPRECATED: Using a direct GET call to retreive payment documents for each payable entity.
	//
	// Payments.
	// This section contains keys to mark entities as paid.
	// For example, when a payroll or a bill is paid, an event will be sent from the payments service
	// to the proper service where the paid entity is located, in order to update a flag.
	// Webhooks are located in payrolls and billable microservices.
	//
	// PaymentsTopicKey                     Topic            = "payment"
	// PaymentsSubKey                       Subscription     = "payment"
	// PaymentsPaymentIDKey                 PubSubMessageKey = "_id"
	// PaymentsPaidEntityIDKey              PubSubMessageKey = "paid_entity_id"
	// PaymentsPaidEntityCollectionPayrolls Value            = "payrolls"
	// PaymentsPaidEntityCollectionBills    Value            = "bills"
)

// ------------------------------
// TYPES
// ------------------------------

type pubSubPayload struct {
	Message struct {
		Data        []byte            `json:"data,omitempty"`
		Attributes  map[string]string `json:"attributes,omitempty"`
		MessageID   string            `json:"messageId,omitempty"`
		PublishTime time.Time         `json:"publishTime,omitempty"`
	} `json:"message,omitempty"`
	Subscription string `json:"subscription,omitempty"`
}

// ------------------------------
// FUNCTION VARIABLES (for testing)
// ------------------------------

var (
	jsonDecodeFunc   = (*json.Decoder).Decode
	decodeStringFunc = (*base64.StdEncoding).DecodeString
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// DecodePubSubMessage decodes a Google Cloud Pub/Sub message from the request body and maps it to the provided Pub/Sub Message struct.
func DecodePubSubMessage(r *http.Request, message *pubsub.Message) error {
	// Verify that the content type is application/json.
	if r.Header.Get("Content-Type") != "application/json" {
		return fmt.Errorf("invalid content type")
	}

	// Check if the request body is empty.
	if r.Body == nil || r.Body == http.NoBody || r.ContentLength == 0 {
		return fmt.Errorf("empty body")
	}
	defer r.Body.Close()

	// Struct to unmarshal the JSON payload.
	payload := pubSubPayload{}

	// Decode the JSON payload from the request body.
	if err := jsonDecodeFunc(json.NewDecoder(r.Body), &payload); err != nil {
		return err
	}

	if payload.Message.Data != nil {
		// Decode base64-encoded data.
		decodedData, err := decodeStringFunc(string(payload.Message.Data))
		if err != nil {
			return err
		}
		message.Data = decodedData
	}

	// Map the attributes, message ID, and publish time.
	message.Attributes = payload.Message.Attributes
	message.ID = payload.Message.MessageID
	message.PublishTime = payload.Message.PublishTime

	return nil
}
