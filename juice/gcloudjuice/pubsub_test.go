package gcloudjuice

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
	"reflect"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/fruitygo/gojuice/testhelpers"
	"gitlab.com/fruitygo/gojuice/testhelpers/mocks"
)

func TestDecodePubSubMessage(t *testing.T) {
	var (
		h     = http.Header{"Content-Type": []string{"application/json"}}
		data  = []byte("data")
		attrs = map[string]string{"key": "val"}
		id    = "id"
		time  = time.Now()
	)

	type args struct {
		r   *http.Request
		msg *pubsub.Message
	}
	tests := []struct {
		name           string
		args           args
		want           *pubsub.Message
		wantErr        bool
		expectedErrMsg string
		setup          func()
	}{
		{
			name: "Fail: Missing Content-Type Header",
			args: args{
				r: &http.Request{Header: http.Header{}},
			},
			wantErr:        true,
			expectedErrMsg: "invalid content type",
			setup:          func() {},
		},
		{
			name: "Fail: Incorrect Content-Type Header",
			args: args{
				r: &http.Request{Header: http.Header{"Content-Type": []string{"incorrect"}}},
			},
			wantErr:        true,
			expectedErrMsg: "invalid content type",
			setup:          func() {},
		},
		{
			name: "Fail: nil Body",
			args: args{
				r: &http.Request{Header: h, Body: nil, ContentLength: 1},
			},
			wantErr:        true,
			expectedErrMsg: "empty body",
			setup:          func() {},
		},
		{
			name: "Fail: Body == NoBody",
			args: args{
				r: &http.Request{Header: h, Body: http.NoBody, ContentLength: 1},
			},
			wantErr:        true,
			expectedErrMsg: "empty body",
			setup:          func() {},
		},
		{
			name: "Fail: ContentLength == 0",
			args: args{
				r: &http.Request{Header: h, Body: &mocks.MockReader{}, ContentLength: 0},
			},
			wantErr:        true,
			expectedErrMsg: "empty body",
			setup:          func() {},
		},
		{
			name: "Fail: json.Decode returns an error",
			args: args{
				r: &http.Request{Header: h, Body: &mocks.MockReader{}, ContentLength: 1},
			},
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup: func() { // Override Decoder.Decode to return an error.
				jsonDecodeFunc = func(*json.Decoder, any) error { return testhelpers.ErrTest }
			},
		},
		{
			name: "Fail: base64.StdEncoding.DecodeString returns an error",
			args: args{
				r: &http.Request{Header: h, Body: &mocks.MockReader{}, ContentLength: 1},
			},
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup: func() {
				// Override Decoder.Decode to put data into the payload.
				jsonDecodeFunc = func(_ *json.Decoder, v any) error {
					if payload, ok := v.(*pubSubPayload); ok {
						payload.Message.Data = []byte("data")
					}
					return nil
				}

				// Override StdEncoding.DecodeString to return an error.
				decodeStringFunc = func(string) ([]byte, error) { return nil, testhelpers.ErrTest }
			},
		},
		{
			name: "Success: non-nil payload data",
			args: args{
				r:   &http.Request{Header: h, Body: &mocks.MockReader{}, ContentLength: 1},
				msg: &pubsub.Message{},
			},
			want: &pubsub.Message{
				Attributes:  attrs,
				Data:        data,
				ID:          id,
				PublishTime: time,
			},
			setup: func() {
				// Override Decoder.Decode to put data into the payload.
				jsonDecodeFunc = func(dec *json.Decoder, v any) error {
					if payload, ok := v.(*pubSubPayload); ok {
						payload.Message.Data = data
						payload.Message.Attributes = attrs
						payload.Message.MessageID = id
						payload.Message.PublishTime = time
					}
					return nil
				}
				// Override StdEncoding.DecodeString to return data.
				decodeStringFunc = func(string) ([]byte, error) { return data, nil }
			},
		},
		{
			name: "Success: nil payload data",
			args: args{
				r:   &http.Request{Header: h, Body: &mocks.MockReader{}, ContentLength: 1},
				msg: &pubsub.Message{},
			},
			want: &pubsub.Message{
				Attributes:  attrs,
				ID:          id,
				PublishTime: time,
			},
			setup: func() {
				// Override Decoder.Decode to put data into the payload.
				jsonDecodeFunc = func(dec *json.Decoder, v any) error {
					if payload, ok := v.(*pubSubPayload); ok {
						payload.Message.Attributes = attrs
						payload.Message.MessageID = id
						payload.Message.PublishTime = time
					}
					return nil
				}
				// Override StdEncoding.DecodeString to return data.
				decodeStringFunc = func(string) ([]byte, error) { return data, nil }
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { // Reset function variables after each test.
				jsonDecodeFunc = (*json.Decoder).Decode
				decodeStringFunc = (*base64.StdEncoding).DecodeString
			}()

			msg := &pubsub.Message{}
			err := DecodePubSubMessage(tt.args.r, msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodePubSubMessage() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.expectedErrMsg != "" && !strings.Contains(err.Error(), tt.expectedErrMsg) {
				t.Errorf("Unexpected error msg: got = %s, want = %s", err.Error(), tt.expectedErrMsg)
			}
			if !tt.wantErr && !reflect.DeepEqual(msg, tt.want) {
				t.Errorf("msg = %v, want %v", msg, tt.want)
			}
		})
	}
}
