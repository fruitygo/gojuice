// Description: This package provides a set of functions to interact with Google Cloud services such as Secret Manager, Pub/Sub, and Maps.
package gcloudjuice

import (
	"context"
	"fmt"
	"io/fs"

	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	"cloud.google.com/go/secretmanager/apiv1/secretmanagerpb"
	"google.golang.org/api/option"
)

// ------------------------------
// TYPES
// ------------------------------

type SecretInfo struct {
	ProjectID string
	SecretID  string
	VersionID string
}

// ------------------------------
// FUNCTION VARIABLES (for testing)
// ------------------------------

var (
	secretmanagerNewClientFunc    = secretmanager.NewClient
	clientCloseFunc               = (*secretmanager.Client).Close
	clientAccessSecretVersionFunc = (*secretmanager.Client).AccessSecretVersion
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// GetSecretValue retrieves a secret value from Google Cloud Secret Manager.
//
// It returns the secret value as a byte slice or an error if the operation fails.
func GetSecretValue(ctx context.Context, FS fs.ReadFileFS, credentialsPath string, secret SecretInfo) ([]byte, error) {
	credentialsContent, err := FS.ReadFile(credentialsPath)
	if err != nil {
		return nil, err
	}

	client, err := secretmanagerNewClientFunc(ctx, option.WithCredentialsJSON(credentialsContent))
	if err != nil {
		return nil, err
	}
	defer clientCloseFunc(client)

	secretName := fmt.Sprintf("projects/%s/secrets/%s/versions/%s", secret.ProjectID, secret.SecretID, secret.VersionID)

	accessRequest := &secretmanagerpb.AccessSecretVersionRequest{
		Name: secretName,
	}

	result, err := clientAccessSecretVersionFunc(client, ctx, accessRequest)
	if err != nil {
		return nil, err
	}

	return result.Payload.Data, nil
}
