package gcloudjuice

import (
	"context"
	"io/fs"
	"reflect"
	"strings"
	"testing"

	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	"cloud.google.com/go/secretmanager/apiv1/secretmanagerpb"
	"github.com/googleapis/gax-go/v2"
	"gitlab.com/fruitygo/gojuice/testhelpers"
	"gitlab.com/fruitygo/gojuice/testhelpers/mocks"
	"google.golang.org/api/option"
)

func TestGetSecretValue(t *testing.T) {
	var (
		ctx    = context.Background()
		path   = "path"
		secret = SecretInfo{
			ProjectID: "pID",
			SecretID:  "sID",
			VersionID: "vID",
		}
		data = []byte("data")
	)

	type args struct {
		FS              fs.ReadFileFS
		credentialsPath string
		secret          SecretInfo
	}
	tests := []struct {
		name           string
		args           args
		want           []byte
		wantErr        bool
		expectedErrMsg string
		setup          func()
	}{
		{
			name: "Fail: fs.ReadFile returns an error",
			args: args{
				FS:              &mocks.MockReadFileFS{ReadFileErr: true},
				credentialsPath: path,
				secret:          secret,
			},
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup:          func() {},
		},
		{
			name: "Fail: secretmanager.NewClient returns an error",
			args: args{
				FS:              &mocks.MockReadFileFS{},
				credentialsPath: path,
				secret:          secret,
			},
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup: func() { // Override secretmanager.NewClient to return an error.
				secretmanagerNewClientFunc = func(context.Context, ...option.ClientOption) (*secretmanager.Client, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
		{
			name: "Fail: client.AccessSecretVersion returns an error",
			args: args{
				FS:              &mocks.MockReadFileFS{},
				credentialsPath: path,
				secret:          secret,
			},
			wantErr:        true,
			expectedErrMsg: testhelpers.ErrTest.Error(),
			setup: func() {
				// Override secretmanager.NewClient to succeed.
				secretmanagerNewClientFunc = func(context.Context, ...option.ClientOption) (*secretmanager.Client, error) {
					return &secretmanager.Client{}, nil
				}

				// Override client.Close so it doesn't panic.
				clientCloseFunc = func(*secretmanager.Client) error { return nil }

				// Override client.AccessSecretVersion to return an error.
				clientAccessSecretVersionFunc = func(
					*secretmanager.Client, context.Context, *secretmanagerpb.AccessSecretVersionRequest, ...gax.CallOption,
				) (*secretmanagerpb.AccessSecretVersionResponse, error) {
					return nil, testhelpers.ErrTest
				}
			},
		},
		{
			name: "Success",
			args: args{
				FS:              &mocks.MockReadFileFS{},
				credentialsPath: path,
				secret:          secret,
			},
			want: data,
			setup: func() {
				// Override secretmanager.NewClient to succeed.
				secretmanagerNewClientFunc = func(context.Context, ...option.ClientOption) (*secretmanager.Client, error) {
					return &secretmanager.Client{}, nil
				}

				// Override client.Close so it doesn't panic.
				clientCloseFunc = func(*secretmanager.Client) error { return nil }

				// Override client.AccessSecretVersion to return a result.
				clientAccessSecretVersionFunc = func(
					*secretmanager.Client, context.Context, *secretmanagerpb.AccessSecretVersionRequest, ...gax.CallOption,
				) (*secretmanagerpb.AccessSecretVersionResponse, error) {
					return &secretmanagerpb.AccessSecretVersionResponse{
						Payload: &secretmanagerpb.SecretPayload{Data: data},
					}, nil
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() { // Reset function variables after each test.
				secretmanagerNewClientFunc = secretmanager.NewClient
				clientCloseFunc = (*secretmanager.Client).Close
				clientAccessSecretVersionFunc = (*secretmanager.Client).AccessSecretVersion
			}()

			got, err := GetSecretValue(ctx, tt.args.FS, tt.args.credentialsPath, tt.args.secret)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetSecretValue() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.expectedErrMsg != "" && !strings.Contains(err.Error(), tt.expectedErrMsg) {
				t.Errorf("Unexpected error msg: got = %s, want = %s", err.Error(), tt.expectedErrMsg)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetSecretValue() = %v, want %v", got, tt.want)
			}
		})
	}
}
