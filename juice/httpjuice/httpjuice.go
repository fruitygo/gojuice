// Description: This package provides a set of functions to create HTTP requests with a JSON body and a Bearer token.
package httpjuice

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/fruitygo/gojuice/cup/structcup"
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// CreateJSONHTTPRequestWithBearer creates an HTTP request with the given method, URL, idToken and body.
//
// It sets the Authorization header to "Bearer " followed by the provided idToken.
//
// It also sets the Content-Type header to "application/json".
//
// It returns the created *http.Request and any error encountered.
func CreateJSONHTTPRequestWithBearer(method, url, customTokenKey, idToken string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	// Set headers.
	req.Header.Set(customTokenKey, "Bearer "+idToken)
	req.Header.Set("Content-Type", "application/json")

	return req, nil
}

// CreateJSONHTTPRequestNoBearer creates an HTTP request with the given method, URL, idToken and body.
//
// It sets the customTokenKey header to the provided idToken.
//
// It also sets the Content-Type header to "application/json".
//
// It returns the created *http.Request and any error encountered.
func CreateJSONHTTPRequestNoBearer(method, url, customTokenKey, idToken string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	// Set headers.
	req.Header.Set(customTokenKey, idToken)
	req.Header.Set("Content-Type", "application/json")

	return req, nil
}

// ForwardHeaders forwards headers from an incoming request to an outgoing request.
func ForwardHeaders(incomingRequest, outgoingRequest *http.Request, headerKeys ...string) {
	for _, headerKey := range headerKeys {
		headerValue := incomingRequest.Header.Get(headerKey)
		if headerValue != "" {
			outgoingRequest.Header.Set(headerKey, headerValue)
		}
	}
}

// DoRequest performs an HTTP request.
//
// It blocks until a response is received or the context is done.
//
// A context with a timeout should be used to prevent the method from blocking indefinitely.
func DoRequest(ctx context.Context, client *http.Client, r *http.Request) (*http.Response, error) {
	// Create a channel to receive the response and error.
	respChan := make(chan *structcup.HTTPResponse)

	// Perform the HTTP request in a goroutine.
	go func() {
		defer close(respChan)
		resp, err := client.Do(r)
		respChan <- &structcup.HTTPResponse{Resp: resp, Err: err}
	}()

	// Wait for either the response or the context to be done.
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	case resp := <-respChan:
		if resp.Err != nil {
			return nil, resp.Err
		}
		return resp.Resp, nil
	}
}

// CreateRequestBody creates a request body from the given data.
func CreateRequestBody(data interface{}) (io.Reader, error) {
	// Encoding to JSON.
	marshalledObj, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	return bytes.NewBuffer(marshalledObj), nil
}

// ExtractHeaders extracts the headers from an HTTP request and format them as a map[string]string.
func ExtractHeaders(r *http.Request, headerKeys ...string) map[string]string {
	headerMap := make(map[string]string)

	for _, header := range headerKeys {
		if v := r.Header.Get(header); v != "" {
			headerMap[header] = v
		}
	}

	return headerMap
}
