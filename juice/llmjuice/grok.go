package llmjuice

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/fruitygo/gojuice/juice/httpjuice"
)

// ------------------------------
// FUNCTION VARIABLES (for testing)
// ------------------------------

var (
	jsonMarshalFunc        = json.Marshal
	jsonUnmarshalFunc      = json.Unmarshal
	httpNewRequestFunc     = http.NewRequest
	httpjuiceDoRequestFunc = httpjuice.DoRequest
	ioReadAllFunc          = io.ReadAll
)

// ------------------------------
// CONSTS
// ------------------------------

const (
	GrokChatCompletionsURL = "https://api.x.ai/v1/chat/completions"
	Grok2LatestModel       = "grok-2-latest"
)

// ------------------------------
// TYPES
// ------------------------------

// GrokPrompter is a prompter for the Grok LLM.
type GrokPrompter struct {
	APIKey string
	URL    string
	Model  string
}

// PromptRequest represents the payload for the LLM API request.
type PromptRequest struct {
	Model          string          `json:"model"`
	Messages       []Message       `json:"messages"`
	Stream         bool            `json:"stream,omitempty"`
	Temperature    float64         `json:"temperature,omitempty"`
	ResponseFormat *ResponseFormat `json:"response_format,omitempty"`
}

// Message represents a message in the conversation.
//
// For contextual messages, see:
//   - https://docs.x.ai/docs/guides/chat#conversations
type Message struct {
	Role    string `json:"role"`
	Content string `json:"content"`
}

// ResponseFormat represents the response format customization.
type ResponseFormat struct {
	Type       string            `json:"type"`
	JSONSchema *JSONSchemaFormat `json:"json_schema,omitempty"`
}

// JSONSchemaFormat represents the JSON schema format customization of the response.
type JSONSchemaFormat struct {
	Name   string                 `json:"name"`
	Schema map[string]interface{} `json:"schema"`
	Strict bool                   `json:"strict"`
}

// PromptResponse represents the response from the Grok LLM API.
type PromptResponse struct {
	ID      string `json:"id"`
	Object  string `json:"object"`
	Created int    `json:"created"`
	Model   string `json:"model"`
	Choices []struct {
		Index   int `json:"index"`
		Message struct {
			Role    string `json:"role"`
			Content string `json:"content"`
			Refusal string `json:"refusal,omitempty"`
		} `json:"message"`
		FinishReason string `json:"finish_reason"`
	} `json:"choices"`
	Usage struct {
		PromptTokens        int `json:"prompt_tokens"`
		CompletionTokens    int `json:"completion_tokens"`
		TotalTokens         int `json:"total_tokens"`
		PromptTokensDetails struct {
			TextTokens   int `json:"text_tokens"`
			AudioTokens  int `json:"audio_tokens"`
			ImageTokens  int `json:"image_tokens"`
			CachedTokens int `json:"cached_tokens"`
		} `json:"prompt_tokens_details"`
	} `json:"usage"`
	SystemFingerprint string `json:"system_fingerprint"`
}

// ------------------------------
// FUNCS
// ------------------------------

// NewGrokPrompter returns a new GrokPrompter instance.
func NewGrokPrompter(apiKey, url, model string) *GrokPrompter {
	return &GrokPrompter{
		APIKey: apiKey,
		URL:    url,
		Model:  model,
	}
}

// ------------------------------
// METHODS
// ------------------------------

// Prompt sends a prompt to the LLM and retrieves the response.
//
// Use contextual messages to provide context to the LLM.
//
// Additional options are not utilized by this implementation.
func (p *GrokPrompter) Prompt(ctx context.Context, opts PrompterOpts) (string, error) {
	// Build the message history with optional context.
	messages := []Message{
		{Role: MessageRoleSystem, Content: opts.SystemPrompt},
	}

	// Build the messages slice ending wiht the user prompt.
	for _, ctx := range opts.Context {
		messages = append(messages, Message{Role: ctx.Role, Content: ctx.Content})
	}
	messages = append(messages, Message{Role: MessageRoleUser, Content: opts.UserPrompt})

	// Construct the request payload.
	requestBody := PromptRequest{
		Model:          p.Model,
		Messages:       messages,
		Stream:         false, // Prompt method does not support streaming.
		Temperature:    opts.Temperature,
		ResponseFormat: opts.ResponseFormat,
	}

	// Serialize the request to JSON.
	jsonData, err := jsonMarshalFunc(requestBody)
	if err != nil {
		return "", fmt.Errorf("failed to serialize request: %v", err)
	}

	// Create the HTTP POST request.
	req, err := httpNewRequestFunc(http.MethodPost, p.URL, bytes.NewBuffer(jsonData))
	if err != nil {
		return "", fmt.Errorf("failed to create HTTP request: %v", err)
	}

	// Set headers.
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", p.APIKey))
	req.Header.Set("Content-Type", "application/json")

	// Send the request.
	client := &http.Client{}
	resp, err := httpjuiceDoRequestFunc(ctx, client, req)
	if err != nil {
		return "", fmt.Errorf("failed to send request: %v", err)
	}
	defer resp.Body.Close()

	// Read the response body.
	body, err := ioReadAllFunc(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read response: %v", err)
	}

	// Check if the response body is empty.
	if len(body) == 0 {
		return "", fmt.Errorf("response body is empty")
	}

	// Parse the response.
	var response PromptResponse
	if err := jsonUnmarshalFunc(body, &response); err != nil {
		// Try to parse the error response from the Grok API.
		var errorResponse struct {
			Code  string `json:"code"`
			Error string `json:"error"`
		}
		if err := json.Unmarshal(body, &errorResponse); err != nil {
			return "", fmt.Errorf("failed to parse response: %w", err)
		}

		// Return the extracted error.
		return "", fmt.Errorf("grok API error: %s - %s", errorResponse.Code, errorResponse.Error)
	}

	// Check if choices exist.
	if len(response.Choices) == 0 {
		return "", fmt.Errorf("no choices found in the response")
	}

	// Extract the content.
	content := response.Choices[0].Message.Content
	if content == "" {
		return "", fmt.Errorf("response content is empty")
	}

	return content, nil
}
