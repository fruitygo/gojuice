package llmjuice

// ------------------------------
// TYPES
// ------------------------------

// MessageRole represents the role of a message.
type MessageRole = string

// PrompterOpts represents the options for the Prompter.
type PrompterOpts struct {
	SystemPrompt   string                 // The system prompt for the LLM.
	UserPrompt     string                 // The user prompt for the LLM.
	Context        []Message              // The context for the LLM.
	Temperature    float64                // The temperature for the LLM.
	ResponseFormat *ResponseFormat        // The response format for the LLM.
	AdditionalOpts map[string]interface{} // Additional options for the LLM to be implemented by prompters, optionally.
}

// ------------------------------
// CONSTS
// ------------------------------

const (
	// MessageRoleSystem is the role for the system message.
	MessageRoleSystem MessageRole = "system"

	// MessageRoleUser is the role for the user message.
	MessageRoleUser MessageRole = "user"

	// MessageRoleAssistant is the role for the assistant message.
	MessageRoleAssistant MessageRole = "assistant"
)
