// Description: This package provides utility functions for logging.
package logjuice

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/fruitygo/gojuice/juice/validationjuice"
)

// ------------------------------
// CONSTANTS
// ------------------------------

const (
	GoogleMessageKey   googleLogKey = "message"
	GoogleSeverityKey  googleLogKey = "severity"
	GoogleTimestampKey googleLogKey = "timestamp"
	GoogleComponentKey googleLogKey = "component"
	GoogleTraceKey     googleLogKey = "trace"

	componentFailure  failureMessage = "failed to get caller information"
	nilDataFailure    failureMessage = "provided data is nil"
	jsonIndentFailure failureMessage = "failed to indent data into JSON"

	noIndent string = ""
	indent   string = "  "
	space    string = " "

	maxFieldLength = 500
)

// ------------------------------
// ENUMS
// ------------------------------

const (
	Debug LogLevel = iota
	Info
	Warn
	Error
)

// ------------------------------
// INTERFACES
// ------------------------------

type LoggerPort interface {
	Info(entry GoogleLogEntry)
	Error(entry GoogleLogEntry)
	Debug(entry GoogleLogEntry)
	Warn(entry GoogleLogEntry)
	Log(ctx context.Context, level LogLevel, message, component string)
	SLog(ctx context.Context, level LogLevel, emoji string, messages ...string)
	JSONLog(ctx context.Context, level LogLevel, data interface{}, emoji string, message ...string)
}

// ------------------------------
// ALIASES
// ------------------------------

type (
	googleLogKey   = string
	failureMessage = string
)

// ------------------------------
// TYPES
// ------------------------------

// LogLevel represents the level of a log entry.
type LogLevel int

// GoogleLogEntry represents a log entry for Google's logging service.
//
// It includes a message, trace, and component information.
type GoogleLogEntry struct {
	Message   string `json:"message"`
	Trace     string `json:"logging.googleapis.com/trace,omitempty"`
	Component string `json:"component,omitempty"`
}

type Logger struct {
	logger   slog.Logger
	traceKey interface{}
}

// ------------------------------
// FUNCTIONS
// ------------------------------

// NewLogger creates a new logger with the specified output and log level.
func NewLogger(out io.Writer, level slog.Level, traceKey interface{}) LoggerPort {
	options := &slog.HandlerOptions{
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.LevelKey {
				a.Key = GoogleSeverityKey
			}
			if a.Key == slog.MessageKey {
				a.Key = GoogleMessageKey
			}
			if a.Key == slog.TimeKey {
				a.Key = GoogleTimestampKey
			}
			return a
		},
		Level: level,
	}

	// Define the logger.
	logger := slog.New(slog.NewJSONHandler(out, options))

	return &Logger{
		logger:   *logger,
		traceKey: traceKey,
	}
}

// GetComponentWithFile returns details about the caller function, including the function name, file name, and line number.
//
// This information is used to populate the component field of a log entry when detailed caller information is required.
//
// If the caller's details cannot be retrieved, it returns a string indicating that the caller information could not be obtained.
func GetComponentWithFile() string {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		return componentFailure
	}

	callerFunc := runtime.FuncForPC(pc).Name()

	return fmt.Sprintf("%s (%s:%d)", callerFunc, file, line)
}

// GetComponent returns details about the caller function, including the function name and line number.
//
// This information is used to populate the component field of a log entry when concise caller information is sufficient.
//
// If the caller's details cannot be retrieved, it returns a string indicating that the caller information could not be obtained.
func GetComponent() string {
	pc, _, line, ok := runtime.Caller(1)
	if !ok {
		return componentFailure
	}

	callerFunc := runtime.FuncForPC(pc).Name()

	return fmt.Sprintf("%s:%d", callerFunc, line)
}

// GetTrace returns the trace value from the context.
//
// This function is used to retrieve the trace value from the context, which is used to correlate log entries.
func GetTrace(ctx context.Context, traceKey interface{}) string {
	if err := validationjuice.CheckNilValues(ctx, traceKey); err != nil {
		return ""
	}

	// Get the trace value from the context.
	traceValue := ctx.Value(traceKey)

	// Assert that the trace value is a string.
	trace, ok := traceValue.(string)
	if !ok {
		trace = ""
	}

	return trace
}

// GetComponentCustom returns details about the caller function, including the function name, file, and line number.
//
// This information is used to populate the component field of a log entry.
//
// If the caller's details cannot be retrieved, it returns a string indicating that the caller information could not be obtained.
func GetComponentCustom(skip int, includeFuncPath, includeFile, includeFilePath, includeLine bool) string {
	pc, file, line, ok := runtime.Caller(skip)
	if !ok {
		return componentFailure
	}

	callerFunc := runtime.FuncForPC(pc).Name()

	// If includeFuncPath is false, get only the function name.
	if !includeFuncPath {
		callerFunc = filepath.Base(callerFunc)
	}

	// If includeFile is false, ignore the file.
	fileComponent := ""

	if includeFile {
		// If includeFilePath is false, get only the file name.
		if !includeFilePath {
			file = filepath.Base(file)
		}

		if includeLine {
			// If includeLine is true, include the line number inside the parentheses with the file.
			fileComponent = fmt.Sprintf(" (%s:%d)", file, line)
		} else {
			fileComponent = fmt.Sprintf(" (%s)", file)
		}
	} else if includeLine {
		// If includeFile is false but includeLine is true, include the line number outside the parentheses.
		fileComponent = fmt.Sprintf(":%d", line)
	}

	return fmt.Sprintf("%s%s", callerFunc, fileComponent)
}

// ------------------------------
// INTERFACE IMPLEMENTATIONS
// ------------------------------

// Info logs an info message with the specified component and trace.
func (l *Logger) Info(entry GoogleLogEntry) {
	logger := l.logger.With(
		slog.String(GoogleComponentKey, entry.Component),
	)

	if entry.Trace != "" {
		logger = logger.With(
			slog.String(GoogleTraceKey, entry.Trace))
	}

	logger.Info(entry.Message)
}

// Error logs an error message with the specified component and trace.
func (l *Logger) Debug(entry GoogleLogEntry) {
	logger := l.logger.With(
		slog.String(GoogleComponentKey, entry.Component),
	)

	if entry.Trace != "" {
		logger = logger.With(
			slog.String(GoogleTraceKey, entry.Trace))
	}

	logger.Debug(entry.Message)
}

// Error logs an error message with the specified component and trace.
func (l *Logger) Error(entry GoogleLogEntry) {
	logger := l.logger.With(
		slog.String(GoogleComponentKey, entry.Component),
	)

	if entry.Trace != "" {
		logger = logger.With(
			slog.String(GoogleTraceKey, entry.Trace))
	}

	logger.Error(entry.Message)
}

// Warn logs a warning message with the specified component and trace.
func (l *Logger) Warn(entry GoogleLogEntry) {
	logger := l.logger.With(
		slog.String(GoogleComponentKey, entry.Component),
	)

	if entry.Trace != "" {
		logger = logger.With(
			slog.String(GoogleTraceKey, entry.Trace))
	}

	logger.Warn(entry.Message)
}

// Log logs a message with the specified level & component.
//
// Trace is retrieved from the context using the logger's trace key.
func (l *Logger) Log(ctx context.Context, level LogLevel, message, component string) {
	entry := GoogleLogEntry{
		Message:   message,
		Component: component,
		Trace:     GetTrace(ctx, l.traceKey),
	}

	switch level {
	case Debug:
		l.Debug(entry)
	case Info:
		l.Info(entry)
	case Warn:
		l.Warn(entry)
	case Error:
		l.Error(entry)
	default:
		l.Info(entry)
	}
}

// ------------------------------
// ENHANCED LOGGING METHODS
// ------------------------------

// SLog logs a structured message with the specified level, emoji & messages, in a structured way following Google Cloud's logging guidelines.
//
// Trace is retrieved from the context using the logger's trace key.
func (l *Logger) SLog(ctx context.Context, level LogLevel, emoji string, messages ...string) {
	// Trim each message to a maximum length.
	trimmedMessages := make([]string, len(messages))
	for i, message := range messages {
		if len(message) > maxFieldLength {
			trimmedMessages[i] = message[:maxFieldLength] + "..."
		} else {
			trimmedMessages[i] = message
		}
	}

	// Build a short component string.
	shortComponent := GetComponentCustom(3, false, false, false, false)

	// Set error emoji if the level is Error.
	if level == Error {
		emoji = "❌"
	}

	// Build the log message.
	msg := fmt.Sprintf("%s%s%s", spacer(emoji), spacer(shortComponent), strings.Join(trimmedMessages, space))

	// Log the message.
	l.Log(ctx, level, msg, GetComponentCustom(3, true, true, true, true))
}

// func (l *Logger) SLog(ctx context.Context, level LogLevel, emoji string, messages ...string) {
// 	// Build a short component string.
// 	shortComponent := GetComponentCustom(3, false, false, false, false)
//
// 	// Set error emoji if the level is Error.
// 	if level == Error {
// 		emoji = "❌"
// 	}
//
// 	// Build the log message.
// 	msg := fmt.Sprintf("%s%s%s", spacer(emoji), spacer(shortComponent), strings.Join(messages, space))
//
// 	// Log the message.
// 	l.Log(ctx, level, msg, GetComponentCustom(3, true, true, true, true))
// }

// JSONLog logs a structured message with the specified level, emoji & messages, in a structured way following Google Cloud's logging guidelines.
//
// The data parameter is expected to be a struct or map that can be marshaled into JSON.
//
// Message field will be populated with the indent JSON string of the data.
//
// If the data is nil, the message field will be populated with a message indicating that the data is nil.
//
// If the data cannot be indented into a JSON string, the message field will be populated with a message indicating that the data could not be indented.
//
// Trace is retrieved from the context using the logger's trace key.
func (l *Logger) JSONLog(ctx context.Context, level LogLevel, data interface{}, emoji string, messages ...string) {
	dataStr := ""

	// Check for nil values in data.
	if err := validationjuice.CheckNilValues(data); err != nil {
		dataStr = nilDataFailure
	}

	// Build a short component string.
	shortComponent := GetComponentCustom(3, false, false, false, false)

	// Set error emoji if the level is Error.
	if level == Error {
		emoji = "❌"
	}

	// If data is not nil, process it.
	if dataStr != nilDataFailure {
		trimmedData := trimJSON(data, maxFieldLength)

		// Indent the trimmed JSON data for readability.
		indented, err := json.MarshalIndent(trimmedData, noIndent, indent)
		if err != nil {
			dataStr = jsonIndentFailure
		} else {
			dataStr = string(indented)
		}
	}

	// Build the log message
	dataStr = fmt.Sprintf("%s%s%s\n%s", spacer(emoji), spacer(shortComponent), strings.Join(messages, space), dataStr)

	// Log the message
	l.Log(ctx, level, dataStr, GetComponentCustom(3, true, true, true, true))
}

// ------------------------------
// HELPERS
// ------------------------------

// spacer returns a string with a space appended to it if the string is not empty.
func spacer(s string) string {
	if s != "" {
		return fmt.Sprintf("%s ", s)
	}
	return s
}

// trimJSON ensures that the input is converted into a form that can be processed before trimming fields.
// It recursively trims all string fields in a JSON object or an array if they exceed maxFieldLength.
func trimJSON(data interface{}, maxFieldLength int) interface{} {
	switch v := data.(type) {
	case map[string]interface{}:
		return trimFields(v, maxFieldLength)
	case []interface{}:
		return trimArray(v, maxFieldLength)
	default:
		marshaled, err := json.Marshal(data)
		if err != nil {
			return map[string]interface{}{"_error": "[ERROR: Failed to convert input to JSON]"}
		}

		// Check if input is a slice or a struct.
		var jsonData interface{}
		if err := json.Unmarshal(marshaled, &jsonData); err != nil {
			return map[string]interface{}{"_error": "[ERROR: Failed to unmarshal JSON]"}
		}
		switch jsonVal := jsonData.(type) {
		case map[string]interface{}:
			return trimFields(jsonVal, maxFieldLength)
		case []interface{}:
			return trimArray(jsonVal, maxFieldLength)
		default:
			return map[string]interface{}{"_error": "[ERROR: Unexpected data type after JSON conversion]"}
		}
	}
}

// trimFields is a helper function that trims long string fields inside a map[string]interface{}.
func trimFields(data map[string]interface{}, maxLength int) map[string]interface{} {
	trimmedMap := make(map[string]interface{})
	truncated := false

	for key, value := range data {
		switch v := value.(type) {
		case string:
			if len(v) > maxLength {
				trimmedMap[key] = v[:maxLength] + "..."
				truncated = true
			} else {
				trimmedMap[key] = v
			}
		case map[string]interface{}:
			trimmedMap[key] = trimFields(v, maxLength) // Recursively trim nested maps.
		case []interface{}:
			trimmedMap[key] = trimArray(v, maxLength) // Process arrays separately.
		default:
			trimmedMap[key] = v // Keep non-string values unchanged.
		}
	}

	// If any field was truncated, add a warning field.
	if truncated {
		trimmedMap["_warning"] = "[WARNING: Some fields were truncated]"
	}

	return trimmedMap
}

// trimArray is a helper function that trims long string elements inside an array.
func trimArray(arr []interface{}, maxLength int) []interface{} {
	trimmedArray := make([]interface{}, len(arr))
	for i, item := range arr {
		switch v := item.(type) {
		case string:
			if len(v) > maxLength {
				trimmedArray[i] = v[:maxLength] + "..."
			} else {
				trimmedArray[i] = v
			}
		case map[string]interface{}:
			trimmedArray[i] = trimFields(v, maxLength) // Recursively trim nested objects
		case []interface{}:
			trimmedArray[i] = trimArray(v, maxLength) // Recursively trim nested arrays
		default:
			trimmedArray[i] = v // Keep non-string values unchanged
		}
	}
	return trimmedArray
}
