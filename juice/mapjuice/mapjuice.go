// Description: The mapjuice package provides functions for working with maps.
package mapjuice

// ------------------------------
// FUNCTIONS
// ------------------------------

// RenameKey renames a key in a map.
//
// If the oldKey does not exist, the map is not modified.
//
// To modify multiple keys, call this function multiple times.
func RenameKey(data map[string][]string, oldKey, newKey string) {
	// Check if the oldKey exists.
	if values, ok := data[oldKey]; ok {
		// Assign the values to the newKey.
		data[newKey] = values

		// Delete the oldKey.
		delete(data, oldKey)
	}
}
