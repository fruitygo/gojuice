// Description: This package provides a factory registry for creating instances of different structs.
// It also provides an interface for the factory registry.
package modeljuice

import (
	"fmt"
	"reflect"
	"sync"
)

// ------------------------------
// TYPES
// ------------------------------

// factoryRegistry is a concurrent-safe registry for struct instance creation functions.
type factoryRegistry struct {
	mu          sync.Mutex              // Protects instanceMap from concurrent writes.
	instanceMap map[string]reflect.Type // Maps struct names to the actual struct type.
}

// ------------------------------
// INTERFACES
// ------------------------------

// FactoryRegistry is a registry for creating instances of different structs.
type FactoryRegistryInterface interface {
	RegisterStruct(objectName string, structInstance interface{}) error
	Instanciate(objectName string) (interface{}, error)
}

// ------------------------------
// FUNCTIONS
// ------------------------------

// NewFactoryRegistry creates a new factory registry and returns it as a FactoryRegistryInterface.
func NewFactoryRegistry() FactoryRegistryInterface {
	return &factoryRegistry{
		instanceMap: make(map[string]reflect.Type),
	}
}

// ------------------------------
// METHODS
// ------------------------------

// RegisterStruct registers a struct or a pointer to a struct and ensures only structs are allowed.
//
// If a pointer is provided, it extracts the element type for safe instantiation.
//
// Returns an error if the provided type is not a struct or a pointer to a struct.
func (f *factoryRegistry) RegisterStruct(structName string, structInstance interface{}) error {
	f.mu.Lock()
	defer f.mu.Unlock()

	structType := reflect.TypeOf(structInstance)

	// If a pointer is provided, unwrap it to check the actual type.
	if structType.Kind() == reflect.Ptr {
		structType = structType.Elem()
	}

	// Ensure the type is a struct only.
	if structType.Kind() != reflect.Struct {
		return fmt.Errorf("only structs or pointers to structs are allowed, got %s", structType.Kind())
	}

	// Register the validated struct type.
	f.instanceMap[structName] = structType

	return nil
}

// Instanciate creates a new instance of a struct registered with the factory registry.
//
// Always returns a new pointer to a struct.
func (f *factoryRegistry) Instanciate(structName string) (interface{}, error) {
	f.mu.Lock()
	defer f.mu.Unlock()

	structType, found := f.instanceMap[structName]
	if !found {
		return nil, fmt.Errorf("unknown struct name: %s", structName)
	}

	return reflect.New(structType).Interface(), nil
}
