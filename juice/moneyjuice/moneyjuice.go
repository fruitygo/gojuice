package moneyjuice

import "math"

// BankerRoundInt rounds a float64 value representing a monetary amount to the nearest integer using banker's rounding.
//
// This method is designed specifically for financial systems where currency values are stored as the smallest denomination (e.g., cents for USD) in int64 variables.
//
// It considers the sign of the value and rounds to the nearest even integer when the fractional part is exactly 0.5.
func BankerRoundInt(x float64) int64 {
	// Retrieve sign.
	sign := math.Signbit(x)
	var signInt int64
	if sign {
		signInt = -1
	} else {
		signInt = 1
	}

	whole := math.Floor(math.Abs(x))
	fraction := math.Abs(x - whole)

	// Check for exact half and even integer.
	if fraction == 0.5 && math.Mod(whole, 2) == 0 {
		// Round down for even whole number.
		return int64(whole) * signInt
	} else if fraction == 0.5 {
		// Round up for odd whole number.
		return int64(whole+1) * signInt
	} else {
		// Round towards the nearest integer
		return int64(math.Round(math.Abs(x))) * signInt
	}
}
