// Description: This package provides a set of utility functions for working with MongoDB databases using the official MongoDB Go driver.
// The functions in this package are designed to be used in combination with the official MongoDB Go driver.
// The package provides functions for creating, reading, updating, and deleting documents in a MongoDB collection.
// It also provides functions for creating BSON filters from query parameters, and for performing text searches and paginated searches.
package mongojuice

import (
	"context"
	"fmt"
	"reflect"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/fruitygo/gojuice/cup/structcup"
	"gitlab.com/fruitygo/gojuice/juice/slicejuice"
	"gitlab.com/fruitygo/gojuice/juice/stringjuice"
	"gitlab.com/fruitygo/gojuice/juice/validationjuice"
)

// ------------------------------
// TYPES
// ------------------------------

// TextSearch represents a text search operation.
//
// It includes an index name, a query, and fields to search in.
type TextSearch struct {
	IndexName string
	Query     string
	Fields    []string
}

// PaginationOpts represents the options for pagination.
//
// It includes a page number and a page size.
type PaginationOpts struct {
	PageNumber int
	PageSize   int
}

// ChangeEvent represents a change event in a MongoDB change stream.
type ChangeEvent struct {
	OperationType string      `json:"operation_type"`
	FullDocument  interface{} `json:"full_document"`
}

// DateMatchOptions encapsulates the fields and reference date components
type DateMatchOptions struct {
	YearField  string
	MonthField string
	DayField   string
	RefYear    uint
	RefMonth   uint
	RefDay     uint
}

// ------------------------------
// ALIASES
// ------------------------------

// MatchBehavior defines different behaviors for matching MongoDB documents.
type MatchBehavior = string

// SortDirection defines the direction of sorting for MongoDB queries.
type SortDirection = int

// BSONOperator defines different BSON operators for MongoDB queries.
type BSONOperator = string

// MongoOperationType constants represent the operationType values for MongoDB change stream pipeline to be utilized with "operationType" field.
type MongoOperationType = string

// MongoLookupTarget defines the target field for the $lookup stage in a MongoDB aggregation pipeline for change streams.
type MongoLookupTarget = string

// AtlasDataType represents the data type recognized by MongoDB Atlas.
type AtlasDataType = string

// MongoOutCSChan is a type alias for a channel that sends decoded MongoDB change stream responses out a function.
type MongoOutCSChan = chan *ChangeEvent

// ------------------------------
// CONSTANTS
// ------------------------------

const (
	// MatchAll matches documents where the field value contains all of the specified values.
	MatchAll MatchBehavior = "$all"
	// MatchIn matches documents where the field value is in the specified list of values.
	MatchIn MatchBehavior = "$in"
	// MatchEqual matches documents where the field value is equal to the specified value.
	MatchEqual MatchBehavior = "$eq"
	// Ascending represents the ascending sort direction.
	Ascending SortDirection = 1
	// Descending represents the descending sort direction.
	Descending SortDirection = -1
	// AndOperator represents the $and operator.
	AndOperator BSONOperator = "$and"
	// OrOperator represents the $or operator.
	OrOperator BSONOperator = "$or"
	// NorOperator represents the $nor operator.
	NorOperator BSONOperator = "$nor"

	MongoOperationTypeCreate                   MongoOperationType = "create"
	MongoOperationTypeCreateIndexes            MongoOperationType = "createIndexes"
	MongoOperationTypeDelete                   MongoOperationType = "delete"
	MongoOperationTypeDrop                     MongoOperationType = "drop"
	MongoOperationTypeDropDatabase             MongoOperationType = "dropDatabase"
	MongoOperationTypeDropIndexes              MongoOperationType = "dropIndexes"
	MongoOperationTypeInsert                   MongoOperationType = "insert"
	MongoOperationTypeInvalidate               MongoOperationType = "invalidate"
	MongoOperationTypeModify                   MongoOperationType = "modify"
	MongoOperationTypeRefineCollectionShardKey MongoOperationType = "refineCollectionShardKey"
	MongoOperationTypeRename                   MongoOperationType = "rename"
	MongoOperationTypeReplace                  MongoOperationType = "replace"
	MongoOperationTypeReshardCollection        MongoOperationType = "reshardCollection"
	MongoOperationTypeShardCollection          MongoOperationType = "shardCollection"
	MongoOperationTypeUpdate                   MongoOperationType = "update"

	Array            AtlasDataType = "array"
	Boolean          AtlasDataType = "boolean"
	Date             AtlasDataType = "date"
	DateFacet        AtlasDataType = "dateFacet"
	Double           AtlasDataType = "number"
	DoubleFacet      AtlasDataType = "numberFacet"
	DoubleDeprecated AtlasDataType = "knnVector" // Deprecated
	GeoJSON          AtlasDataType = "geo"
	Number           AtlasDataType = "number"
	NumberFacet      AtlasDataType = "numberFacet"
	Int64            AtlasDataType = "number"
	Int64Facet       AtlasDataType = "numberFacet"
	Object           AtlasDataType = "document"
	ObjectArray      AtlasDataType = "embeddedDocument" // For array of objects
	ObjectID         AtlasDataType = "objectId"
	String           AtlasDataType = "string"
	StringFacet      AtlasDataType = "stringFacet"
	Autocomplete     AtlasDataType = "autocomplete"
	Token            AtlasDataType = "token"

	MongoLookupFullDocument  MongoLookupTarget = "fullDocument"
	MongoLookupDocumentKey   MongoLookupTarget = "documentKey"
	MongoLookupOperationType MongoLookupTarget = "operationType"
)

// ------------------------------
// MONGO TRANSACTION FUNCTIONS
// ------------------------------

// StartSessionAndTransaction starts a session and a transaction on the given client.
//
// This method should be called prior to performing any operations that require a transaction.
//
// Do not forget to end the session after the transaction is completed using defer session.EndSession().
func StartSessionAndTransaction(ctx context.Context, client *mongo.Client) (mongo.SessionContext, error) {
	// Start a session
	session, err := client.StartSession()
	if err != nil {
		return nil, fmt.Errorf("failed to start session: %w", err)
	}

	// Start a transaction
	err = session.StartTransaction()
	if err != nil {
		session.EndSession(ctx)
		return nil, fmt.Errorf("failed to start transaction: %w", err)
	}

	return mongo.NewSessionContext(ctx, session), nil
}

// ------------------------------
// MONGO BSON FILTER FUNCTIONS
// ------------------------------

// FilterFromQueryParams creates a BSON filter from the given query parameters.
//
// This function is used to convert query parameters from a web request into a filter that can be used by a MongoDB query.
func FilterFromQueryParams(convertValues bool, queryParams map[string][]string, matchBehavior MatchBehavior, prependStr, appendStr string, excludedFields ...string) (bson.M, error) {
	query := bson.M{}

	// Iterate over the query parameters.
	for key, values := range queryParams {
		// Check if the key is in the excluded fields list.
		if slicejuice.ContainsStringMap(excludedFields, key) {
			continue
		}

		// Convert the values based on their types.
		convertedValues := make([]interface{}, len(values))
		for i, value := range values {
			if convertValues {
				convertedValue, err := stringjuice.ConvertToType(value)
				if err != nil {
					return nil, fmt.Errorf("failed to convert value for key %s: %w", key, err)
				}
				convertedValues[i] = convertedValue
			} else {
				convertedValues[i] = value
			}
		}

		// Build the key with the prepend and append strings.
		fullKey := fmt.Sprintf("%s%s%s", prependStr, key, appendStr)

		// Append the values to the query map.
		existingValues := query[fullKey]
		switch existingValuesTyped := existingValues.(type) {
		case []interface{}:
			query[fullKey] = append(existingValuesTyped, convertedValues...)
		case nil:
			query[fullKey] = bson.M{string(matchBehavior): convertedValues}
		default:
			return nil, fmt.Errorf("unsupported type for key %s in query map", key)
		}
	}

	return query, nil
}

// FilterByFieldValueInArray creates a BSON filter that matches documents where the specified field contains one of the values provided in the array.
//
// This function uses the $in operator to match documents where the field value is in the specified list of values.
func FilterByFieldValueInArray(field string, values []interface{}) bson.M {
	return bson.M{
		field: bson.M{
			MatchIn: values,
		},
	}
}

// FilterByArrayValue creates a BSON filter that matches documents where the specified value is contained in the specified array field.
//
// This function uses the `$elemMatch` operator to match documents where the specified value is equal to the specified value.
//
// It uses the $eq operator to match documents where the field value is equal to the specified value.
func FilterByArrayValue(arrayField string, prependStr, appendStr string, value interface{}) bson.M {
	// Build the key with the prepend and append strings.
	fullKey := fmt.Sprintf("%s%s%s", prependStr, arrayField, appendStr)

	return bson.M{
		fullKey: bson.M{
			"$elemMatch": bson.M{
				MatchEqual: value,
			},
		},
	}
}

// FilterByArrayValues creates a BSON filter that matches documents where the specified values are contained in the specified array field.
//
// This function uses the `$elemMatch` operator to match documents where the specified values are in the specified list of values.
//
// It uses the $in operator to match documents where the field value is in the specified list of values.
func FilterByArrayValues(arrayField string, prependStr, appendStr string, values []interface{}) bson.M {
	// Build the key with the prepend and append strings.
	fullKey := fmt.Sprintf("%s%s%s", prependStr, arrayField, appendStr)

	return bson.M{
		fullKey: bson.M{
			"$elemMatch": bson.M{
				MatchIn: values,
			},
		},
	}
}

// FilterByAllArrayValues creates a BSON filter that matches documents where the specified values are contained in the specified array field.
//
// This function uses the `$all` operator to match documents where the specified values are in the specified list of values.
func FilterByAllArrayValues(arrayField string, prependStr, appendStr string, values []interface{}) bson.M {
	// Build the key with the prepend and append strings.
	fullKey := fmt.Sprintf("%s%s%s", prependStr, arrayField, appendStr)

	return bson.M{
		fullKey: bson.M{
			MatchAll: values,
		},
	}
}

// ------------------------------
// MONGO CRUD FUNCTIONS
// ------------------------------

// Create creates a new document in the given collection using the provided data.
//
// The data parameter should be a struct or a pointer to a struct.
func Create(ctx context.Context, collection *mongo.Collection, data interface{}) (primitive.ObjectID, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, data); err != nil {
		return primitive.NilObjectID, err
	}

	// Check that the data parameter is a struct or a pointer to a struct.
	if reflect.TypeOf(data).Kind() != reflect.Struct && reflect.TypeOf(data).Kind() != reflect.Ptr {
		return primitive.NilObjectID, fmt.Errorf("data must be a struct or a pointer to a struct")
	}

	// Insert the data into the collection.
	res, err := collection.InsertOne(ctx, data)
	if err != nil {
		return primitive.NilObjectID, err
	}

	// Assert the inserted ID to a primitive.ObjectID.
	insertedID, ok := res.InsertedID.(primitive.ObjectID)
	if !ok {
		return primitive.NilObjectID, fmt.Errorf("failed to get inserted ID")
	}

	// Return the inserted ID.
	return insertedID, nil
}

// FindByID finds a document in the given collection by its ID and decodes it into the provided receiver.
//
// The receiver type should be a pointer to an instance of the desired type to decode the document into.
func FindByID(ctx context.Context, collection *mongo.Collection, receiver interface{}, IDKey string, IDValue primitive.ObjectID) error {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver); err != nil {
		return err
	}

	// Check that the receiver is a pointer to a struct.
	if reflect.TypeOf(receiver).Kind() != reflect.Ptr || reflect.TypeOf(receiver).Elem().Kind() != reflect.Struct {
		return fmt.Errorf("receiver must be a pointer to a struct")
	}

	// Find the document with the given ID.
	result := collection.FindOne(ctx, bson.M{IDKey: IDValue})
	if err := result.Err(); err != nil {
		return err
	}

	// Decode the document into the receiver.
	if err := result.Decode(receiver); err != nil {
		if err == mongo.ErrNoDocuments {
			return fmt.Errorf("no document found with id %v", IDValue)
		}
		return err
	}

	// Return nil if the document was found and decoded successfully.
	return nil
}

// DeleteByID deletes a document in the given collection by its ID.
func DeleteByID(ctx context.Context, collection *mongo.Collection, IDKey string, IDValue primitive.ObjectID) error {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection); err != nil {
		return err
	}

	// Delete the document with the given ID.
	result, err := collection.DeleteOne(ctx, bson.M{IDKey: IDValue})
	if err != nil {
		return err
	}

	// Check if the document was deleted.
	if result.DeletedCount == 0 {
		return fmt.Errorf("no document found with id %v", IDValue)
	}

	// Return nil if the document was deleted successfully.
	return nil
}

// FindAll finds all documents in the given collection, filters them based on the provided query parameters, and sorts them by the given key and direction.
//
// The underlying type of each element in the slice of interface returned will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
func FindAll(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, sortByKey string, sortDirection SortDirection) ([]interface{}, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, queryParams); err != nil {
		return nil, err
	}

	// Create a filter from the query parameters.
	query, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "")
	if err != nil {
		return nil, err
	}

	// Create a FindOptions object to specify the sort order.
	opts := options.Find()
	opts.SetSort(bson.D{{Key: sortByKey, Value: sortDirection}})

	// Find all documents in the collection.
	cursor, err := collection.Find(ctx, query, opts)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	// Decode the documents into a slice of interfaces.
	results, err := decodeCursor(ctx, cursor, receiver)
	if err != nil {
		return nil, err
	}

	// Return the results.
	return results, nil
}

// FindAllForceString finds all documents in the given collection, filters them based on the provided query parameters, and sorts them by the given key and direction.
//
// The underlying type of each element in the slice of interface returned will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// This function is similar to FindAll, but it does not convert the values in the query parameters to their respective types. Generated BSON filter will contain the values as strings.
func FindAllForceString(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, sortByKey string, sortDirection SortDirection) ([]interface{}, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, queryParams); err != nil {
		return nil, err
	}

	// Create a filter from the query parameters.
	query, err := FilterFromQueryParams(false, queryParams, MatchIn, "", "")
	if err != nil {
		return nil, err
	}

	// Create a FindOptions object to specify the sort order.
	opts := options.Find()
	opts.SetSort(bson.D{{Key: sortByKey, Value: sortDirection}})

	// Find all documents in the collection.
	cursor, err := collection.Find(ctx, query, opts)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	// Decode the documents into a slice of interfaces.
	results, err := decodeCursor(ctx, cursor, receiver)
	if err != nil {
		return nil, err
	}

	// Return the results.
	return results, nil
}

// UpdateByID updates a document in the given collection by its ID.
//
// The data parameter should be a struct or a pointer to a struct.
func UpdateByID(ctx context.Context, collection *mongo.Collection, data interface{}, IDKey string, IDValue primitive.ObjectID) error {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, data); err != nil {
		return err
	}

	// Check that the data parameter is a struct or a pointer to a struct.
	if reflect.TypeOf(data).Kind() != reflect.Struct && reflect.TypeOf(data).Kind() != reflect.Ptr {
		return fmt.Errorf("data must be a struct or a pointer to a struct")
	}

	// Replace the document with the given ID.
	_, err := collection.ReplaceOne(ctx, bson.M{IDKey: IDValue}, data)
	if err != nil {
		return err
	}

	// Return nil if the document was updated successfully.
	return nil
}

// ------------------------------
// MONGO SPECIALIZED CRUD FUNCTIONS
// ------------------------------

// FindAllByArrayValue finds all documents in the given collection, filters them based on the provided array field and value, and sorts them by the given key and direction.
//
// Results will contain only document where the specified value is contained in the specified array field.
//
// In this method, queryParams it not mandatory, but it can be used to filter the results even more.
func FindAllByArrayValue(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, arrayField string, value interface{}, sortByKey string, sortDirection SortDirection) ([]interface{}, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, value); err != nil {
		return nil, err
	}

	// Create a filter from the arrayField and value.
	arrayFilter := FilterByArrayValue(arrayField, "", "", value)

	// Create a filter from the query parameters.
	queryParamsFilter, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "")
	if err != nil {
		return nil, err
	}

	finalFilter := mergeBsonFilters(AndOperator, queryParamsFilter, arrayFilter)

	// Create a FindOptions object to specify the sort order.
	opts := options.Find()
	opts.SetSort(bson.D{{Key: sortByKey, Value: sortDirection}})

	// Find all documents in the collection.
	cursor, err := collection.Find(ctx, finalFilter, opts)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	// Decode the documents into a slice of interfaces.
	results, err := decodeCursor(ctx, cursor, receiver)
	if err != nil {
		return nil, err
	}

	// Return the results.
	return results, nil
}

// Search searches for documents in the given collection based on the provided query parameters, text search criteria, and pagination options.
//
// The underlying type of each element in the slice of interface (Results field) returned will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
func SearchText(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, textSearch *TextSearch, pagination *PaginationOpts, searchQueryKey, pageQueryKey, perPageQueryKey string) (*structcup.SearchResponse, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, queryParams, textSearch, pagination); err != nil {
		return nil, err
	}

	// Create a filter from the query parameters.
	query, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "", searchQueryKey, pageQueryKey, perPageQueryKey)
	if err != nil {
		return nil, err
	}

	// Perform the search.
	return searchDocs(ctx, collection, receiver, query, textSearch, pagination, nil, nil)
}

// SearchTextByArrayValues searches for documents in the given collection based on the provided array field and values, text search criteria, and pagination options.
//
// The underlying type of each element in the slice of interface (Results field) returned will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// Results will contain only document where the specified value is contained in the specified array field.
//
// In this method, queryParams it not mandatory, but it can be used to filter the results even more.
func SearchTextByArrayValue(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, arrayField string, value interface{}, textSearch *TextSearch, pagination *PaginationOpts) (*structcup.SearchResponse, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, value, textSearch, pagination); err != nil {
		return nil, err
	}

	// Create a filter from the arrayField and value.
	arrayFilter := FilterByArrayValue(arrayField, "", "", value)

	// Create a filter from the query parameters.
	queryParamsFilter, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "")
	if err != nil {
		return nil, err
	}

	finalFilter := mergeBsonFilters(AndOperator, queryParamsFilter, arrayFilter)

	// Perform the search.
	return searchDocs(ctx, collection, receiver, finalFilter, textSearch, pagination, nil, nil)
}

// SearchTextByArrayValues searches for documents in the given collection based on the provided array field and values, text search criteria, and pagination options.
//
// The underlying type of each element in the slice of interface (Results field) returned will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// Results will contain only document where a minimum of one of the specified values is contained in the specified array field.
//
// In this method, queryParams it not mandatory, but it can be used to filter the results even more.
func SearchTextByArrayValues(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, arrayField string, values []interface{}, textSearch *TextSearch, pagination *PaginationOpts) (*structcup.SearchResponse, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, values, textSearch, pagination); err != nil {
		return nil, err
	}

	// Create a filter from the arrayField and values.
	arrayFilter := FilterByArrayValues(arrayField, "", "", values)

	// Create a filter from the query parameters.
	queryParamsFilter, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "")
	if err != nil {
		return nil, err
	}

	finalFilter := mergeBsonFilters(AndOperator, queryParamsFilter, arrayFilter)

	// Perform the search.
	return searchDocs(ctx, collection, receiver, finalFilter, textSearch, pagination, nil, nil)
}

// SearchTextByAllArrayValues searches for documents in the given collection based on the provided array field and values, text search criteria, and pagination options.
//
// The underlying type of each element in the slice of interface (Results field) returned will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// Results will contain only document where all of the specified values are contained in the specified array field.
//
// In this method, queryParams it not mandatory, but it can be used to filter the results even more.
func SearchTextByAllArrayValues(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, arrayField string, values []interface{}, textSearch *TextSearch, pagination *PaginationOpts) (*structcup.SearchResponse, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, values, textSearch, pagination); err != nil {
		return nil, err
	}

	// Create a filter from the arrayField and values.
	arrayFilter := FilterByAllArrayValues(arrayField, "", "", values)

	// Create a filter from the query parameters.
	queryParamsFilter, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "")
	if err != nil {
		return nil, err
	}

	finalFilter := mergeBsonFilters(AndOperator, queryParamsFilter, arrayFilter)

	// Perform the search.
	return searchDocs(ctx, collection, receiver, finalFilter, textSearch, pagination, nil, nil)
}

// FindAllPaginated finds all documents in the given collection, filters them based on the provided query parameters, and sorts them by the given key and direction.
//
// The underlying type of each element in the slice of interface (Results field) returned will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// Documents will match all the query parameters, but if a query parameter has multiple values, documents will match any of the values within the slice.
func FindAllPaginated(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, pagination *PaginationOpts, searchQueryKey, pageQueryKey, perPageQueryKey, sortByKey string, sortDirection SortDirection) (*structcup.SearchResponse, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, queryParams, pagination); err != nil {
		return nil, err
	}

	// Create a filter from the query parameters.
	query, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "", searchQueryKey, pageQueryKey, perPageQueryKey)
	if err != nil {
		return nil, err
	}

	// Perform the search without text search.
	return searchDocs(ctx, collection, receiver, query, nil, pagination, &sortByKey, &sortDirection)
}

// FindAllPaginatedByArrayValue finds all documents in the given collection, filters them based on the provided array field and value, and sorts them by the given key and direction.
//
// Results will contain only document where the specified value is contained in the specified array field.
//
// In this method, queryParams it not mandatory, but it can be used to filter the results even more.
func FindAllPaginatedByArrayValue(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, arrayField string, value interface{}, pagination *PaginationOpts, sortByKey string, sortDirection SortDirection) (*structcup.SearchResponse, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, value, pagination, value); err != nil {
		return nil, err
	}

	// Create a filter from the arrayField and value.
	arrayFilter := FilterByArrayValue(arrayField, "", "", value)

	// Create a filter from the query parameters.
	queryParamsFilter, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "")
	if err != nil {
		return nil, err
	}

	finalFilter := mergeBsonFilters(AndOperator, queryParamsFilter, arrayFilter)

	// Perform the search without text search.
	return searchDocs(ctx, collection, receiver, finalFilter, nil, pagination, &sortByKey, &sortDirection)
}

// FindAllPaginatedByArrayValues finds all documents in the given collection, filters them based on the provided array field and values, and sorts them by the given key and direction.
//
// Results will contain only document where a minimum of one of the specified values is contained in the specified array field.
//
// In this method, queryParams it not mandatory, but it can be used to filter the results even more.
func FindAllPaginatedByArrayValues(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, arrayField string, values []interface{}, pagination *PaginationOpts, sortByKey string, sortDirection SortDirection) (*structcup.SearchResponse, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, values, pagination); err != nil {
		return nil, err
	}

	// Create a filter from the arrayField and values.
	arrayFilter := FilterByArrayValues(arrayField, "", "", values)

	// Create a filter from the query parameters.
	queryParamsFilter, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "")
	if err != nil {
		return nil, err
	}

	finalFilter := mergeBsonFilters(AndOperator, queryParamsFilter, arrayFilter)

	// Perform the search without text search.
	return searchDocs(ctx, collection, receiver, finalFilter, nil, pagination, &sortByKey, &sortDirection)
}

// FindAllPaginatedByAllArrayValues finds all documents in the given collection, filters them based on the provided array field and values, and sorts them by the given key and direction.
//
// Results will contain only document where all of the specified values are contained in the specified array field.
//
// In this method, queryParams it not mandatory, but it can be used to filter the results even more.
func FindAllPaginatedByAllArrayValues(ctx context.Context, collection *mongo.Collection, receiver interface{}, queryParams map[string][]string, arrayField string, values []interface{}, pagination *PaginationOpts, sortByKey string, sortDirection SortDirection) (*structcup.SearchResponse, error) {
	// Validate the input parameters.
	if err := validationjuice.CheckNilValues(collection, receiver, values, pagination); err != nil {
		return nil, err
	}

	// Create a filter from the arrayField and values.
	arrayFilter := FilterByAllArrayValues(arrayField, "", "", values)

	// Create a filter from the query parameters.
	queryParamsFilter, err := FilterFromQueryParams(true, queryParams, MatchIn, "", "")
	if err != nil {
		return nil, err
	}

	finalFilter := mergeBsonFilters(AndOperator, queryParamsFilter, arrayFilter)

	// Perform the search without text search.
	return searchDocs(ctx, collection, receiver, finalFilter, nil, pagination, &sortByKey, &sortDirection)
}

// ------------------------------
// MONGO CHANGE STREAM FUNCTIONS
// ------------------------------

// WatchCollection watches a collection for changes and sends the decoded responses out of the function.
//
// It uses the fullDocument lookup to get the full document for each change event.
//
// The underlying type of each document retrieved will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// A short context should not be used with this function, as it may cause the function to return prematurely.
func WatchCollection(ctx context.Context, collection *mongo.Collection, receiver interface{}, pipeline *mongo.Pipeline, opts *options.ChangeStreamOptions, out MongoOutCSChan) error {
	return watchColl(ctx, collection, receiver, pipeline, opts, MongoLookupFullDocument, out)
}

// WatchDatabase watches a database for changes and sends the decoded responses out of the function.
//
// It uses the fullDocument lookup to get the full document for each change event.
//
// The underlying type of each document retrieved will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// A short context should not be used with this function, as it may cause the function to return prematurely.
func WatchDatabase(ctx context.Context, db *mongo.Database, receiver interface{}, pipeline *mongo.Pipeline, opts *options.ChangeStreamOptions, out MongoOutCSChan) error {
	return watchDB(ctx, db, receiver, pipeline, opts, MongoLookupFullDocument, out)
}

// WatchCollectionWithLookup watches a collection for changes and sends the decoded responses out of the function.
//
// It uses the specified lookup to get the full document for each change event.
//
// The underlying type of each document retrieved will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// A short context should not be used with this function, as it may cause the function to return prematurely.
func WatchCollectionWithLookup(ctx context.Context, collection *mongo.Collection, receiver interface{}, pipeline *mongo.Pipeline, opts *options.ChangeStreamOptions, lookup string, out MongoOutCSChan) error {
	return watchColl(ctx, collection, receiver, pipeline, opts, lookup, out)
}

// WatchDatabaseWithLookup watches a database for changes and sends the decoded responses out of the function.
//
// It uses the specified lookup to get the full document for each change event.
//
// The underlying type of each document retrieved will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// A short context should not be used with this function, as it may cause the function to return prematurely.
func WatchDatabaseWithLookup(ctx context.Context, db *mongo.Database, receiver interface{}, pipeline *mongo.Pipeline, opts *options.ChangeStreamOptions, lookup string, out MongoOutCSChan) error {
	return watchDB(ctx, db, receiver, pipeline, opts, lookup, out)
}

// ------------------------------
// MONGO DATES FUNCTIONS
// ------------------------------

// GetFutureDateMatch generates a BSON match filter for future dates.
func GetFutureDateMatch(opts DateMatchOptions) bson.D {
	return bson.D{
		{Key: "$or", Value: bson.A{
			// Condition 1: Future years.
			bson.D{{Key: opts.YearField, Value: bson.D{{Key: "$gt", Value: opts.RefYear}}}},

			// Condition 2: Same year, but future months.
			bson.D{
				{Key: opts.YearField, Value: opts.RefYear},
				{Key: opts.MonthField, Value: bson.D{{Key: "$gt", Value: opts.RefMonth}}},
			},

			// Condition 3: Same year and month, but future days.
			bson.D{
				{Key: opts.YearField, Value: opts.RefYear},
				{Key: opts.MonthField, Value: opts.RefMonth},
				{Key: opts.DayField, Value: bson.D{{Key: "$gte", Value: opts.RefDay}}},
			},
		}},
	}
}

// GetPastDateMatch generates a BSON match filter for past dates.
func GetPastDateMatch(opts DateMatchOptions) bson.D {
	return bson.D{
		{Key: "$or", Value: bson.A{
			// Condition 1: Past years.
			bson.D{{Key: opts.YearField, Value: bson.D{{Key: "$lt", Value: opts.RefYear}}}},

			// Condition 2: Same year, but past months.
			bson.D{
				{Key: opts.YearField, Value: opts.RefYear},
				{Key: opts.MonthField, Value: bson.D{{Key: "$lt", Value: opts.RefMonth}}},
			},

			// Condition 3: Same year and month, but past days.
			bson.D{
				{Key: opts.YearField, Value: opts.RefYear},
				{Key: opts.MonthField, Value: opts.RefMonth},
				{Key: opts.DayField, Value: bson.D{{Key: "$lte", Value: opts.RefDay}}},
			},
		}},
	}
}

// GetExactDateMatch generates a BSON match filter for an exact date match (same year, month, and day).
func GetExactDateMatch(opts DateMatchOptions) bson.D {
	return bson.D{
		{Key: opts.YearField, Value: opts.RefYear},
		{Key: opts.MonthField, Value: opts.RefMonth},
		{Key: opts.DayField, Value: opts.RefDay},
	}
}

// ------------------------------
// HELPERS
// ------------------------------

// watchColl watches a collection for changes and sends the decoded responses out of the function.
//
// The underlying type of each document retrieved will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// A short context should not be used with this function, as it may cause the function to return prematurely.
func watchColl(ctx context.Context, collection *mongo.Collection, receiver interface{}, pipeline *mongo.Pipeline, opts *options.ChangeStreamOptions, lookup string, out MongoOutCSChan) error {
	// Validating the input parameters.
	if err := validationjuice.CheckNilValues(collection, pipeline, opts); err != nil {
		return err
	}

	// Check that the receiver is a pointer to a struct.
	if reflect.TypeOf(receiver).Kind() != reflect.Ptr || reflect.TypeOf(receiver).Elem().Kind() != reflect.Struct {
		return fmt.Errorf("receiver must be a pointer to a struct")
	}

	// Obtain a change stream for the collection.
	cs, err := collection.Watch(ctx, *pipeline, opts)
	if err != nil {
		return err
	}

	// Continuously listen for changes.
	err = listenAndDecodeCS(ctx, cs, lookup, receiver, out)
	if err != nil {
		return err
	}

	err = cs.Close(ctx)
	if err != nil {
		return err
	}

	return nil
}

// watchDB watches a database for changes and sends the decoded responses out of the function.
//
// The underlying type of each document retrieved will correspond to the type of the receiver.
//
// The receiver type should be the zero value of the desired type or a pointer to it.
//
// A short context should not be used with this function, as it may cause the function to return prematurely.
func watchDB(ctx context.Context, db *mongo.Database, receiver interface{}, pipeline *mongo.Pipeline, opts *options.ChangeStreamOptions, lookup string, out MongoOutCSChan) error {
	// Validating the input parameters.
	if err := validationjuice.CheckNilValues(db, pipeline, opts); err != nil {
		return err
	}

	// Check that the receiver is a pointer to a struct.
	if reflect.TypeOf(receiver).Kind() != reflect.Ptr || reflect.TypeOf(receiver).Elem().Kind() != reflect.Struct {
		return fmt.Errorf("receiver must be a pointer to a struct")
	}

	// Obtain a change stream for the database.
	cs, err := db.Watch(ctx, *pipeline, opts)
	if err != nil {
		return err
	}

	// Continuously listen for changes.
	err = listenAndDecodeCS(ctx, cs, lookup, receiver, out)
	if err != nil {
		return err
	}

	err = cs.Close(ctx)
	if err != nil {
		return err
	}

	return nil
}

// listenAndDecodeCS listens for changes in the given change stream and decodes the responses into the receiver.
func listenAndDecodeCS(ctx context.Context, cs *mongo.ChangeStream, lookup string, receiver interface{}, out MongoOutCSChan) error {
	errChan := make(chan error)

	go func() {
		for {
			if cs.Next(ctx) {
				next := cs.Current

				// Decode the document into a copy of the receiver.
				decodedReceiver := reflect.New(reflect.TypeOf(receiver).Elem()).Interface()

				// Lookup the required field.
				fullDocument := next.Lookup(lookup)

				// Retrieve operation type.
				operationType := strings.Trim(next.Lookup(MongoLookupOperationType).String(), `"`)

				// Unmarshal the full document into the receiver.
				if err := fullDocument.Unmarshal(decodedReceiver); err != nil {
					errChan <- err
					return
				}

				// Build the change event.
				changeEvent := &ChangeEvent{
					OperationType: operationType,
					FullDocument:  decodedReceiver,
				}

				// Send the decoded response out of the function.
				out <- changeEvent
			}

			if err := cs.Err(); err != nil {
				errChan <- err
				return
			}
		}
	}()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case err := <-errChan:
		return err
	}
}

// mergeBsonFilters merges the given BSON filters into a single filter using the `$and` operator.
func mergeBsonFilters(bsonOperator BSONOperator, filters ...bson.M) bson.M {
	if len(filters) == 0 {
		return bson.M{}
	}

	combined := make([]bson.M, 0, len(filters))

	combined = append(combined, filters...)

	return bson.M{bsonOperator: combined}
}

// decodeCursor decodes the documents in a MongoDB cursor into a slice.
//
// A slice of decoded documents, or an error if one occurred.
func decodeCursor(ctx context.Context, cursor *mongo.Cursor, receiver interface{}) ([]interface{}, error) {
	results := make([]interface{}, 0)

	for cursor.Next(ctx) {
		// Create a new instance of the receiver type.
		receiverCopy := reflect.New(reflect.TypeOf(receiver).Elem()).Interface()

		// Decode the document into the receiver copy.
		if err := cursor.Decode(receiverCopy); err != nil {
			return nil, err
		}

		// Append the receiver copy to the results slice.
		results = append(results, receiverCopy)
	}

	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return results, nil
}

// searchDocs searches for documents in the given collection based on the provided query, text search criteria, and pagination options.
func searchDocs(ctx context.Context, collection *mongo.Collection, receiver interface{}, query bson.M, textSearch *TextSearch, pagination *PaginationOpts, sortByKey *string, sortDirection *SortDirection) (*structcup.SearchResponse, error) {
	// Define the count pipeline.
	countPipeline := mongo.Pipeline{}

	// Define the text search fields.
	var convertedFields []interface{}

	// Define the text search filter.
	var searchFilter bson.D

	if textSearch != nil {
		for _, field := range textSearch.Fields {
			convertedFields = append(convertedFields, field)
		}

		searchFilter = bson.D{
			{
				Key: "$search", Value: bson.D{
					{Key: "index", Value: textSearch.IndexName},
					{Key: "text", Value: bson.D{
						{Key: "query", Value: textSearch.Query},
						{Key: "path", Value: convertedFields},
						{Key: "fuzzy", Value: bson.D{}},
					}},
				},
			},
		}

		countPipeline = append(countPipeline, searchFilter)
	}

	countPipeline = append(countPipeline, bson.D{{Key: "$match", Value: query}}, bson.D{{Key: "$count", Value: "total"}})

	countCursor, err := collection.Aggregate(ctx, countPipeline)
	if err != nil {
		return nil, err
	}
	defer countCursor.Close(ctx)

	var counts []bson.M

	if err = countCursor.All(ctx, &counts); err != nil {
		return nil, err
	}

	var totalCount int32 = 0
	var ok bool

	if len(counts) > 0 {
		totalCount, ok = counts[0]["total"].(int32)
		if !ok {
			return nil, fmt.Errorf("failed to convert total count to int32")
		}
	}

	// Define the search pipeline.
	pipeline := mongo.Pipeline{}

	if textSearch != nil {
		pipeline = append(pipeline, searchFilter)
	} else {
		// Perform sorting if textSearch is nil because textSearch sorts the results by relevance, but without textSearch, we need to sort the results by the specified key.
		pipeline = append(pipeline, bson.D{
			{Key: "$sort", Value: bson.D{
				{Key: *sortByKey, Value: *sortDirection},
			}},
		})
	}

	if pagination != nil {
		pipeline = append(pipeline,
			bson.D{
				{Key: "$match", Value: query},
			},
			bson.D{
				{Key: "$skip", Value: (pagination.PageNumber - 1) * pagination.PageSize},
			},
			bson.D{
				{Key: "$limit", Value: pagination.PageSize},
			})
	}

	cursor, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	results, err := decodeCursor(ctx, cursor, receiver)
	if err != nil {
		return nil, err
	}

	lastPage := int(totalCount) / pagination.PageSize
	if int(totalCount)%pagination.PageSize != 0 {
		lastPage++
	}

	data := &structcup.SearchResponse{
		Results:     results,
		TotalCount:  totalCount,
		CurrentPage: pagination.PageNumber,
		LastPage:    lastPage,
	}

	// Return the results.
	return data, nil
}
