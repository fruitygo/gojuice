// Description: Package multipartjuice provides a comprehensive set of utilities for handling multipart/form-data requests.
// It includes functions to create multipart requests, save files to a storage microservice, and perform POST requests using multipart bodies.
package multipartjuice

import (
	"bytes"
	"context"
	"fmt"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"net/url"
	"strconv"

	"gitlab.com/fruitygo/gojuice/juice/httpjuice"
)

// ----------------------------
// CONSTANTS
// ----------------------------

const (
	contentTypeKey        = "Content-Type"
	contentDispositionKey = "Content-Disposition"
	contentLengthKey      = "Content-Length"
	fileKey               = "file"
)

// ----------------------------
// TYPESS
// ----------------------------

type SaveFileParams struct {
	HTTPClient          *http.Client
	Body                *bytes.Buffer
	QueryParams         map[string][]string
	StorageURL          string
	FormDataContentType string
	ForwardHeaders      []string
	ExpectedStatus      int
}

// ----------------------------
// FUNCTIONS
// ----------------------------

// CreateMultipartBody creates a multipart request body and returns it with the appropriate content type.
//
// This function is designed to prepare a multipart form data body for an HTTP request. Multipart form data
// is commonly used for file uploads, as it allows the client to send files along with other form fields
// within a single HTTP request. The function returns both the constructed request body and the content type
// required for the HTTP header.
//
// Parameters:
//
//   - ctx: The context for the operation, which allows for handling cancellations and timeouts.
//   - file: A byte slice representing the file to be uploaded. This should contain the raw file data.
//   - fileName: The name of the file being uploaded. This will be used to set the `filename` parameter in
//     the `Content-Disposition` header, helping the server to process and store the file correctly.
//
// Returns:
//
//   - *bytes.Buffer: A buffer containing the multipart request body. This is ready to be used as the body
//     of an HTTP POST request.
//   - string: The content type for the multipart form data. This includes boundary information that is
//     critical for the server to parse the request properly. The content type should be set as the `Content-Type`
//     header in the HTTP request.
//   - error: An error object if any issues occur during the creation of the multipart body. Errors can arise
//     from problems with writing the file data or constructing the multipart structure.
func CreateMultipartBody(ctx context.Context, file []byte, fileName string) (*bytes.Buffer, string, error) {
	// Create a buffer to hold the multipart data.
	body := new(bytes.Buffer)

	// Create a new multipart writer.
	writer := multipart.NewWriter(body)

	// Detect file type and set content type accordingly.
	contentType := http.DetectContentType(file)

	// Set headers for the file part.
	partHeader := make(textproto.MIMEHeader)
	partHeader.Set(contentTypeKey, contentType)
	partHeader.Set(contentDispositionKey, fmt.Sprintf(`form-data; name="%s"; filename="%s"`, fileKey, fileName))
	partHeader.Set(contentLengthKey, strconv.FormatInt(int64(len(file)), 10))

	// Create the file part.
	part, err := writer.CreatePart(partHeader)
	if err != nil {
		return nil, "", err
	}

	// Write the file data to the part.
	_, err = part.Write(file)
	if err != nil {
		return nil, "", err
	}

	// Set headers for the owner ID part.
	err = writer.Close()
	if err != nil {
		return nil, "", err
	}

	// Check body length.
	if body.Len() == 0 {
		return nil, "", fmt.Errorf("body length is 0")
	}

	return body, writer.FormDataContentType(), nil
}

// SaveFile uploads a file to a storage microservice.
//
// Parameters:
//
// - ctx: The context for the request, used to manage cancellations and deadlines.
// - r: The original HTTP request from which headers will be forwarded.
// - params: A pointer to SaveFileParams, encapsulating the following:
//   - HTTPClient: The HTTP client used to send the request to the storage service.
//   - Body: The body of the request, containing the file data to be uploaded.
//   - QueryParams: A map of query parameters to include in the request URL.
//   - StorageURL: The URL of the storage microservice where the file will be saved.
//   - FormDataContentType: The content type of the multipart form data, including boundary delimiters.
//   - ForwardHeaders: A slice of headers to forward to the storage microservice.
//   - ExpectedStatus: The expected HTTP status code from the storage microservice.
//
// Returns:
//
// - An error if the file cannot be saved due to issues in request preparation, execution, or response handling.
func SaveFile(ctx context.Context, r *http.Request, params *SaveFileParams) error {
	// Parse the storage URL.
	u, err := url.Parse(params.StorageURL)
	if err != nil {
		return err
	}

	// Define the query parameters.
	q := u.Query()

	// Iterate over the queryParams map and add each key-value pair.
	for key, values := range params.QueryParams {
		for _, value := range values {
			q.Add(key, value)
		}
	}

	// Set the query parameters.
	u.RawQuery = q.Encode()

	// The updated storage URL is u.String().
	params.StorageURL = u.String()

	// Create a new HTTP request.
	req, err := http.NewRequest(http.MethodPost, params.StorageURL, params.Body)
	if err != nil {
		return err
	}

	// Forward authorization header to the request.
	httpjuice.ForwardHeaders(r, req, params.ForwardHeaders...)

	// Set content type header.
	req.Header.Set(contentTypeKey, params.FormDataContentType)

	// Execute the request.
	resp, err := httpjuice.DoRequest(ctx, params.HTTPClient, req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	// Return error if status code is not as expected.
	if resp.StatusCode != params.ExpectedStatus {
		return fmt.Errorf("file could not be saved: status code %d", resp.StatusCode)
	}

	return nil
}
