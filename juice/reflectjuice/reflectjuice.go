// Description: This package contains helper functions for working with the reflect package.
package reflectjuice

import (
	"fmt"
	"reflect"
)

// ReplaceNilPointersFields replaces nil pointer fields in the first struct (toUpdate)
// with the corresponding values from the second struct (currentDocument).
//
// Key Principles:
// - Only nil pointers in `toUpdate` will be replaced by the corresponding values from `currentDocument`.
// - Deep copying prevents the structs from sharing underlying data references.
// - Non-nil fields remain untouched in `toUpdate`.
// - Unexported fields will be skipped.
// - Supports nested struct pointers with recursive handling.
// - Prevents panic for non-struct primitive types.
//
// Supported Input Combinations:
// - `toUpdate` and `currentDocument` can be:
//   - An interface holding a struct directly.
//   - An interface holding a pointer to a struct.
//   - A pointer to a struct directly.
//   - A struct directly.
func ReplaceNilPointersFields(toUpdate, currentDocument interface{}) error {
	officialValue := reflect.ValueOf(toUpdate)
	currentValue := reflect.ValueOf(currentDocument)

	// Unwrap interfaces if they hold a pointer to a struct.
	if officialValue.Kind() == reflect.Interface {
		officialValue = officialValue.Elem()
	}

	if currentValue.Kind() == reflect.Interface {
		currentValue = currentValue.Elem()
	}

	// Unwrap pointers to structs.
	if officialValue.Kind() == reflect.Ptr && officialValue.Elem().Kind() == reflect.Struct {
		officialValue = officialValue.Elem()
	}

	if currentValue.Kind() == reflect.Ptr && currentValue.Elem().Kind() == reflect.Struct {
		currentValue = currentValue.Elem()
	}

	// Prevent panic for non-struct primitive types.
	if officialValue.Kind() != reflect.Struct || currentValue.Kind() != reflect.Struct {
		return fmt.Errorf("inputs must be structs or pointers to structs")
	}

	// Ensure both structs are of the same type.
	if officialValue.Type() != currentValue.Type() {
		return fmt.Errorf("struct types must be identical, got %s and %s", officialValue.Type(), currentValue.Type())
	}

	// Handle edge case for zero fields.
	if officialValue.NumField() == 0 {
		return nil
	}

	// Iterate over fields and replace nil pointers if struct.
	for i := 0; i < officialValue.NumField(); i++ {
		// Prevent panic for unexported fields
		if !officialValue.Type().Field(i).IsExported() {
			continue
		}

		officialField := officialValue.Field(i)
		currentField := currentValue.Field(i)

		// If field is a pointer to a primitive type, copy directly.
		if officialField.Kind() == reflect.Ptr && officialField.Type().Elem().Kind() != reflect.Struct {
			if officialField.IsNil() && !currentField.IsNil() {
				// Deep copy primitive types directly.
				officialField.Set(reflect.New(currentField.Type().Elem()))
				officialField.Elem().Set(currentField.Elem())
			}
			continue
		}

		// Recurse only for pointers to structs.
		if officialField.Kind() == reflect.Ptr && officialField.Type().Elem().Kind() == reflect.Struct {
			if officialField.IsNil() && !currentField.IsNil() {
				officialField.Set(reflect.New(currentField.Type().Elem()))
				officialField.Elem().Set(currentField.Elem())
			} else if !officialField.IsNil() && !currentField.IsNil() {
				if err := ReplaceNilPointersFields(officialField.Interface(), currentField.Interface()); err != nil {
					return err
				}
			}
			continue
		}

		// Recurse for direct structs (embedded structs).
		if officialField.Kind() == reflect.Struct {
			if err := ReplaceNilPointersFields(officialField.Addr().Interface(), currentField.Addr().Interface()); err != nil {
				return err
			}
		}
	}

	return nil
}
