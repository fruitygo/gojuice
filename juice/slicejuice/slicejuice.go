// Description: This package contains utility functions for working with slices.
package slicejuice

import (
	"errors"
	"fmt"
	"reflect"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// ------------------------------
// TYPES
// ------------------------------

// SliceComparisonResult holds the differences and similarities between two slices.
type SliceComparisonResult[T comparable] struct {
	DifferenceA []T // Elements in sliceA but not in sliceB.
	DifferenceB []T // Elements in sliceB but not in sliceA.
	Similar     []T // Elements present in both slices.
}

// SliceObjectIDComparisonResult holds the differences and similarities between two slices of primitive.ObjectID.
type SliceObjectIDComparisonResult struct {
	DifferenceA []primitive.ObjectID // Elements in sliceA but not in sliceB.
	DifferenceB []primitive.ObjectID // Elements in sliceB but not in sliceA.
	Similar     []primitive.ObjectID // Elements present in both slices.
}

// ------------------------------
// FUNCTIONS
// ------------------------------

// ContainsStringLinear checks if a slice contains a given element using a linear search.
//
// This method has a time complexity of O(n), where n is the length of the slice.
//
// It is suitable for small slices or when the slice is not expected to contain the element.
func ContainsStringLinear(slice []string, element string) bool {
	for _, s := range slice {
		if s == element {
			return true
		}
	}

	return false
}

// ContainsStringMap checks if a slice contains a given element using a map for faster lookups.
//
// This method has a time complexity of O(n) for creating the map, and O(1) for the lookup.
//
// However, it has a space complexity of O(n), where n is the length of the slice.
//
// It is suitable for large slices or when the slice is expected to contain the element.
func ContainsStringMap(slice []string, element string) bool {
	type blob struct{}

	set := make(map[string]struct{}, len(slice))

	for _, s := range slice {
		set[s] = blob{}
	}

	_, ok := set[element]

	return ok
}

// ContainsObjectIDMap checks if a slice contains a given element using a map for faster lookups.
//
// This method has a time complexity of O(n) for creating the map, and O(1) for the lookup.
//
// However, it has a space complexity of O(n), where n is the length of the slice.
//
// It is suitable for large slices or when the slice is expected to contain the element.
func ContainsObjectIDMap(slice []primitive.ObjectID, element primitive.ObjectID) bool {
	type blob struct{}

	set := make(map[primitive.ObjectID]struct{}, len(slice))

	for _, s := range slice {
		set[s] = blob{}
	}

	_, ok := set[element]

	return ok
}

// ContainsAny checks if an element is present in the given slice.
//
// This method uses reflection to work with slices of any type.
//
// It has a time complexity of O(n), where n is the length of the slice.
func ContainsAny(slice interface{}, element interface{}) (bool, error) {
	sliceValue := reflect.ValueOf(slice)

	if sliceValue.Kind() != reflect.Slice {
		return false, fmt.Errorf("expected a slice, got %T", sliceValue)
	}

	for i := 0; i < sliceValue.Len(); i++ {
		if reflect.DeepEqual(sliceValue.Index(i).Interface(), element) {
			return true, nil
		}
	}

	return false, nil
}

// RemoveDuplicates removes duplicates from a slice of any type using generics.
func RemoveDuplicates[T comparable](slice []T) []T {
	uniqueMap := make(map[T]struct{})

	uniqueSlice := make([]T, 0)

	for _, item := range slice {
		if _, found := uniqueMap[item]; !found {
			uniqueSlice = append(uniqueSlice, item)
			uniqueMap[item] = struct{}{}
		}
	}

	return uniqueSlice
}

// RemoveDuplicatesObjectIDs removes duplicates from a slice of primitive.ObjectID.
func RemoveDuplicatesObjectIDs(ids []primitive.ObjectID) []primitive.ObjectID {
	uniqueMap := make(map[string]struct{})

	uniqueSlice := make([]primitive.ObjectID, 0)

	for i, id := range ids {
		hex := id.Hex()
		if _, found := uniqueMap[hex]; !found {
			uniqueSlice = append(uniqueSlice, ids[i])
			uniqueMap[hex] = struct{}{}
		}
	}

	return uniqueSlice
}

// CheckDuplicateValues checks for duplicates across multiple slices of type T.
// Returns an error if at least one value is duplicated across a minimum of two slices or in the same slice.
func CheckDuplicateValues[T comparable](slices ...[]T) error {
	seenValues := make(map[T]bool)

	for _, slice := range slices {
		for _, value := range slice {
			if seenValues[value] {
				return errors.New("duplicate value found across slices")
			}
			seenValues[value] = true
		}
	}

	return nil
}

// RemoveSliceElements removes elements of the second slice from the first slice.
func RemoveSliceElements[T comparable](slice1, slice2 []T) []T {
	m := make(map[T]bool)
	for _, item := range slice2 {
		m[item] = true
	}

	result := make([]T, 0)
	for _, item := range slice1 {
		if _, found := m[item]; !found {
			result = append(result, item)
		}
	}

	return result
}

// CompareSlices compares two slices and returns a struct with the differences and similarities.
//
// It works with slices of any comparable type and has a time complexity of O(n + m),
// where n is the length of sliceA and m is the length of sliceB.
func CompareSlices[T comparable](sliceA, sliceB []T) SliceComparisonResult[T] {
	setA := make(map[T]struct{}, len(sliceA))
	setB := make(map[T]struct{}, len(sliceB))

	// Populate sets for each slice.
	for _, element := range sliceA {
		setA[element] = struct{}{}
	}
	for _, element := range sliceB {
		setB[element] = struct{}{}
	}

	// Initialize slices to avoid nil values.
	differenceA := []T{}
	differenceB := []T{}
	similar := []T{}

	// Find differences and similarities.
	for _, element := range sliceA {
		if _, found := setB[element]; found {
			similar = append(similar, element)
		} else {
			differenceA = append(differenceA, element)
		}
	}

	for _, element := range sliceB {
		if _, found := setA[element]; !found {
			differenceB = append(differenceB, element)
		}
	}

	return SliceComparisonResult[T]{
		DifferenceA: differenceA,
		DifferenceB: differenceB,
		Similar:     similar,
	}
}

// CompareObjectIDSlices compares two slices of primitive.ObjectID and returns a struct with the differences and similarities.
func CompareObjectIDSlices(sliceA, sliceB []primitive.ObjectID) SliceObjectIDComparisonResult {
	setB := make(map[string]primitive.ObjectID, len(sliceB))
	// Create a map of sliceB using the ObjectID hex representation as keys.
	for _, oid := range sliceB {
		setB[oid.Hex()] = oid
	}

	// Initialize slices to avoid nil values.
	differenceA := []primitive.ObjectID{}
	differenceB := []primitive.ObjectID{}
	similar := []primitive.ObjectID{}
	seenInA := make(map[string]bool)

	// Compare sliceA against sliceB.
	for _, oid := range sliceA {
		if _, found := setB[oid.Hex()]; found {
			similar = append(similar, oid)
		} else {
			differenceA = append(differenceA, oid)
		}
		seenInA[oid.Hex()] = true
	}

	// Find elements in sliceB that are not in sliceA.
	for _, oid := range sliceB {
		if !seenInA[oid.Hex()] {
			differenceB = append(differenceB, oid)
		}
	}

	return SliceObjectIDComparisonResult{
		DifferenceA: differenceA,
		DifferenceB: differenceB,
		Similar:     similar,
	}
}
