// Description: This package provides utility functions for string manipulation.
package stringjuice

import (
	"net/url"
	"strconv"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/text/unicode/norm"
)

// ------------------------------
// TYPES
// ------------------------------

// NormalizeOpts represents options for customizing the normalization process.
type NormalizeOpts struct {
	ToLower        bool // Convert to lowercase.
	RemoveDiacs    bool // Remove diacritics.
	TrimSpace      bool // Remove leading and trailing whitespace.
	RemovePunct    bool // Remove punctuation.
	RemoveSpec     bool // Remove special characters.
	RemoveDigits   bool // Remove digits.
	RemoveNonASCII bool // Remove non-ASCII characters.
}

// ------------------------------
// FUNCTIONS
// ------------------------------

// ConvertToType converts string value to the appropriate type.
//
// It tries to convert to primitive.ObjectID, bool, int, float, time.Time, and if none of these succeed, it defaults to string.
func ConvertToType(s string) (interface{}, error) {

	// Try converting to primitive.ObjectID.
	if objID, err := primitive.ObjectIDFromHex(s); err == nil {
		return objID, nil
	}

	// Try converting to int.
	if intVal, err := strconv.Atoi(s); err == nil {
		return intVal, nil
	}

	// Try converting to bool.
	if boolVal, err := strconv.ParseBool(s); err == nil {
		return boolVal, nil
	}

	// Try converting to float.
	if floatVal, err := strconv.ParseFloat(s, 64); err == nil {
		return floatVal, nil
	}

	// Try converting to time.Time.
	if timeVal, err := time.Parse(time.RFC3339, s); err == nil {
		return timeVal, nil
	}

	// Default to string if no conversion succeeds.
	return s, nil
}

// ConvertQueryStringToMap converts a query string to a map[string][]string.
//
// It returns an error if the query string cannot be parsed.
func ConvertQueryStringToMap(s string) (map[string][]string, error) {
	parsedQuery, err := url.ParseQuery(s)
	if err != nil {
		return nil, err
	}

	result := make(map[string][]string)
	for key, values := range parsedQuery {
		result[key] = values
	}

	return result, nil
}

// Normalize normalizes a string according to the provided options.
func Normalize(s string, opts NormalizeOpts) string {
	// Convert to lowercase if specified.
	if opts.ToLower {
		s = strings.ToLower(s)
	}

	// Remove diacritics if specified.
	if opts.RemoveDiacs {
		s = norm.NFD.String(s)
		var builder strings.Builder
		for _, r := range s {
			if !unicode.Is(unicode.Mn, r) {
				builder.WriteRune(r)
			}
		}
		s = builder.String()
	}

	// Trim leading and trailing whitespace if specified.
	if opts.TrimSpace {
		s = strings.TrimSpace(s)
	}

	// Apply character filtering if specified.
	if opts.RemovePunct || opts.RemoveSpec || opts.RemoveDigits || opts.RemoveNonASCII {
		var builder strings.Builder
		for _, r := range s {
			if opts.RemovePunct && unicode.IsPunct(r) {
				continue
			}
			if opts.RemoveSpec && unicode.IsSymbol(r) {
				continue
			}
			if opts.RemoveDigits && unicode.IsDigit(r) {
				continue
			}
			if opts.RemoveNonASCII && r >= utf8.RuneSelf {
				continue
			}
			builder.WriteRune(r)
		}
		s = builder.String()
	}

	return s
}
