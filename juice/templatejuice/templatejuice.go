// Description: This package provides utilities to be utilized as functions in the Go HTML templates.
package templatejuice

import (
	"net/url"
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// DerefString dereferences a string pointer.
func DerefString(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}

// GenHubLink generates a link based on the provided server URL and fileName and returns it.
//
// If any errors occur during URL parsing, it panics to prevent template from being rendered.
func GenHubLink(serverURL, fileName string) string {
	parsedURL, err := url.Parse(serverURL)
	if err != nil {
		panic(err)
	}

	return parsedURL.JoinPath(fileName).String()
}
