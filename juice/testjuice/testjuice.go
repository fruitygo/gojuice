// Description: This package contains helper functions to be used in tests.
package testjuice

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/fruitygo/gojuice/juice/slicejuice"
	"gitlab.com/fruitygo/gojuice/juice/validationjuice"
)

// ------------------------------
// VARIABLES
// ------------------------------

// ------------------------------
// CONST
// ------------------------------

const (
	MongoIDKey  string = "_id"
	RequestMsg  string = "| Request 🌎  |"
	ResponseMsg string = "| Response 📮 |"
)

// ------------------------------
// TYPES
// ------------------------------

type (

	// PubSubPayload is the model for a Google Pub/Sub payload.
	PubSubPayload struct {
		Message      PubSubMessage `json:"message,omitempty"`
		Subscription string        `json:"subscription,omitempty"`
	}

	// PubSubMessage is the model for a Google Pub/Sub message.
	PubSubMessage struct {
		Data        []byte            `json:"data,omitempty"`
		Attributes  map[string]string `json:"attributes,omitempty"`
		MessageID   string            `json:"messageId,omitempty"`
		PublishTime time.Time         `json:"publishTime,omitempty"`
	}

	// Define a checker function signature.
	checker func(data interface{}) error
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// AssertResponse is a function that checks if the status code of an HTTP response matches the expected status code.
//
// It takes an expected status code, an HTTP response, and a response body as parameters.
//
// If the status code of the response does not match the expected status code, it returns an error with a message indicating the expected and actual status codes and the response body.
//
// If the status code of the response matches the expected status code, it returns nil.
func AssertResponse(expectedStatusCode int, resp *http.Response, responseBody []byte) error {
	if err := validationjuice.CheckNilValues(resp, responseBody); err != nil {
		return err
	}

	if resp.StatusCode != expectedStatusCode {
		return fmt.Errorf("expected status code %d, got: %d. response body: %s", expectedStatusCode, resp.StatusCode, responseBody)
	}

	return nil
}

// AssertStatuses is a function that checks if the status code of an HTTP response matches any of the accepted status codes.
//
// It takes a slice of accepted status codes, an HTTP response, and a response body as parameters.
//
// If the status code of the response matches any of the accepted status codes, it returns nil.
//
// If the status code of the response does not match any of the accepted status codes, it returns an error with a message indicating the accepted status codes and the actual status code and response body.
func AssertStatuses(acceptedStatuses []int, resp *http.Response, responseBody []byte) error {
	if err := validationjuice.CheckNilValues(acceptedStatuses, resp, responseBody); err != nil {
		return err
	}

	for _, status := range acceptedStatuses {
		if resp.StatusCode == status {
			return nil
		}
	}

	return fmt.Errorf("expected status codes %v, got: %d. response body: %s", acceptedStatuses, resp.StatusCode, responseBody)
}

// CheckErrorAndFail is a function that checks if an error is not nil and fails the test if it is.
//
// It takes a testing.T and an error as parameters.
//
// If the error is not nil, it calls t.Fatalf with the error message, causing the test to fail.
//
// This function is intended to be used with a test level (global) error field.
func CheckErrorAndFail(t *testing.T, err error) {
	if err != nil {
		t.Fatalf("error: %v", err)
	}
}

// CheckErrorAndFail is a function that checks if an error is not nil and fails the test if it is.
//
// It takes a testing.T and an error as parameters.
//
// If the error is not nil, it calls t.Fatalf with the error message, causing the test to fail.
//
// This function is intended to be used with a test level (global) error field.
func CheckErrorAndFailWithMessage(t *testing.T, err error, debugMessage string) {
	if err != nil {
		t.Logf("%s: %s", debugMessage, err.Error())
		t.FailNow()
	}
}

// EncodePubSubMessage encodes a Google Pub/Sub message to a bytes.Buffer to use it as a request body in tests.
func NewMockPubSubPayload(message *pubsub.Message, subscriptionMockValue string) (*bytes.Buffer, error) {
	if err := validationjuice.CheckNilValues(message); err != nil {
		return nil, err
	}

	payload := newPubSubPayload(
		[]byte(base64.StdEncoding.EncodeToString(message.Data)),
		message.Attributes,
		message.ID,
		message.PublishTime,
		subscriptionMockValue,
	)

	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	return bytes.NewBuffer(jsonPayload), nil
}

// RetrieveObjectIDFromResponse extracts primitive.ObjectIDs from a response body and append them to a slice for later use.
func RetrieveObjectIDFromResponse(t *testing.T, responseBody []byte, idField string, appendTo *[]primitive.ObjectID, bypass bool) error {
	if err := validationjuice.CheckNilValues(t, responseBody, appendTo); err != nil {
		return err
	}

	// Bypass the function if the bypass flag is set.
	if bypass {
		return nil
	}

	// Define a variable to store the response body.
	var responseData interface{}

	// Unmarshal the response body.
	if err := json.Unmarshal(responseBody, &responseData); err != nil {
		return err
	}

	// Define a recursive function to check for specified ID fields in the map or slice.
	var checkForIDs checker
	checkForIDs = func(data interface{}) error {
		switch data := data.(type) {
		case map[string]interface{}:
			// If the data is a map, check it.
			if idValue, ok := data[idField]; ok {
				idStr, ok := idValue.(string)
				if !ok {
					return fmt.Errorf("expected string for field %s, got: %T", idField, idValue)
				}

				// Convert the string to an ObjectID.
				id, err := primitive.ObjectIDFromHex(idStr)
				if err != nil {
					return err
				}

				if !slicejuice.ContainsObjectIDMap(*appendTo, id) {
					*appendTo = append(*appendTo, id)
				}
			}
		case []interface{}:
			// If the data is a slice, call the method recursively on each item.
			for _, item := range data {
				if itemMap, ok := item.(map[string]interface{}); ok {
					if err := checkForIDs(itemMap); err != nil {
						return err
					}
				}
			}
		}

		return nil
	}

	// Start the recursive check.
	return checkForIDs(responseData)
}

// RetrieveStringFromResponse extracts string values from a response body and appends them to a slice for later use.
func RetrieveStringFromResponse(t *testing.T, responseBody []byte, stringField string, appendTo *[]string, bypass bool) error {
	// Bypass the function if the bypass flag is set.
	if bypass {
		return nil
	}

	// Define a variable to store the response body.
	var responseData interface{}

	// Unmarshal the response body.
	if err := json.Unmarshal(responseBody, &responseData); err != nil {
		return err
	}

	// Define a recursive function to check for specified string fields in the map or slice.
	var checkForStrings checker
	checkForStrings = func(data interface{}) error {
		switch data := data.(type) {
		case map[string]interface{}:
			// If the data is a map, check it.
			if value, ok := data[stringField]; ok {
				strValue, ok := value.(string)
				if !ok {
					return fmt.Errorf("expected string for field %s, got: %T", stringField, value)
				}

				// Append to the slice if the value is not already in it.
				if !slicejuice.ContainsStringMap(*appendTo, strValue) {
					*appendTo = append(*appendTo, strValue)
				}
			}
		case []interface{}:
			// If the data is a slice, call the method recursively on each item.
			for _, item := range data {
				if itemMap, ok := item.(map[string]interface{}); ok {
					if err := checkForStrings(itemMap); err != nil {
						return err
					}
				}
			}
		}

		return nil
	}

	// Start the recursive check.
	return checkForStrings(responseData)
}

// WaitForServer waits for a server to be ready by trying to connect to the URL.
//
// It will retry at intervals until the timeout expires or a response is received.
func WaitForServer(url string, timeout time.Duration) error {
	// Define a deadline.
	deadline := time.Now().Add(timeout)

	// Iterate until the deadline is reached.
	for time.Now().Before(deadline) {
		resp, err := http.Get(url)
		if err == nil && resp != nil {
			resp.Body.Close()
			return nil
		}

		// Wait before retry.
		time.Sleep(200 * time.Millisecond)
	}

	return fmt.Errorf("server not ready after %v", timeout)
}

// LogTest logs the request/response body in a formatted JSON.
//
// It constructs a detailed log message containing information about the HTTP method, URL,
// and a formatted version of the response body. Returns an error if JSON formatting fails.
func LogTest(t *testing.T, responseBody []byte, method, URL, testMessage, testCaseName, collectionName, testDescription string) error {
	// Define constants for log message formatting.
	const (
		logMessageTestName        string = "\n\n\t\t\t%s\n\nTest Name:\t\t%s\nCollection:\t\t%s\nPurpose:\t\t%s\n"
		logMessageMethodURLBody   string = "Method:\t\t\t%s\nURL:\t\t\t%s\nBody:\n%s\n"
		logMessageMethodEmptyBody string = "Method:\t\t\t%s\nURL:\t\t\t%s\nBody:\t\t\tThe body is empty\n"
		logMessageEnd             string = "________________________________\n✅\t\t\tEND\n"
		tab0                             = ""
		tab4                             = "   "
	)

	// Define a string builder for constructing the log message.
	var logMessage strings.Builder

	// Write the initial part of the log message, including test details.
	fmt.Fprintf(&logMessage, logMessageTestName, testMessage, testCaseName, collectionName, testDescription)

	// Check if the response body is non-empty.
	if len(responseBody) > 0 {
		var err error
		var formattedJSON bytes.Buffer

		// Set up the JSON encoder once.
		enc := json.NewEncoder(&formattedJSON)
		enc.SetEscapeHTML(false)
		enc.SetIndent(tab0, tab4)

		// Determine if the response body is a JSON array by inspecting the first byte.
		if responseBody[0] == '[' {
			var result []map[string]interface{}

			// Unmarshal the JSON array into a slice of maps.
			err = json.Unmarshal(responseBody, &result)
			if err != nil {
				return err
			}

			// Encode the array using the same encoder.
			err = enc.Encode(result)
			if err != nil {
				return err
			}
		} else {
			var result map[string]interface{}

			// Unmarshal the JSON object into a map.
			err = json.Unmarshal(responseBody, &result)
			if err != nil {
				return err
			}

			// Encode the object using the same encoder.
			err = enc.Encode(result)
			if err != nil {
				return err
			}
		}

		// Log the HTTP method, URL, and the formatted JSON body.
		fmt.Fprintf(&logMessage, logMessageMethodURLBody, method, URL, formattedJSON.String())
	} else {
		// If the response body is empty, log a specific message.
		fmt.Fprintf(&logMessage, logMessageMethodEmptyBody, method, URL)
	}

	// Append the end of the log message and output it to the test log.
	fmt.Fprint(&logMessage, logMessageEnd)
	t.Log(logMessage.String())

	return nil
}

// ------------------------------
// HELPERS
// ------------------------------

// newPubSubPayload creates a new PubSubPayload with the provided parameters.
func newPubSubPayload(data []byte, attributes map[string]string, messageID string, publishTime time.Time, subscription string) *PubSubPayload {
	return &PubSubPayload{
		Message: PubSubMessage{
			Data:        data,
			Attributes:  attributes,
			MessageID:   messageID,
			PublishTime: publishTime,
		},
		Subscription: subscription,
	}
}
