// Description: Package timejuice provides time-related helper functions for managing timezones, weekdays,
// and payroll weeks. It includes functionality to determine the first period of the year
// in a payroll system, ensuring it always encompasses the first Monday of January.
//
// The first period rule:
// The start date of the first period is set as the Sunday immediately preceding the first Monday of January.

package timejuice

import (
	"time"
)

// ------------------------------
// CONSTANTS
// ------------------------------

const (
	SecondsInDay = 86400
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// NewWithPrecision returns a new now time.Time (UTC) with the given precision.
func NowWithPrecision(precision time.Duration) time.Time {
	return time.Now().UTC().Truncate(precision)
}

// SwapTimezone takes a date (which can be in any timezone) and returns the date in the specified timezone.
//
// The original timezone is overridden by the new timezone.
//
// This method does not perform any timezone conversion. It simply changes the timezone of the date.
//
// If toUTC is true, the date is converted to UTC after the timezone change.
//
// If toMidnight is true, the date is normalized to the beginning of the day after the timezone change, before the conversion to UTC.
func SwapTimezone(date time.Time, timezone string, toUTC bool, toMidnight bool) (time.Time, error) {
	// If the date is already in the target timezone and toUTC is false, or if the date is already in UTC and toUTC is true, return it without modification.
	if (date.Location().String() == timezone && !toUTC) || (IsUTCDate(date) && toUTC) {
		return date, nil
	}

	// Load the new timezone from the IANA Time Zone database.
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		return time.Time{}, err
	}

	// Create a new date that represents the same point in time as the original date, but in the new timezone.
	dateWithTimezone := time.Date(
		date.Year(),
		date.Month(),
		date.Day(),
		date.Hour(),
		date.Minute(),
		date.Second(),
		date.Nanosecond(),
		loc,
	)

	// If toMidnight is true, normalize the date to midnight in the new timezone.
	if toMidnight {
		dateWithTimezone = time.Date(
			dateWithTimezone.Year(),
			dateWithTimezone.Month(),
			dateWithTimezone.Day(),
			0, 0, 0, 0, loc,
		)
	}

	// If toUTC is true and the date is not already in UTC, convert the date to UTC.
	if toUTC && !IsUTCDate(dateWithTimezone) {
		dateWithTimezone = dateWithTimezone.UTC()
	}

	// Return the new date.
	return dateWithTimezone, nil
}

// IsUTCDate checks if the given date is in UTC format.
func IsUTCDate(d time.Time) bool {
	return d.Location().String() == time.UTC.String()
}

// GetFirstPeriodStartDate retrieves the date of the first day of the first period of a year.
//
// Result will always be a Sunday, in UTC timezone.
func GetFirstPeriodStartDate(year int) time.Time {
	return GetFirstMondayOfYear(year).AddDate(0, 0, -1).Truncate(24 * time.Hour).UTC()
}

// GetFirstMondayOfYear retrieves the first Monday of the given year, in UTC timezone.
func GetFirstMondayOfYear(year int) time.Time {
	// Create a time.Time object for January 1st of the given year.
	t := time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC)

	// Retrieve the int value of the weekday from 0 (Sunday) to 6 (Saturday).
	firstYearDay := int(t.Weekday())

	// Calculate the number of days to add to reach the first Monday of the year.
	daysToAdd := (1 - firstYearDay + 7) % 7

	// Add the calculated days to January 1st to get the first Monday.
	return t.AddDate(0, 0, daysToAdd).Truncate(24 * time.Hour).UTC()
}

// GetSundayOfGivenPeriod retrieves the Sunday of the week the given time.Time object is in, in UTC timezone.
func GetSundayOfGivenPeriod(t time.Time) time.Time {
	return GetTimeOfWeekday(t, time.Sunday).Truncate(24 * time.Hour).UTC()
}

// GetMondayOfGivenPeriod retrieves the Monday of the week the given time.Time object is in, in UTC timezone.
func GetMondayOfGivenPeriod(t time.Time) time.Time {
	return GetTimeOfWeekday(t, time.Monday).Truncate(24 * time.Hour).UTC()
}

// GetTuesdayOfGivenPeriod retrieves the Tuesday of the week the given time.Time object is in, in UTC timezone.
func GetTuesdayOfGivenPeriod(t time.Time) time.Time {
	return GetTimeOfWeekday(t, time.Tuesday).Truncate(24 * time.Hour).UTC()
}

// GetWednesdayOfGivenPeriod retrieves the Wednesday of the week the given time.Time object is in, in UTC timezone.
func GetWednesdayOfGivenPeriod(t time.Time) time.Time {
	return GetTimeOfWeekday(t, time.Wednesday).Truncate(24 * time.Hour).UTC()
}

// GetThursdayOfGivenPeriod retrieves the Thursday of the week the given time.Time object is in, in UTC timezone.
func GetThursdayOfGivenPeriod(t time.Time) time.Time {
	return GetTimeOfWeekday(t, time.Thursday).Truncate(24 * time.Hour).UTC()
}

// GetFridayOfGivenPeriod retrieves the Friday of the week the given time.Time object is in, in UTC timezone.
func GetFridayOfGivenPeriod(t time.Time) time.Time {
	return GetTimeOfWeekday(t, time.Friday).Truncate(24 * time.Hour).UTC()
}

// GetSaturdayOfGivenPeriod retrieves the Saturday of the week the given time.Time object is in, in UTC timezone.
func GetSaturdayOfGivenPeriod(t time.Time) time.Time {
	return GetTimeOfWeekday(t, time.Saturday).Truncate(24 * time.Hour).UTC()
}

// GetLastDayOfLastPeriod retrieves the last day of the last period of year, in UTC timezone.
//
// Result will always be a Saturday.
func GetLastDayOfLastPeriod(year int) time.Time {
	return GetFirstPeriodStartDate(year+1).AddDate(0, 0, -1).Truncate(24 * time.Hour).UTC()
}

// GetFirstDayOfMonth retrieves the first day of the month for the given date, in UTC timezone.
func GetFirstDayOfMonth(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, time.UTC).Truncate(24 * time.Hour).UTC()
}

// GetPeriodNumber retrieves the period week number for the given date, from 1 trough 53.
func GetPeriodNumber(t time.Time) int {
	// Get the first day of the first period of the year.
	firstPeriodDay := GetFirstPeriodStartDate(t.Year())

	// Check if t is before the first day of the first period of the year.
	if t.Before(firstPeriodDay) {
		// If true, retrieve the first day of the first period of the previous year.
		firstPeriodDay = GetFirstPeriodStartDate(t.Year() - 1)
	}

	// Calculate the number of days between the input date and the first day of the first period of the year with a precision of seconds.
	daysSinceStart := int(t.Sub(firstPeriodDay).Seconds() / SecondsInDay)

	// Compute period number.
	return daysSinceStart/7 + 1
}

// GetTimeOfWeekday retrieves the time.Time value of the given weekday within the given time.Time object, in UTC timezone.
func GetTimeOfWeekday(t time.Time, targetWeekday time.Weekday) time.Time {
	// Calculate the number of days to add to reach the target weekday.
	daysToAdd := (int(targetWeekday) - int(t.Weekday()) + 7) % 7

	// If target weekday is Sunday and daysToAdd is greater than 0, subtract 7 days.
	if targetWeekday == time.Sunday && daysToAdd >= 1 {
		daysToAdd -= 7
	}

	return t.AddDate(0, 0, daysToAdd).Truncate(24 * time.Hour).UTC()
}
