// Package unitjuice provides utility functions to work with basis points of various units for precise computations,
// avoiding the use of floating point numbers.
package unitjuice

import (
	"errors"
	"math"
)

// ------------------------------
// INTERFACES
// ------------------------------

type Integer interface {
	int | int32 | int64
}

// ------------------------------
// TYPES
// ------------------------------

// Unit represents a conversion rate between a unit and its basis points.
type Unit int64

// ------------------------------
// CONSTANTS
// ------------------------------

// Predefined conversion rates (Units).
const (
	// Represents a ratio of 1000000000 (one billionths) of a unit to one unit.
	X1000000000 Unit = 1000000000

	// Represents a ratio of 100000000 (one hundred millionths) of a unit to one unit.
	X100000000 Unit = 100000000

	// Represents a ratio of 10000000 (ten millionths) of a unit to one unit.
	X10000000 Unit = 10000000

	// Represents a ratio of 1000000 (millionths) of a unit to one unit.
	X1000000 Unit = 1000000

	// Represents a ratio of 100000 (thousandths) of a unit to one unit.
	X100000 Unit = 100000

	// Represents a ratio of 10000 (thousandths) of a unit to one unit.
	X10000 Unit = 10000

	// Represents a ratio of 1000 (thousandths) of a unit to one unit.
	X1000 Unit = 1000

	// Represents a ratio of 100 (hundredths) of a unit to one unit.
	X100 Unit = 100

	// Represents a ratio of 10 (tenths) of a unit to one unit.
	X10 Unit = 10

	// Represents a ratio of 1 (unit) of a unit to one unit.
	X1 Unit = 1
)

// ------------------------------
// FUNCTIONS
// ------------------------------

// NewUnit creates a new Unit with the specified value.
func NewUnit(value int64) Unit {
	return Unit(value)
}

// ToMajor converts the given quantity in basis points to the major unit.
//   - valueBasisPoints: The quantity in basis points to be converted.
//   - u: The conversion rate (Unit).
func ToMajor[T Integer](valueBasisPoints T, u Unit) float64 {
	return float64(valueBasisPoints) / float64(u)
}

// ToBasis converts the given quantity from the major unit back to basis points.
//   - valueMajorUnit: The quantity in the major unit to be converted.
//   - u: The conversion rate (Unit).
func ToBasis(valueMajorUnit float64, u Unit) (int64, error) {
	if u <= 0 {
		return 0, errors.New("unit must be greater than zero")
	}

	result := valueMajorUnit * float64(u)
	if result > math.MaxInt64 || result < math.MinInt64 {
		return 0, errors.New("basis points overflow int64 range")
	}

	return int64(result), nil
}

// ------------------------------
// METHODS
// ------------------------------

// FromInt converts an int value in basis points to the major unit.
//   - valueBasisPoints: The quantity in basis points to be converted.
func (u Unit) FromInt(valueBasisPoints int) float64 {
	return ToMajor(valueBasisPoints, u)
}

// FromInt32 converts an int32 value in basis points to the major unit.
//   - valueBasisPoints: The quantity in basis points to be converted.
func (u Unit) FromInt32(valueBasisPoints int32) float64 {
	return ToMajor(valueBasisPoints, u)
}

// FromInt64 converts an int64 value in basis points to the major unit.
//   - valueBasisPoints: The quantity in basis points to be converted.
func (u Unit) FromInt64(valueBasisPoints int64) float64 {
	return ToMajor(valueBasisPoints, u)
}

// ToBasisInt converts a float64 value from the major unit back to basis points.
//   - valueMajorUnit: The quantity in the major unit to be converted.
func (u Unit) ToBasisInt(valueMajorUnit float64) (int, error) {
	result, err := ToBasis(valueMajorUnit, u)
	if err != nil {
		return 0, err
	}
	return int(result), nil
}

// ToBasisInt32 converts a float64 value from the major unit back to basis points.
//   - valueMajorUnit: The quantity in the major unit to be converted.
func (u Unit) ToBasisInt32(valueMajorUnit float64) (int32, error) {
	result, err := ToBasis(valueMajorUnit, u)
	if err != nil {
		return 0, err
	}
	return int32(result), nil
}

// ToBasisInt64 converts a float64 value from the major unit back to basis points.
//   - valueMajorUnit: The quantity in the major unit to be converted.
func (u Unit) ToBasisInt64(valueMajorUnit float64) (int64, error) {
	return ToBasis(valueMajorUnit, u)
}
