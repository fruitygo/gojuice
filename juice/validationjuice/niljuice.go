// Description: This package contains functions that are used to validate nil values.
package validationjuice

import "fmt"

// ------------------------------
// FUNCTIONS
// ------------------------------

// CheckNilValues checks if any of the passed parameters are nil.
//
// NOTE: Only parameters that have the possibility of being nil should be passed to this function.
// Passing a non-nil value of a type that cannot be nil (e.g., a string) will cause a panic.
func CheckNilValues(params ...interface{}) error {
	for _, param := range params {
		if param == nil {
			return fmt.Errorf("one or more parameters is nil")
		}
	}

	return nil
}
