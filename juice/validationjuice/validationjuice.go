// Description: This package provides a set of validators that can be used to validate struct fields.
// It also provides a registry to store and run these validators.
package validationjuice

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"

	"gitlab.com/fruitygo/gojuice/juice/timejuice"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
)

// ------------------------------
// VARIABLES
// ------------------------------

// e164Regex is a regular expression that matches E.164 phone numbers.
var e164Regex = regexp.MustCompile(`^\+[1-9]\d{1,14}$`)

// ------------------------------
// INTERFACES
// ------------------------------

// ValidatorRegistryInterface is an interface that defines the methods that a validatorRegistry must implement.
type ValidatorRegistryInterface interface {
	AddValidator(string, Validator)
	RunTags(interface{}, string) error
}

// ------------------------------
// TYPES
// ------------------------------

// Validator is a function type that performs validation on a string value.
//
// The function returns an error if the validation fails.
type Validator func(fieldValue reflect.Value, fieldName string) error

// validatorRegistry is a struct that holds a map of validators.
//
// Each validator is associated with a tag, and can be used to validate struct fields with that tag.
type validatorRegistry struct {
	Validators map[string]Validator
}

// ------------------------------
// FUNCTIONS
// ------------------------------

// NewValidatorRegistry creates a new validatorRegistry and initializes its validators map.
//
// It returns a ValidatorRegistryInterface.
func NewValidatorRegistry() ValidatorRegistryInterface {
	return &validatorRegistry{
		Validators: make(map[string]Validator),
	}
}

// ------------------------------
// METHODS
// ------------------------------

// AddValidator adds a new validator to the validatorRegistry.
//
// The validator is associated with a tag, and can be used to validate struct fields with that tag.
func (r *validatorRegistry) AddValidator(tag string, validator Validator) {
	r.Validators[tag] = validator
}

// RunTags checks the tags of the fields in a struct. If a field has a tag that matches
// a validator in the validatorRegistry, it will run that validator on the field.
//
// It returns an error if the data is not a struct or a pointer to a struct, or if a validator returns an error.
func (r *validatorRegistry) RunTags(data interface{}, tagKey string) error {
	val := reflect.ValueOf(data)

	if val.Kind() == reflect.Ptr && val.Elem().Kind() == reflect.Struct {
		val = val.Elem()
	}

	if val.Kind() != reflect.Struct {
		return fmt.Errorf("RunTags: expected struct or pointer to struct, got %s", val.Kind())
	}

	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)

		fieldName := val.Type().Field(i).Name

		tagRules := val.Type().Field(i).Tag.Get(tagKey)

		tags := strings.Split(tagRules, ",")

		for _, tag := range tags {
			if validator, ok := r.Validators[tag]; ok {
				if err := validator(field, fieldName); err != nil {
					return err
				}
			}
		}

		// If the field is a struct, call RunTags recursively.
		if field.Kind() == reflect.Struct {
			if err := r.RunTags(field.Addr().Interface(), tagKey); err != nil {
				return err
			}
		}
	}
	return nil
}

// ------------------------------
// POINTER FIELDS VALIDATORS
// ------------------------------

// NewPointerFieldE164Validator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is a valid E.164 phone number.
func NewPointerFieldE164PhoneValidator() Validator {
	return validatePointerFieldPhoneNumber
}

// NewNonNilPointerValidator returns a validator that checks if a field is a pointer and is not nil.
func NewNonNilPointerValidator() Validator {
	return validatePointerFieldIsNotNil
}

// NewNonNilPointerElemValidator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is not zero.
func NewNonZeroPointerElemValidator() Validator {
	return validateNonNilPointerFieldElemNotZero
}

// NewPointerFieldIntInsideRangeValidator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is an int inside a given range.
func NewPointerFieldIntInsideRangeValidator(min int, max int) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validatePointerFieldIntInsideRange(field, fieldName, min, max)
	}
}

// NewPointerFieldInt32InsideRangeValidator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is an int inside a given range.
func NewPointerFieldInt32InsideRangeValidator(min int32, max int32) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validatePointerFieldInt32InsideRange(field, fieldName, min, max)
	}
}

// NewPointerFieldInt64InsideRangeValidator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is an int inside a given range.
func NewPointerFieldInt64InsideRangeValidator(min int64, max int64) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validatePointerFieldInt64InsideRange(field, fieldName, min, max)
	}
}

// NewPointerFieldUintInsideRangeValidator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is an uint inside a given range.
func NewPointerFieldUintInsideRangeValidator(min uint, max uint) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validatePointerFieldUintInsideRange(field, fieldName, min, max)
	}
}

// NewPointerFieldFloat32InsideRangeValidator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is a float32 inside a given range.
func NewPointerFieldFloat32InsideRangeValidator(min float32, max float32) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validatePointerFieldFloat32InsideRange(field, fieldName, min, max)
	}
}

// NewPointerFieldFloat64InsideRangeValidator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is a float64 inside a given range.
func NewPointerFieldFloat64InsideRangeValidator(min float64, max float64) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validatePointerFieldFloat64InsideRange(field, fieldName, min, max)
	}
}

// NewPointerFieldIsValidBPC47Validator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is a valid BCP47 language tag.
func NewPointerFieldIsValidBPC47Validator() Validator {
	return validatePointerFieldIsValidBCP47
}

// NewNonNilPointerFieldElemIsUTCDateValidator returns a validator that checks if a field is a pointer, is not nil, and its underlying value is a valid UTC date.
func NewNonNilPointerFieldElemIsUTCDateValidator() Validator {
	return validateNonNilPointerFieldElemIsUTCDate
}

// NewPointerFieldIsValidISOCurrency returns a validator that checks if a field is a pointer, is not nil, and its underlying value is a valid ISO currency code.
func NewPointerFieldIsValidISOCurrency() Validator {
	return validatePointerFieldIsValidISOCurrency
}

// ------------------------------
// NON POINTER FIELDS VALIDATORS
// ------------------------------

// NewE164Validator returns a validator that checks if a field is a string and is a valid E.164 phone number.
func NewE164PhoneValidator() Validator {
	return validatePhoneNumber
}

// NewNonZeroValidator returns a validator that checks if a field is not a pointer and is not zero.
func NewNonZeroValidator() Validator {
	return validateNonPointerFieldNotZero
}

// NewNonPointerFieldIntInsideRangeValidator returns a validator that checks if a field is an int and is inside a given range.
func NewNonPointerFieldIntInsideRangeValidator(min int, max int) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validateNonPointerFieldIntInsideRange(field, fieldName, min, max)
	}
}

// NewNonPointerFieldUintInsideRangeValidator returns a validator that checks if a field is an uint and is inside a given range.
func NewNonPointerFieldUintInsideRangeValidator(min uint, max uint) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validateNonPointerFieldUintInsideRange(field, fieldName, min, max)
	}
}

// NewNonPointerFieldFloat32InsideRangeValidator returns a validator that checks if a field is a float32 and is inside a given range.
func NewNonPointerFieldFloat32InsideRangeValidator(min float32, max float32) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validateNonPointerFieldFloat32InsideRange(field, fieldName, min, max)
	}
}

// NewNonPointerFieldFloat64InsideRangeValidator returns a validator that checks if a field is a float64 and is inside a given range.
func NewNonPointerFieldFloat64InsideRangeValidator(min float64, max float64) Validator {
	return func(field reflect.Value, fieldName string) error {
		return validateNonPointerFieldFloat64InsideRange(field, fieldName, min, max)
	}
}

// ------------------------------
// HELPERS
// ------------------------------

func validatePointerFieldIsNotNil(field reflect.Value, fieldName string) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}

	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}

	return nil
}

func validatePointerFieldIsValidBCP47(field reflect.Value, fieldName string) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}

	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}

	if field.Elem().Kind() != reflect.String {
		return fmt.Errorf("field %s is required to be a string but is of kind %s", fieldName, field.Elem().Kind())
	}

	_, err := language.Parse(field.Elem().String())
	switch e := err.(type) {
	case nil:
		return nil
	case language.ValueError:
		return fmt.Errorf("culprit - field %s is required to be a valid BCP47 language tag but is %s and culprit is %s", fieldName, field.Elem().String(), e.Subtag())
	default:
		return fmt.Errorf("ill-formed - field %s is required to be a valid BCP47 language tag but is %s", fieldName, field.Elem().String())
	}
}

func validatePointerFieldIsValidISOCurrency(field reflect.Value, fieldName string) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().Kind() != reflect.String {
		return fmt.Errorf("field %s is required to be a string but is of kind %s", fieldName, field.Elem().Kind())
	}

	_, err := currency.ParseISO(field.Elem().String())
	if err != nil {
		return fmt.Errorf("field %s is required to be a valid ISO 4217 currency code but is %s", fieldName, field.Elem().String())
	}
	return nil
}

func validateNonNilPointerFieldElemNotZero(field reflect.Value, fieldName string) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().IsZero() {
		return fmt.Errorf("field %s is required but is nil or the underlying value is zero", fieldName)
	}
	return nil
}

func validateNonPointerFieldNotZero(field reflect.Value, fieldName string) error {
	if field.Kind() == reflect.Ptr {
		return fmt.Errorf("field %s is required not to be a pointer but is a pointer", fieldName)
	}
	if field.IsZero() {
		return fmt.Errorf("field %s is required but is a zero value", fieldName)
	}
	return nil
}

func validatePhoneNumber(field reflect.Value, fieldName string) error {
	if field.Kind() != reflect.String {
		return fmt.Errorf("field %s is required to be a string but is of kind %s", fieldName, field.Kind())
	}
	if field.String() == "" {
		return fmt.Errorf("field %s is required but is empty", fieldName)
	}
	if valid := isValidPhoneNumber(field.String()); !valid {
		return fmt.Errorf("field %s is required to be a valid phone number but is %s", fieldName, field.String())
	}
	return nil
}

func validatePointerFieldPhoneNumber(field reflect.Value, fieldName string) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().Kind() != reflect.String {
		return fmt.Errorf("field %s is required to be a string but is of kind %s", fieldName, field.Elem().Kind())
	}
	if valid := isValidPhoneNumber(field.Elem().String()); !valid {
		return fmt.Errorf("field %s is required to be a valid phone number but is %s", fieldName, field.String())
	}
	return nil
}

func validateNonPointerFieldIntInsideRange(field reflect.Value, fieldName string, min int, max int) error {
	if field.Kind() != reflect.Int {
		return fmt.Errorf("field %s is required to be an int but is of kind %s", fieldName, field.Kind())
	}
	if field.Int() < int64(min) || field.Int() > int64(max) {
		return fmt.Errorf("field %s is required to be between %d and %d but is %d", fieldName, min, max, field.Int())
	}
	return nil
}

func validatePointerFieldIntInsideRange(field reflect.Value, fieldName string, min int, max int) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().Kind() != reflect.Int {
		return fmt.Errorf("field %s is required to be an int but is of kind %s", fieldName, field.Elem().Kind())
	}
	if field.Elem().Int() < int64(min) || field.Elem().Int() > int64(max) {
		return fmt.Errorf("field %s is required to be between %d and %d but is %d", fieldName, min, max, field.Elem().Int())
	}
	return nil
}

func validatePointerFieldInt32InsideRange(field reflect.Value, fieldName string, min int32, max int32) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().Kind() != reflect.Int32 {
		return fmt.Errorf("field %s is required to be an int but is of kind %s", fieldName, field.Elem().Kind())
	}
	if field.Elem().Int() < int64(min) || field.Elem().Int() > int64(max) {
		return fmt.Errorf("field %s is required to be between %d and %d but is %d", fieldName, min, max, field.Elem().Int())
	}
	return nil
}

func validatePointerFieldInt64InsideRange(field reflect.Value, fieldName string, min int64, max int64) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().Kind() != reflect.Int64 {
		return fmt.Errorf("field %s is required to be an int but is of kind %s", fieldName, field.Elem().Kind())
	}
	if field.Elem().Int() < min || field.Elem().Int() > max {
		return fmt.Errorf("field %s is required to be between %d and %d but is %d", fieldName, min, max, field.Elem().Int())
	}
	return nil
}

func validateNonPointerFieldUintInsideRange(field reflect.Value, fieldName string, min uint, max uint) error {
	if field.Kind() != reflect.Uint {
		return fmt.Errorf("field %s is required to be an uint but is of kind %s", fieldName, field.Kind())
	}
	if field.Uint() < uint64(min) || field.Uint() > uint64(max) {
		return fmt.Errorf("field %s is required to be between %d and %d but is %d", fieldName, min, max, field.Uint())
	}
	return nil
}

func validatePointerFieldUintInsideRange(field reflect.Value, fieldName string, min uint, max uint) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().Kind() != reflect.Uint {
		return fmt.Errorf("field %s is required to be an uint but is of kind %s", fieldName, field.Elem().Kind())
	}
	if field.Elem().Uint() < uint64(min) || field.Elem().Uint() > uint64(max) {
		return fmt.Errorf("field %s is required to be between %d and %d but is %d", fieldName, min, max, field.Elem().Uint())
	}
	return nil
}

func validateNonPointerFieldFloat32InsideRange(field reflect.Value, fieldName string, min float32, max float32) error {
	if field.Kind() != reflect.Float32 {
		return fmt.Errorf("field %s is required to be a float32 but is of kind %s", fieldName, field.Kind())
	}
	if field.Float() < float64(min) || field.Float() > float64(max) {
		return fmt.Errorf("field %s is required to be between %f and %f but is %f", fieldName, min, max, field.Float())
	}
	return nil
}

func validatePointerFieldFloat32InsideRange(field reflect.Value, fieldName string, min float32, max float32) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().Kind() != reflect.Float32 {
		return fmt.Errorf("field %s is required to be a float32 but is of kind %s", fieldName, field.Elem().Kind())
	}
	if field.Elem().Float() < float64(min) || field.Elem().Float() > float64(max) {
		return fmt.Errorf("field %s is required to be between %f and %f but is %f", fieldName, min, max, field.Elem().Float())
	}
	return nil
}

func validateNonPointerFieldFloat64InsideRange(field reflect.Value, fieldName string, min float64, max float64) error {
	if field.Kind() != reflect.Float64 {
		return fmt.Errorf("field %s is required to be a float64 but is of kind %s", fieldName, field.Kind())
	}
	if field.Float() < min || field.Float() > max {
		return fmt.Errorf("field %s is required to be between %f and %f but is %f", fieldName, min, max, field.Float())
	}
	return nil
}

func validatePointerFieldFloat64InsideRange(field reflect.Value, fieldName string, min float64, max float64) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().Kind() != reflect.Float64 {
		return fmt.Errorf("field %s is required to be a float64 but is of kind %s", fieldName, field.Elem().Kind())
	}
	if field.Elem().Float() < min || field.Elem().Float() > max {
		return fmt.Errorf("field %s is required to be between %f and %f but is %f", fieldName, min, max, field.Elem().Float())
	}
	return nil
}

func isValidPhoneNumber(phoneNumber string) bool {
	return e164Regex.MatchString(phoneNumber)
}

func validateNonNilPointerFieldElemIsUTCDate(field reflect.Value, fieldName string) error {
	if field.Kind() != reflect.Ptr {
		return fmt.Errorf("field %s is required to be a pointer but is of kind %s", fieldName, field.Kind())
	}
	if field.IsNil() {
		return fmt.Errorf("field %s is required but is nil", fieldName)
	}
	if field.Elem().Kind() != reflect.Struct || field.Elem().Type() != reflect.TypeOf(time.Time{}) {
		return fmt.Errorf("field %s is required to be a time.Time but is of kind %s", fieldName, field.Elem().Kind())
	}

	t := field.Elem().Interface().(time.Time)

	if valid := timejuice.IsUTCDate(t); !valid {
		return fmt.Errorf("field %s is required to be a valid UTC date but is %s", fieldName, t)
	}

	return nil
}
