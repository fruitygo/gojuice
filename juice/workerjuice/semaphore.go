// Description: This file contains the implementation of a simple semaphore.
// It is used to control the number of resources that can be accessed concurrently.
package workerjuice

import "sync"

// ------------------------------
// INTERFACES
// ------------------------------

// SemaphoreInterface defines the methods that a semaphore should implement.
type SemaphoreInterface interface {
	// Acquire acquires a resource controlled by the semaphore.
	Acquire()
	// Release releases a resource controlled by the semaphore.
	Release()
	// Wait blocks until all resources have been released.
	Wait()
}

// ------------------------------
// TYPES
// ------------------------------

// semaphore is a simple semaphore implementation.
type semaphore struct {
	// sem is a channel that controls the resources. The channel's capacity is the number of resources.
	sem chan struct{}

	// waitGrp is a WaitGroup that tracks the number of acquired resources.
	waitGrp sync.WaitGroup
}

// ------------------------------
// FUNCTIONS
// ------------------------------

// NewSemaphore creates a new Semaphore with the specified concurrency level.
func NewSemaphore(concurrency int) SemaphoreInterface {
	return &semaphore{
		// Create a buffered channel with a capacity equal to the concurrency level.
		sem: make(chan struct{}, concurrency),
	}
}

// ------------------------------
// METHODS
// ------------------------------

// Acquire acquires a resource by sending an empty struct to the sem channel.
//
// It also increments the WaitGroup counter.
func (s *semaphore) Acquire() {
	s.sem <- struct{}{}

	s.waitGrp.Add(1)
}

// Release releases a resource by receiving an empty struct from the sem channel.
//
// It also decrements the WaitGroup counter.
func (s *semaphore) Release() {
	<-s.sem

	s.waitGrp.Done()
}

// Wait blocks until all resources have been released.
//
// This is achieved by waiting for the WaitGroup counter to reach zero.
func (s *semaphore) Wait() {
	s.waitGrp.Wait()
}
