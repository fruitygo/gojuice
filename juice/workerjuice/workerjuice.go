// Description: This package provides a worker pool implementation for concurrency limiting.
// It includes a Pool interface and a Pool struct that implements the interface.
package workerjuice

import (
	"sync"
)

// ------------------------------
// INTERFACES
// ------------------------------

// Create an interface for the Pool struct.
type Pool interface {
	AddWorker()
	RemoveWorker()
	WorkerCount() int
	Wait()
	Close()
}

// ------------------------------
// TYPES
// ------------------------------

// Worker represents a worker in a worker pool.
type Worker struct{}

// pool represents a worker pool.
//
// It includes a channel of workers for concurrency limiting,
// and a WaitGroup for synchronizing multiple workers.
type pool struct {
	WorkerChan chan Worker // Responsible for concurrency limiting

	WaitGroup sync.WaitGroup // Responsible for synchronization of multiple workers
}

// ------------------------------
// FUNCTIONS
// ------------------------------

// CreateEmptyPool creates a new worker pool with the specified maximum number of workers.
//
// It returns a pointer to the created Pool.
func CreateEmptyPool(maxWorkers uint) Pool {
	return &pool{
		WorkerChan: make(chan Worker, maxWorkers),
	}
}

// ------------------------------
// METHODS
// ------------------------------

// AddWorker is a method of the Pool struct that adds a new worker to the worker pool.
//
// It adds a Worker instance to the workerChan channel and increments the waitGroup counter.
func (p *pool) AddWorker() {
	p.WorkerChan <- Worker{}

	p.WaitGroup.Add(1)
}

// RemoveWorker is a method of the Pool struct that removes a worker from the worker pool.
//
// It removes a Worker instance from the workerChan channel and calls the Done method on the waitGroup to indicate completion of the worker.
func (p *pool) RemoveWorker() {
	<-p.WorkerChan

	p.WaitGroup.Done()
}

// WorkerCount is a method of the Pool struct that returns the current count of workers in the worker pool.
//
// It retrieves the length of the workerChan channel, which represents the number of workers currently in the pool.
func (p *pool) WorkerCount() int {
	return len(p.WorkerChan)
}

// Wait is a method of the Pool struct that blocks until all workers in the pool have finished their tasks.
//
// It uses the waitGroup's Wait() method to wait for all worker goroutines to call Done(), indicating their completion.
func (p *pool) Wait() {
	p.WaitGroup.Wait()
}

// Close is a method of the Pool struct that closes the workerChan channel.
//
// It closes the workerChan channel, which will cause any goroutines attempting to add a worker to the pool to panic.
func (p *pool) Close() {
	close(p.WorkerChan)
}
