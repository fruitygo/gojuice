package testhelpers

import (
	"errors"
	"fmt"
)

var (
	ErrTest = errors.New("test error")
)

func ErrMissingHeader(key, val string) error {
	return fmt.Errorf("missing Header: [%s : %s]", key, val)
}

func ErrUnexpectedHeader(key, val string) error {
	return fmt.Errorf("unexpected Header: [%s : %s]", key, val)
}

func ErrUnexpectedStatusCode(want, got int) error {
	return fmt.Errorf("unexpected Status Code. want: %d, got: %d", want, got)
}

func ErrUnexpectedData(want, got string) error {
	return fmt.Errorf("unexpected data. want: %s, got: %s", want, got)
}
