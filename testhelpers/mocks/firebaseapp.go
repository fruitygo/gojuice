package mocks

import (
	"context"

	"firebase.google.com/go/v4/auth"
	"gitlab.com/fruitygo/gojuice/testhelpers"
)

// MockFirebaseApp is a mock implementation of FirebaseAppInterface.
type MockFirebaseApp struct {
	AuthClient *auth.Client
	AuthErr    bool
}

// Auth mocks the Auth method of FirebaseAppInterface.
func (m *MockFirebaseApp) Auth(ctx context.Context) (*auth.Client, error) {
	if m.AuthErr {
		return nil, testhelpers.ErrTest
	}
	return m.AuthClient, nil
}
