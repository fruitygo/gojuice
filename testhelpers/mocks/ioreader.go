package mocks

import (
	"io"

	"gitlab.com/fruitygo/gojuice/testhelpers"
)

var _ io.Reader = (*MockReader)(nil)
var _ io.ReadCloser = (*MockReader)(nil)

type MockReader struct {
	ReadResult int

	ReadErr  bool
	CloseErr bool
}

func NewMockReader() *MockReader {
	return &MockReader{}
}

func (r *MockReader) Read([]byte) (int, error) {
	if r.ReadErr { // Force an error.
		return 0, testhelpers.ErrTest
	}
	return r.ReadResult, nil
}

func (r *MockReader) Close() error {
	if r.CloseErr { // Force an error.
		return testhelpers.ErrTest
	}
	return nil
}
