package mocks

import (
	"context"

	"gitlab.com/fruitygo/gojuice/testhelpers"
	"googlemaps.github.io/maps"
)

// MockMapsClient is a mock implementation of MapsClientInterface.
type MockMapsClient struct {
	TimezoneResult *maps.TimezoneResult
	GeocodeResult  []maps.GeocodingResult

	TimezoneErr bool
	GeocodeErr  bool
}

// Timezone calls the mocked Timezone function.
func (m *MockMapsClient) Timezone(ctx context.Context, r *maps.TimezoneRequest) (*maps.TimezoneResult, error) {
	if m.TimezoneErr {
		return nil, testhelpers.ErrTest
	}
	return m.TimezoneResult, nil
}

// Geocode calls the mocked Geocode function.
func (m *MockMapsClient) Geocode(ctx context.Context, r *maps.GeocodingRequest) ([]maps.GeocodingResult, error) {
	if m.GeocodeErr {
		return nil, testhelpers.ErrTest
	}
	return m.GeocodeResult, nil
}
