package mocks

import (
	"io/fs"

	"gitlab.com/fruitygo/gojuice/testhelpers"
)

var _ fs.ReadFileFS = (*MockReadFileFS)(nil)

// MockReadFileFS is a mock implementation of fs.ReadFileFS.
type MockReadFileFS struct {
	FileContents map[string][]byte

	ReadFileErr bool
}

// ReadFile simulates reading a file from the mock filesystem.
func (m *MockReadFileFS) ReadFile(name string) ([]byte, error) {
	if m.ReadFileErr {
		return nil, testhelpers.ErrTest
	}

	return m.FileContents[name], nil
}

func (m *MockReadFileFS) Open(string) (fs.File, error) {
	// We don't need to mock this yet because we don't call it.
	// We just need to provide the method signature so that the interface check can pass.
	return nil, nil
}
