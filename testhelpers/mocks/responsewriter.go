package mocks

import (
	"bytes"
	"errors"
	"net/http"
)

var _ http.ResponseWriter = (*MockResponseWriter)(nil)

// MockResponseWriter is a custom mock implementation of http.ResponseWriter.
type MockResponseWriter struct {
	HeaderMap  http.Header
	StatusCode int
	Body       bytes.Buffer

	WriteErr bool
}

// NewMockResponseWriter creates a new instance of MockResponseWriter.
func NewMockResponseWriter() *MockResponseWriter {
	return &MockResponseWriter{
		HeaderMap: make(http.Header),
	}
}

// Header returns the mock headers.
func (rw *MockResponseWriter) Header() http.Header {
	return rw.HeaderMap
}

// WriteHeader mocks the WriteHeader method, which sets the HTTP status code.
func (rw *MockResponseWriter) WriteHeader(statusCode int) {
	rw.StatusCode = statusCode
}

// Write mocks the Write method, which writes the response body.
func (rw *MockResponseWriter) Write(p []byte) (int, error) {
	if rw.WriteErr {
		return -1, errors.New("test error")
	}
	return rw.Body.Write(p)
}
