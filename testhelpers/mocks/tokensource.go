package mocks

import (
	"gitlab.com/fruitygo/gojuice/testhelpers"
	"golang.org/x/oauth2"
)

// MockTokenSource is a mock implementation of oauth2.TokenSource.
type MockTokenSource struct {
	T        *oauth2.Token
	TokenErr bool
}

// Token returns the mock token or error based on the configuration.
func (m *MockTokenSource) Token() (*oauth2.Token, error) {
	if m.TokenErr {
		return nil, testhelpers.ErrTest
	}
	return m.T, nil
}
